#### build.gradle.kts 
Unresolved Error  
https://github.com/gradle/kotlin-dsl-samples/issues/843
```text
Thanks for the very well done report @StefMa!
This is expected. The Gradle Kotlin DSL can't know at script compilation time that these configurations will be available when the script is applied, it could be applied by any other script. Hence the Kotlin extensions for these configuration not being available at script compilation time. You have to reference the configurations by name at runtime.
Either by string:
// dependencies.gradle.kts
dependencies {
    "implementation"("com.android.support:appcompat-v7:27.1.1")
    "implementation"("org.jetbrains.kotlin:kotlin-stdlib-jdk7:1.2.30")
    "implementation"("org.jetbrains.kotlin:kotlin-reflect:1.2.30")
    "testImplementation"("junit:junit:4.12")
    "androidTestImplementation"("com.android.support.test:runner:1.0.1")
    "androidTestImplementation"("com.android.support.test.espresso:espresso-core:3.0.1")
}
or by bringing them into scope:

// dependencies.gradle.kts
val implementation by configurations
val testImplementation by configurations
val androidTestImplementation by configurations
dependencies {
    implementation("com.android.support:appcompat-v7:27.1.1")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk7:1.2.30")
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.2.30")
    testImplementation("junit:junit:4.12")
    androidTestImplementation("com.android.support.test:runner:1.0.1")
    androidTestImplementation("com.android.support.test.espresso:espresso-core:3.0.1")
}
```
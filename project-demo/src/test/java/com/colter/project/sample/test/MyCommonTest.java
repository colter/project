package com.colter.project.sample.test;

import com.colter.project.util.file.FileUtil;
import org.junit.jupiter.api.Test;


import java.io.File;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class MyCommonTest {
    @Test
    public void testDiffFile() {
        List<String> files = FileUtil.getAllAbsolutePathUnderFolderEndWith("/media/liangchao/文档1/", "pdf");
        Set<String> set1 = new TreeSet<>(files);

        List<String> files2 = FileUtil.getAllAbsolutePathUnderFolderEndWith("/home/liangchao/Documents/pdf/", "pdf");
        Set<String> set2 = new TreeSet<>(files2);

        Set<String> set11 = set1.stream().map(s -> {
            String name = new File(s).getName();
            return name;
        }).collect(Collectors.toSet());

        Set<String> set22 = set2.stream().map(s -> {
            String name = new File(s).getName();
            return name;
        }).collect(Collectors.toSet());

        System.out.println(set1.size() + "  " + set2.size());
        List<String> l1 = set1.stream().filter(s -> !set22.contains(new File(s).getName())).collect(Collectors.toList());
        List<String> l2 = set2.stream().filter(s -> !set11.contains(new File(s).getName())).collect(Collectors.toList());

        System.out.println(l1.size());

        System.out.println(l1);
//        l1.forEach(l -> {
//            FileUtil.copyFileToFolder(l,"/media/liangchao/文档1");
//        });
        System.out.println(l2.size());
        System.out.println("2有1没有：" + l2);

    }

}

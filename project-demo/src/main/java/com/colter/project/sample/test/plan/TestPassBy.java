package com.colter.project.sample.test.plan;

import java.util.ArrayList;
import java.util.List;

public class TestPassBy {
    //TODO remove mainTue Dec 31 13:13:21 CST 2019
//========
     //public static void main(String[] args) {
         //
         //System.out.println("----------int------------");
         //int a = 0;
         //System.out.println("before change:" + a);
         //changeNum(a);
         //System.out.println("after change:" + a);
         //
         //System.out.println("-----------list-----------");
         //List<String> list = new ArrayList<>();
         //list.add("a");
         //System.out.println("before change:" + list);
         //changeList(list);
         //System.out.println("after change:" + list);
         //
         //System.out.println("----------string------------");
         //String str = "abc";
         //System.out.println("before change:" + str);
         //changeString(str);
         //System.out.println("after change:" + str);
         //
         //System.out.println("----------class------------");
         //People p = new People();
         //p.setName("aaa");
         //System.out.println("before change:" + p);
         //changeClass(p);
         //System.out.println("after change:" + p);
         //
         //System.out.println("--------string array--------------");
         //String[] arr = new String[]{"a"};
         //System.out.println("before change:" + arr);
         //printArray(arr);
         //changeArray(arr);;
         //System.out.println("after change:" + arr);
         //printArray(arr);
         //
         //System.out.println("----------people array------------");
         //People[] arr2 = new People[1];
         //System.out.println("before change:" );
         //printArray(arr2);
         //changeClassArray(arr2);;
         //System.out.println("after change:" + arr2);
         //printArray(arr2);
         //System.out.println("----------------------");
         //
 //
     //}
//========

	public static void changeNum(int o) {
		o = 222;
		System.out.println("change method:" + o);
	}

	public static void changeList(List<String> list) {
		list.add("b");
		System.out.println("change method:" + list);
	}

	public static void changeString(String str) {
		str = new String("aaaa");
		System.out.println("change method:" + str);
	}

	public static void changeClass(People p) {
		p.setName("bbb");
		System.out.println("change method:" + p);
	}
	
	public static void changeArray(String[] arr){
		arr = new String[]{"a","b","c"};
		System.out.println("change method:" + arr);
		printArray(arr);
	}
	
	public static void changeClassArray(People[] arr){
		arr = new People[2];
		People p = new People("1");
		arr[0] = p;
		System.out.println("change method:" + arr);
		printArray(arr);
	}
	
	public static void printArray(Object[] arr){
		for(Object o :arr){
			System.out.println(o);
		}
	}
	
}

class People {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "People [name=" + name + "]";
	}

	public People() {
		super();
	}

	public People(String name) {
		super();
		this.name = name;
	}
	

}

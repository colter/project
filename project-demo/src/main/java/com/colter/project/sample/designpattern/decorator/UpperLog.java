package com.colter.project.sample.designpattern.decorator;

public class UpperLog extends Decorator {

	public UpperLog(ILog log) {
		super(log);
	}

	@Override
	public void log(String msg) {
		msg = msg.toUpperCase();
		log.log(msg);
	}
	
}

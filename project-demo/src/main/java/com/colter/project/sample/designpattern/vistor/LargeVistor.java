package com.colter.project.sample.designpattern.vistor;

/**
 * @author liangchao03
 * @date 2017/7/18
 */
public class LargeVistor implements IVistor {

    @Override
    public Object visit(Student student) {
        return "Large:" + student.getName() + " " + (student.getAge() + 100);
    }

    @Override
    public Object visit(Loli loli) {
        return "Large:" + loli.getName() + " " + loli.getAge() + " Loli should not change.";
    }
}

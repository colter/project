package com.colter.project.sample.designpattern.vistor;

public interface IVistor {
	public Object visit(Student student);
	public Object visit(Loli loli);
}

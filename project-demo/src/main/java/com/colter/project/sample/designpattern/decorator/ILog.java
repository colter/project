package com.colter.project.sample.designpattern.decorator;

public interface ILog {
	public void log(String msg);
}

package com.colter.project.sample.designpattern.bridge;

public abstract class AbstractThings {
	private IPost obj;
	
	public AbstractThings(IPost post){
		this.obj = post;
	}
	
	public void post(){
		obj.post();
	}
}

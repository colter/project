package com.colter.project.sample.thread.mycurrent.part2;

/**
 * @author LC
 * 2018/2/14
 */
public class OrderedCharA implements Runnable {

    public OrderedCharA(OrderHelper helper) {
        this.helper = helper;
    }


    private OrderHelper helper;

    @Override
    public void run() {
        synchronized (helper) {
            for (int i = 0; i < helper.getNum() ; ) {
                while (helper.isA()) {
                    System.out.println("A " + Thread.currentThread().getName());
                    helper.setA(false);
                    helper.setB(true);
                    helper.notifyAll();
                    i++;
                }
                try {
                    Thread.sleep(100);
                    helper.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            System.out.println("输出A完毕。" + Thread.currentThread().getName());
            helper.notifyAll();
        }

    }
}

package com.colter.project.sample.designpattern.factory.factroy3.factory;

import com.colter.project.sample.designpattern.factory.factroy3.entity.IBus;
import com.colter.project.sample.designpattern.factory.factroy3.entity.ICar;
import com.colter.project.sample.designpattern.factory.factroy3.entity.MiddleBus;
import com.colter.project.sample.designpattern.factory.factroy3.entity.MiddleCar;

public class MiddleFactory extends AbstractFactory1 {

	@Override
	public ICar createCar() {
		return new MiddleCar();
	}

	@Override
	public IBus createBus() {
		return new MiddleBus();
	}

}

package com.colter.project.sample.designpattern.vistor;

public interface IPeople {
	public int getAge();
	public String getName();

	public Object accept(IVistor vistor);
}

package com.colter.project.sample.thread.part07.test1;

import java.util.concurrent.atomic.AtomicInteger;

public class Test11 {
    //TODO remove mainTue Dec 31 13:13:22 CST 2019
//========
     //public static void main(String[] args) {
         //ParkingCouner counter = new ParkingCouner(4);
         //Sensor1 s1 = new Sensor1(counter);
         //Sensor2 s2 = new Sensor2(counter);
         //Thread t1 = new Thread(s1);
         //Thread t2 = new Thread(s2);
         //t1.start();
         //t2.start();
         //
         //try {
             //t1.join();
             //t2.join();
         //} catch (InterruptedException e) {
             //// TODO Auto-generated catch block
             //e.printStackTrace();
         //}
         //System.out.println("Main:Number of cars:"+counter.get());
         //System.out.println("Main:End.");
     //}
//========
}

class ParkingCouner extends AtomicInteger{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int maxNumber;

	public ParkingCouner(int maxNumber) {
		super();
		set(0);
		this.maxNumber = maxNumber;
	}
	public boolean carIn(){
		for(;;){
			int value = get();
			if(value == maxNumber){
				System.out.println("ParkingCounter: The parking lot is full.");
				return false;
			}else{
				int newValue = value+1;
				boolean changed = compareAndSet(value, newValue);
				if(changed){
					System.out.println("ParkingCounter: A car has entered.");
					return true;
				}
			}
		}
	}
	
	
	public boolean carOut(){
		for(;;){
			int value = get();
			if(value == 0){
				System.out.println("ParkingCounter: The parking lot is empty.");
				return false;
			}else{
				int newValue = value - 1;
				boolean changed = compareAndSet(value, newValue);
				if(changed){
					System.out.println("ParkingCounter: A car has gone.");
					return true;
				}
			}
		}
	}
}


class Sensor1 implements Runnable{
	private ParkingCouner counter;

	public Sensor1(ParkingCouner counter) {
		super();
		this.counter = counter;
	}


	@Override
	public void run() {
		counter.carIn();	
		counter.carIn();
		counter.carIn();
		counter.carIn();
		counter.carIn();
		counter.carOut();
		counter.carOut();
		counter.carOut();
		
		counter.carIn();
		counter.carIn();
		counter.carIn();
		counter.carIn();
		
	}
}

class Sensor2 implements Runnable{
	private ParkingCouner counter;

	public Sensor2(ParkingCouner counter) {
		super();
		this.counter = counter;
	}


	@Override
	public void run() {
		counter.carIn();	
		counter.carOut();
		counter.carOut();
		counter.carOut();
		counter.carIn();
		counter.carOut();
		counter.carOut();
		counter.carOut();
		
		counter.carIn();
		counter.carIn();
		counter.carIn();
		counter.carIn();
		
	}
}

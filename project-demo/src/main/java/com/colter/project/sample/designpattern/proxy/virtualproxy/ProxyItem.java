package com.colter.project.sample.designpattern.proxy.virtualproxy;

public class ProxyItem implements IItem {
	private RealItem realItem = new RealItem();
	private boolean isFillItem;

	@Override
	public String getAccount() {
		return realItem.getAccount();
	}

	@Override
	public void setAccount(String account) {
		realItem.setAccount(account);
	}

	@Override
	public String getName() {
		return realItem.getName();
	}

	@Override
	public void setName(String name) {
		realItem.setName(name);
	}

	@Override
	public String getProject() {
		return realItem.getProject();
	}

	@Override
	public void setProject(String project) {
		realItem.setProject(project);
	}

	@Override
	public String getContent() {
		return realItem.getAccount();
	}

	@Override
	public String getPlan() {
		return realItem.getPlan();
	}

	@Override
	public void fillItem() {
		if (!isFillItem) {
			realItem.fillItem();
		}
		isFillItem = true;
	}

}

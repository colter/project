package com.colter.project.sample.designpattern.chain.filter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liangchao03
 * @date 2017/7/18
 */
public class FilterChain {
    private int cursor = 0;
    public List<Filter> filters = new ArrayList<>();
    private Servlet servlet;

    public void addFilter(Filter filter){
        filters.add(filter);
    }

    public void setServlet(Servlet servlet){
        this.servlet = servlet;
    }

    public void doFilter(String request,String response){
        if(cursor < filters.size()){
            filters.get(cursor++).doFilter(request,response,this);
        }else{
            servlet.doService(request);
        }
    }

}

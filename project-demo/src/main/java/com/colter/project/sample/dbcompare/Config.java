package com.colter.project.sample.dbcompare;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * @author liangchao03
 *         2017/10/24
 */
public class Config {
    private static Properties prop;

    static {
        prop = new Properties();
        try {
            // FileInputStream is = new FileInputStream("config.properties");
            InputStream is = Config.class.getResourceAsStream("/dbcompare.properties");
            InputStreamReader io = new InputStreamReader(is, "UTF-8");
            prop.load(io);
            is.close();
            io.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static String get(String key) {
        return (String) prop.get(key);
    }

}

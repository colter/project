package com.colter.project.sample.designpattern.proxy.jdkproxy.jdkproxy.test;

public interface UserService {
	public void addUser();
	public void delUser();
	public void updateUser();
}

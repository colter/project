package com.colter.project.sample.designpattern.proxy.jdkproxy.jdkproxy.test;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class UserDynamicProxy implements InvocationHandler {
	private Object target;

	public Object getProxy(UserService service) {
		this.target = service;
		return Proxy.newProxyInstance(target.getClass().getClassLoader(), target.getClass().getInterfaces(), this);
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		Object result = null;
		System.out.println("Dynamic Proxy Pre.");
		result = method.invoke(target, args);
		System.out.println("Dynamic Proxy After.");
		System.out.println();
		return result;
	}

}

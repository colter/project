package com.colter.project.sample.thread.mycurrent.part2;

/**
 * @author LC
 * 2018/2/14
 */
public class OrderedCharB implements Runnable {
    public OrderedCharB(OrderHelper helper) {
        this.helper = helper;
    }

    private OrderHelper helper;

    @Override
    public void run() {
        synchronized (helper) {
            for (int i = 0; i < helper.getNum(); ) {
                while (helper.isB()) {
                    System.out.println("B " + Thread.currentThread().getName());
                    helper.setB(false);
                    helper.setC(true);
                    helper.notifyAll();
                    i++;
                }
                try {
                    Thread.sleep(100);
                    helper.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            System.out.println("输出B完毕。" + Thread.currentThread().getName());
            helper.notifyAll();

        }

    }
}

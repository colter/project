package com.colter.project.sample.test;

import lombok.Data;

import java.util.LinkedList;
import java.util.List;

@Data
public class FileFinder {
    private int top;
    private int last;
    private int split;

    private int contentSize;

    private List<String> keys;
    private LinkedList<String> contents;

    private LinkedList<LinkedList<String>> result;

    public FileFinder(List<String> keys) {
        this.top = 5;
        this.last = 5;
        this.split = 5;
        this.keys = keys;
        this.contentSize = calcContentSize();
    }

    public FileFinder(int top, int last, int split, List<String> keys) {
        this.top = top;
        this.last = last;
        this.split = split;
        this.keys = keys;
        this.contentSize = calcContentSize();
    }

    private int calcContentSize() {
        return top + last + (keys.size() - 1) * split;
    }

    public void read(String line) {
        
    }
}

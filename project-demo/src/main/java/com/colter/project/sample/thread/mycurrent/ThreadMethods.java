package com.colter.project.sample.thread.mycurrent;

import com.colter.project.data.MyBlockingQueue;
import com.colter.project.sample.thread.mycurrent.part2.OrderHelper;
import com.colter.project.sample.thread.mycurrent.part2.OrderedCharA;
import com.colter.project.sample.thread.mycurrent.part2.OrderedCharB;
import com.colter.project.sample.thread.mycurrent.part2.OrderedCharC;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.IntStream;

/**
 * @author LC
 * 2018/2/17
 */
public class ThreadMethods {

    /**
     * 顺序输出ABC
     */
    public void orderPrintABC() {
        OrderHelper helper = new OrderHelper();
        helper.setA(true);
        helper.setNum(10);
        Thread a = new Thread(new OrderedCharA(helper));
        Thread b = new Thread(new OrderedCharB(helper));
        Thread c = new Thread(new OrderedCharC(helper));
        a.start();
        b.start();
        c.start();
    }

    /**
     * 顺序输出BCA
     */
    public void orderPrintBCA() {
        OrderHelper helper = new OrderHelper();
        helper.setB(true);
        helper.setNum(3);
        Thread a = new Thread(new OrderedCharA(helper));
        Thread b = new Thread(new OrderedCharB(helper));
        Thread c = new Thread(new OrderedCharC(helper));
        a.start();
        b.start();
        c.start();
    }

    private ReentrantLock lock = new ReentrantLock();
    private ReentrantLock lock2 = new ReentrantLock();


    /**
     * 重入锁，多次进入
     */
    public void testReentrantLock() {
        new Thread(() -> {
            lock.lock();
            ThreadUtils.print("Lock1");
            lock.lock();
            ThreadUtils.print("Lock2");
            lock.unlock();
            ThreadUtils.print("Unlock1");
            lock.unlock();
            ThreadUtils.print("Unlock2");

            ThreadUtils.print("Done");
        }, "A").start();

        new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            lock.lock();
            ThreadUtils.print("Lock");
            lock.unlock();
            ThreadUtils.print("Unlock");
            ThreadUtils.print("Done");
        }, "B").start();
    }

    /**
     * 测试响应中断的加锁
     */
    public void testInterrupt() {
        Thread thread1 = new Thread(() -> {
            try {
                lock.lockInterruptibly();
                Thread.sleep(1000);
                lock2.lockInterruptibly();
                ThreadUtils.print("Done.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                if (lock2.isHeldByCurrentThread()) {
                    lock2.unlock();
                }
                if (lock.isHeldByCurrentThread()) {
                    lock.unlock();
                }
            }

        });


        Thread thread2 = new Thread(() -> {
            try {
                lock2.lockInterruptibly();
                Thread.sleep(1000);
                lock.lockInterruptibly();
                ThreadUtils.print("Done2.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                if (lock.isHeldByCurrentThread()) {
                    lock.unlock();
                }
                if (lock2.isHeldByCurrentThread()) {
                    lock2.unlock();
                }
            }

        });

        thread1.start();
        thread2.start();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        thread1.interrupt();
    }

    /**
     * 限时等待
     */
    public void testWaitLimit() {
        Thread thread1 = new Thread(() -> {
            try {
                if (lock.tryLock(1000, TimeUnit.MILLISECONDS)) {
                    Thread.sleep(1000);
                    if (lock2.tryLock(1000, TimeUnit.MILLISECONDS)) {
                        lock2.lockInterruptibly();
                        ThreadUtils.print("Done.");
                    } else {
                        ThreadUtils.print("Can not get Lock2");
                    }
                } else {
                    ThreadUtils.print("Can not get Lock");
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                if (lock2.isHeldByCurrentThread()) {
                    lock2.unlock();
                }
                if (lock.isHeldByCurrentThread()) {
                    lock.unlock();
                }
            }

        });


        Thread thread2 = new Thread(() -> {
            try {
                lock2.lock();
                Thread.sleep(1000);
                lock.lock();
                ThreadUtils.print("Done2.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                if (lock.isHeldByCurrentThread()) {
                    lock.unlock();
                }
                if (lock2.isHeldByCurrentThread()) {
                    lock2.unlock();
                }
            }

        });

        thread1.start();
        thread2.start();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private ReentrantLock fairLock = new ReentrantLock(true);

    /**
     * todo
     */
    public void testFairUnFair() {
        new Thread(() -> {
            while (true) {
                try {
                    fairLock.lock();
                    ThreadUtils.print("获取锁。");
                } finally {
                    if (fairLock.isHeldByCurrentThread()) {
                        fairLock.unlock();
                    }
                }

            }
        }).start();

        new Thread(() -> {
            while (true) {
                try {
                    fairLock.lock();
                    ThreadUtils.print("获取 锁。");
                } finally {
                    if (fairLock.isHeldByCurrentThread()) {
                        fairLock.unlock();
                    }
                }

            }
        }).start();


    }

    /**
     * 测试自己写的ArrayBlockingQueue
     */
    public void testMyArrayBlockingQueue() {
        MyBlockingQueue<Integer> myBlockingQueue = new MyBlockingQueue<Integer>(10);
        new Thread(() -> {
            while (true) {
                Random random = new Random();
                int r = random.nextInt(100);
                myBlockingQueue.put(r);
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }).start();

        new Thread(() -> {
            while (true) {
                int result = myBlockingQueue.take();
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

        }).start();
    }

    public void testSemaphore() {
        Semaphore semaphore = new Semaphore(3, true);
        for (int j = 0; j < 10; j++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            new Thread(() -> {
                //for (int i = 0; i < 10; i++) {
                try {
                    semaphore.acquire();
                    ThreadUtils.print("Doing ");
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    semaphore.release();
                }
                //  }

            }, "name" + j).start();
        }


    }

    /**
     * 测试读写锁
     * 多读 一写
     * 多写 一读
     */
    public void testReadWriteLock() {
        ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

        List<Thread> lists = new ArrayList<>();
        IntStream.range(0, 9).forEach(i -> {
            Thread t = new Thread(() -> {
                readWriteLock.readLock().lock();
                ThreadUtils.print("读取1秒");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                readWriteLock.readLock().unlock();
            }, "read:" + i);
            lists.add(t);
        });
        Thread w = new Thread(() -> {
            readWriteLock.writeLock().lock();
            ThreadUtils.print(" 写入3秒");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            readWriteLock.writeLock().unlock();
        }, "write:1");
        lists.add(w);
        long t1 = System.currentTimeMillis();
        for (Thread thread : lists) {
            thread.start();
        }
        for (Thread thread : lists) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        long t2 = System.currentTimeMillis();
        System.out.println((t2 - t1) + "readwrite 多读1写");
    }

    /**
     * 多写 1读
     */
    public void testReadWriteLock2() {
        ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

        List<Thread> lists = new ArrayList<>();
        IntStream.range(0, 9).forEach(i -> {
            Thread t = new Thread(() -> {
                readWriteLock.writeLock().lock();
                ThreadUtils.print("写入3秒");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                readWriteLock.writeLock().unlock();
            }, "write:" + i);
            lists.add(t);
        });
        Thread w = new Thread(() -> {
            readWriteLock.readLock().lock();
            ThreadUtils.print(" 读取1秒");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            readWriteLock.readLock().unlock();
        }, "read:1");
        lists.add(w);
        long t1 = System.currentTimeMillis();
        for (Thread thread : lists) {
            thread.start();
        }
        for (Thread thread : lists) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        long t2 = System.currentTimeMillis();
        System.out.println((t2 - t1) + "readwrite 多写1读");
    }


    /**
     * 测试读写锁
     * 多读 一写
     * 多写 一读
     */
    public void testReadWriteLock3() {
        ReentrantLock lock = new ReentrantLock();

        List<Thread> lists = new ArrayList<>();
        IntStream.range(0, 9).forEach(i -> {
            Thread t = new Thread(() -> {
                lock.lock();
                ThreadUtils.print("reentrant 读取1秒");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                lock.unlock();
            }, "reentrant read:" + i);
            lists.add(t);
        });
        Thread w = new Thread(() -> {
            lock.lock();
            ThreadUtils.print("reentrant 写入3秒");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            lock.unlock();
        }, "reentrant write:1");
        lists.add(w);
        long t1 = System.currentTimeMillis();
        for (Thread thread : lists) {
            thread.start();
        }
        for (Thread thread : lists) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        long t2 = System.currentTimeMillis();
        System.out.println((t2 - t1) + "reentrant 多读1写");
    }

    /**
     * 多写 1读
     */
    public void testReadWriteLock4() {
        ReentrantLock lock = new ReentrantLock();
        List<Thread> lists = new ArrayList<>();
        IntStream.range(0, 9).forEach(i -> {
            Thread t = new Thread(() -> {
                lock.lock();
                ThreadUtils.print("reentrant 写入3秒");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                lock.unlock();
            }, "reentrant write:" + i);
            lists.add(t);
        });
        Thread w = new Thread(() -> {
            lock.lock();
            ThreadUtils.print("reentrant 读取1秒");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            lock.unlock();
        }, "reentrant read:1");
        lists.add(w);
        long t1 = System.currentTimeMillis();
        for (Thread thread : lists) {
            thread.start();
        }
        for (Thread thread : lists) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        long t2 = System.currentTimeMillis();
        System.out.println((t2 - t1) + "reentrant 多写1读");
    }

    /**
     * 测试倒计时
     * 发射火箭
     */
    public void testCountDownLatch() {
        int n = 4;
        CountDownLatch countDownLatch = new CountDownLatch(n);
        new Thread(() -> {
            ThreadUtils.print("检查燃料 Start");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ThreadUtils.print("燃料就绪End");
            countDownLatch.countDown();

        }, "燃料Thread").start();

        new Thread(() -> {
            ThreadUtils.print("检查发射架 Start");
            try {
                Thread.sleep(6000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ThreadUtils.print("发射架就绪End");
            countDownLatch.countDown();

        }, "发射架Thread").start();


        new Thread(() -> {
            ThreadUtils.print("检查宇航员 Start");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ThreadUtils.print("宇航员就绪End");
            countDownLatch.countDown();

        }, "宇航员Thread").start();

        new Thread(() -> {
            ThreadUtils.print("检查武器装备 Start");
            try {
                Thread.sleep(8000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ThreadUtils.print("武器就绪 End");
            countDownLatch.countDown();

        }, "武器Thread").start();

        try {
            countDownLatch.await();
            System.out.println("全部就绪，点火发射！");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /**
     * 循环栅栏
     */
    int step = 0;

    public void testCyclicBarrier() {
        int num = 10;
        CyclicBarrier cyclicBarrier = new CyclicBarrier(num, () -> {
            if (step == 0) {
                ThreadUtils.print("门口集合完毕");
            }

            if (step == 1) {
                ThreadUtils.print("座位分配完毕");
            }

            if (step == 2) {
                ThreadUtils.print("开始考试");
            }
        });

        for (int i = 0; i < num; i++) {
            new Thread(() -> {
                try {
                    Thread.sleep((long) (Math.random() * 5000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ThreadUtils.print(String.format("当前已经到达%s,还有%s", Thread.currentThread().getName(), num - cyclicBarrier.getNumberWaiting() - 1));
                try {
                    cyclicBarrier.await();
                    step = 1;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }


                try {
                    Thread.sleep((long) (Math.random() * 5000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ThreadUtils.print(String.format("已经找到座位%s,还有%s", Thread.currentThread().getName(), num - cyclicBarrier.getNumberWaiting() - 1));

                try {
                    cyclicBarrier.await();
                    step = 2;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }

                try {
                    Thread.sleep((long) (Math.random() * 5000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ThreadUtils.print(String.format("发放试卷%s,还有%s", Thread.currentThread().getName(), num - cyclicBarrier.getNumberWaiting()-1));
                try {
                    cyclicBarrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }).start();
        }


    }

    //TODO remove mainTue Dec 31 13:13:21 CST 2019
//========
     //public static void main(String[] args) {
         //ThreadMethods methods = new ThreadMethods();
         //// methods.orderPrintABC();
         //// methods.orderPrintBCA();
         ////methods.testReentrantLock();
         ////methods.testInterrupt();
         ////methods.testWaitLimit();
         ////methods.testFairUnFair();
 ////        methods.testMyArrayBlockingQueue();
         ////methods.testSemaphore();
 //
 ////        methods.testReadWriteLock();
 ////        methods.testReadWriteLock2();
 ////        methods.testReadWriteLock3();
 ////        methods.testReadWriteLock4();
 ////        methods.testCountDownLatch();
         //methods.testCyclicBarrier();
 //
     //}
//========
}

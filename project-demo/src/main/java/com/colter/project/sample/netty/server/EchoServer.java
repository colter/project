package com.colter.project.sample.netty.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.net.InetSocketAddress;

/**
 * @author liangchao
 * creation time:  2019/12/15 10:51
 * desc:
 */

public class EchoServer {
    private final int port;

    public EchoServer(int port) {
        this.port = port;
    }

    //TODO remove mainTue Dec 31 13:13:20 CST 2019
//========
     //public static void main(String[] args) throws Exception {
 ////        if (args.length != 1) {
 ////            System.err.println("Usage:" + EchoServer.class.getSimpleName() + "<port>");
 ////            return;
 ////        }
         //int port = 9999;
         //new EchoServer(port).start();
     //}
//========

    private void start() throws Exception {
        final EchoServerOutHandler serverOutHandler = new EchoServerOutHandler();
        final EchoServerHandler serverHandler = new EchoServerHandler();
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap.group(group)
                    .channel(NioServerSocketChannel.class)
                    .localAddress(new InetSocketAddress(port))
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline().addLast(serverHandler);
                            ch.pipeline().addLast(serverOutHandler);
                        }
                    });


            ChannelFuture channelFuture = serverBootstrap.bind().sync();
            channelFuture.channel().closeFuture().sync();
        } finally {
            group.shutdownGracefully().sync();
        }
    }
}

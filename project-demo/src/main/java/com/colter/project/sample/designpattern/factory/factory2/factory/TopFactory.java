package com.colter.project.sample.designpattern.factory.factory2.factory;

import com.colter.project.sample.designpattern.factory.factory2.entity.ICar;
import com.colter.project.sample.designpattern.factory.factory2.entity.TopCar;

public class TopFactory extends AbstractFactory{

	@Override
	public ICar create() {
		return new TopCar();
	}

}

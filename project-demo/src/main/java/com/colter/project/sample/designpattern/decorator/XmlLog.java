package com.colter.project.sample.designpattern.decorator;

public class XmlLog extends Decorator {

	public XmlLog(ILog log) {
		super(log);
	}

	@Override
	public void log(String msg) {
		msg = "<xml>"+msg + "</xml>";
		log.log(msg);
	}

}

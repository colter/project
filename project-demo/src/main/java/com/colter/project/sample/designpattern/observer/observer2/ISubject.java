package com.colter.project.sample.designpattern.observer.observer2;

public interface ISubject<T> {
	public void register(IObserver<T> observer);
	public void unregister(IObserver<T> observer);
	public void notifyObservers();
}

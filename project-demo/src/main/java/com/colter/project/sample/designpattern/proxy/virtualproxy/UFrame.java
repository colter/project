package com.colter.project.sample.designpattern.proxy.virtualproxy;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;

public class UFrame extends JFrame implements MouseListener {
	private static final long serialVersionUID = 1L;
	ManageItem manage = new ManageItem();
	JTable table;
	JTextArea t = new JTextArea();
	JTextArea t2 = new JTextArea();

	public void init() {
		setLayout(null);
		manage.firstSearch();
		String[] title = { "账号", "姓名", "项目名称" };
		String[][] data = null;
		Vector<ProxyItem> v = manage.v;
		data = new String[v.size()][title.length];
		for (int i = 0; i < data.length; i++) {
			ProxyItem proxy = v.get(i);
			data[i][0] = proxy.getAccount();
			data[i][1] = proxy.getName();
			data[i][2] = proxy.getProject();
		}

		table = new JTable(data, title);
		JScrollPane pane = new JScrollPane(table);
		pane.setBounds(10, 10, 200, 340);
		JLabel label = new JLabel("项目主要内容");
		JLabel label2 = new JLabel("计划");
		label.setBounds(230, 5, 100, 20);
		;
		t.setBounds(230, 40, 200, 100);
		label2.setBounds(230, 160, 100, 20);
		t2.setBounds(230, 195, 200, 100);
		;
		add(pane);
		add(label);
		add(t);
		add(label2);
		add(t2);

		table.addMouseListener(this);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500, 400);
		setVisible(true);

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		try {
			int n = table.getSelectedRow();
			if (n >= 0) {
				ProxyItem item = manage.v.get(n);
				item.fillItem();
				t.setText(item.getContent());
				t2.setText(item.getPlan());
			}
		} catch (Exception e2) {
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

    //TODO remove mainTue Dec 31 13:13:20 CST 2019
//========
     //public static void main(String[] args) {
         //new UFrame().init();
     //}
//========
}

package com.colter.project.sample.event;

import java.util.EventListener;

/**
 * @author liangchao03
 * @date 2017/7/24
 */
public class ColterListener implements EventListener {
    private String name;

    public ColterListener(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void doEvent(ColterEvent event) {
        Object source = event.getSource();
        System.out.println("Listener handle event.Source:" + source + " Listener:" + name);
    }
}

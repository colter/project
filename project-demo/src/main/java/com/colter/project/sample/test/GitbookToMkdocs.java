package com.colter.project.sample.test;

import com.colter.project.util.file.FileUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;

/**
 * @author liangchao03
 * 2018/3/9
 */
public class GitbookToMkdocs {
    private List<String> replaceString = newArrayList("*", "[", "]", "(", ")");

    //TODO remove mainTue Dec 31 13:13:20 CST 2019
//========
     //public static void main(String[] args) {
         //List<String> ignoreFolders = newArrayList(".git", "_book");
         //String computer = "C:/Users/liangchao03/";
         //String gitbook = computer+"books";
         //String mkdocsPre = computer+"books-mkdocs";
         //String mkdocs = mkdocsPre+"/docs";
         //String yml = mkdocsPre + "/mkdocs.yml";
         //String summary = gitbook + "/SUMMARY.md";
 //
         //GitbookToMkdocs gm = new GitbookToMkdocs();
         //List<Link> links = gm.parseSummary(summary);
         //String prefix = "site_name: MkDocs\n";
         //String result = gm.parseLinksToYml(links);
         //String suffix =
                 //"#theme: material\n" +
                         //"#theme: material\n" +
                         //"# extra:\n" +
                         //"#   repo_icon: 'gitlab'\n" +
                         //"#   feature:\n" +
                         //"#     tabs: true\n" +
                         //"#   palette:\n" +
                         //"#     primary: 'indigo'\n" +
                         //"#     accent: 'light blue'\n" +
                         //"\n" +
                         //"theme:\n" +
                         //"    name: 'material'\n" +
                         //"    language: 'en'\n" +
                         //"    feature:\n" +
                         //"        tabs: false\n" +
                         //"    palette:\n" +
                         //"        primary: 'teal'\n" +
                         //"        accent: 'teal'\n" +
                         //"    font:\n" +
                         //"        text: 'Roboto'\n" +
                         //"        code: 'Roboto Mono'\n" +
                         //"\n" +
                         //"# Extensions\n" +
                         //"markdown_extensions:\n" +
                         //"  - admonition\n" +
                         //"  - codehilite(guess_lang=false, linenums=true)\n" +
                         //"  - toc(permalink=true)";
         //result = prefix + result + suffix;
 //
         //FileUtil.copyFolderIgnoreFolder(gitbook, mkdocs, ignoreFolders);
         //FileUtil.writeFile(result, yml);
         //FileUtil.copyFile(gitbook+"/README.md",mkdocs+"/index.md");
     //}
//========

    public List<Link> parseSummary(String path) {
        List<String> lines = FileUtil.readFileByLine(path);
        List<String> results = lines.stream().filter(line -> !line.contains("#") && !line.trim().equals("")).collect(Collectors.toList());
        return parseGitbookSummary(results);
    }

    public String parseLinksToYml(List<Link> links) {
        StringBuffer sb = new StringBuffer("pages:\n");
        for (Link link : links) {
            int index = 0;
            append(link, index, sb);
        }
        return sb.toString();
    }

    private void append(Link link, int index, StringBuffer sb) {
        sb.append(getBlank(index) + "- " + link.getTitle() + ": " + (link.hasChild() ? "" : link.getLink()) + "\n");
        if (link.hasChild()) {
            index++;
            for (Link child : link.getChildren()) {
                append(child, index, sb);
            }
        }
    }

    private String getBlank(int index) {
        StringBuffer sb = new StringBuffer();
        int total = (index + 1) * 4;
        for (int i = 0; i < total; i++) {
            sb.append(" ");
        }
        return sb.toString();
    }

    private List<Link> parseGitbookSummary(List<String> lines) {
        List<Link> result = new ArrayList<>();
        Link base = new Link();
        base.setChildren(result);

        List<Integer> index = new ArrayList<>();
        for (String line : lines) {
            Link link = parseToLink(line);
            int head = getIndex(line);
            if (!index.contains(head)) {
                index.add(head);
                Collections.sort(index);
            }
            add(base, link, index.indexOf(head));
        }
        return result;
    }

    private void add(Link base, Link link, int i) {
        base.getNumChildren(i).add(link);
    }

    private int getIndex(String line) {
        return line.indexOf("*");
    }

    private Link parseToLink(String line) {
        Link link = new Link();
        int index = getIndex(line);
        if (index == 0) {
            link.setFirst(true);
        }
        Pattern pattern = Pattern.compile("]\\s*\\(");
        Matcher matcher = pattern.matcher(line);
        if (matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();
            link.setTitle(format(line.substring(0, start + 1)));
            link.setLink(format(line.substring(end - 1, line.length())));
        }
        return link;
    }

    private String format(String old) {
        String result = old;
        for (String format : replaceString) {
            result = result.replace(format, "");
        }
        return result.trim();
    }
}


class Link {
    private boolean isFirst;
    private List<Link> children = new ArrayList<>();
    private String title;
    private String link;

    public List<Link> getNumChildren(int i) {
        Link start = this;
        List<Link> child = null;
        for (int j = 0; j <= i; j++) {
            if (start == null) {
                throw new RuntimeException("Has not so many level");
            }
            child = start.getChildren();
            if (child.size() > 0) {
                start = child.get(child.size() - 1);
            } else {
                start = null;
            }
        }
        return child;
    }

    private Link getNumChild(int i) {
        List<Link> child = null;
        for (int j = 0; j < i; j++) {
            child = this.getChildren();
        }
        if (child == null || child.size() == 0) {
            return null;
        } else {
            return child.get(child.size() - 1);
        }
    }

    public boolean isFirst() {
        return isFirst;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }

    public boolean hasChild() {
        return this.children.size() > 0;
    }

    public List<Link> getChildren() {
        return children;
    }

    public void setChildren(List<Link> children) {
        this.children = children;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void addChild(Link link) {
        this.getChildren().add(link);
    }

    @Override
    public String toString() {
        return "Link{" +
                "title='" + title + '\'' +
                ", link='" + link + '\'' +
                '}';
    }
}

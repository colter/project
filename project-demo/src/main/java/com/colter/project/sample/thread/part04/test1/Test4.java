package com.colter.project.sample.thread.part04.test1;

import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

//执行器中延时执行任务 ScheduledThreadPoolExecutor
public class Test4 {
    //TODO remove mainTue Dec 31 13:13:21 CST 2019
//========
     //public static void main(String[] args) {
         //ScheduledThreadPoolExecutor executor = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(1);
         //System.out.println("Main: start.");
         //for (int i = 0; i < 10; i++) {
             //Task4 task = new Task4(i+"");
             //executor.schedule(task, i*2, TimeUnit.SECONDS);
         //}
         //executor.shutdown();
         //
         //try {
             //executor.awaitTermination(1, TimeUnit.HOURS);
         //} catch (InterruptedException e) {
             //// TODO Auto-generated catch block
             //e.printStackTrace();
         //}
         //System.out.println("Main: Ends at "+new Date());
     //}
//========
}

class Task4 implements Callable<String> {

	private String name;

	public Task4(String name) {
		super();
		this.name = name;
	}

	@Override
	public String call() throws Exception {
		System.out.println(name + " start at:" + new Date());
		return "Hello " + name;
	}

}

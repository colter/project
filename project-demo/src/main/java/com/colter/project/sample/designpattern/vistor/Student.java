package com.colter.project.sample.designpattern.vistor;

public class Student implements IPeople {
	int age;
	String name;

	@Override
	public int getAge() {
		return this.age;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public Object accept(IVistor vistor) {
		return vistor.visit(this);
	}
}

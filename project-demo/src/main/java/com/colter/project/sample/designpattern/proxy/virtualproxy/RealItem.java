package com.colter.project.sample.designpattern.proxy.virtualproxy;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RealItem implements IItem {
	private String account;
	private String name;
	private String project;
	private String content;
	private String plan;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	@Override
	public void fillItem() {
		System.out.println("耗时操作：" + this.account);
		String sql = "select * from project_proxy where account = ?";
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "root");
			ps = conn.prepareStatement(sql);
			ps.setString(1, this.account);
			rs = ps.executeQuery();
			if (rs.next()) {
				this.account = rs.getString(1);
				this.name = rs.getString(2);
				this.project = rs.getString(3);
				this.content = rs.getString(4);
				this.plan = rs.getString(5);
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (ps != null) {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public String toString() {
		return "RealItem [account=" + account + ", name=" + name + ", project=" + project + ", content=" + content
				+ ", plan=" + plan + "]";
	}

}

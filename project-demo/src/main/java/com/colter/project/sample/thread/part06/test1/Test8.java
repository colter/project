package com.colter.project.sample.thread.part06.test1;

import java.util.concurrent.atomic.AtomicLong;

//原子变量
public class Test8 {
    //TODO remove mainTue Dec 31 13:13:22 CST 2019
//========
     //public static void main(String[] args) {
         //Account account = new Account();
         //account.setBalance(1000);
 //
         //Company company = new Company(account);
         //Thread companyThread = new Thread(company);
 //
         //Bank bank = new Bank(account);
         //Thread bankThread = new Thread(bank);
         //System.out.println("Account: init:" + account.getBalance());
 //
         //companyThread.start();
         //bankThread.start();
 //
         //try {
             //companyThread.join();
             //bankThread.join();
             //System.out.println("Account:Final:" + account.getBalance());
 //
         //} catch (InterruptedException e) {
             //e.printStackTrace();
         //}
 //
     //}
//========
}

class Account {
	private AtomicLong balance;

	public Account() {
		super();
		balance = new AtomicLong();
	}

	public long getBalance() {
		return balance.get();
	}

	public void setBalance(long balance) {
		this.balance.set(balance);
	}

	public void addAmount(long amount) {
		this.balance.getAndAdd(amount);
	}

	public void substactAmount(long amount) {
		this.balance.getAndAdd(-amount);
	}
}

class Company implements Runnable {
	private Account account;

	public Company(Account account) {
		super();
		this.account = account;
	}

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {

			account.addAmount(1000);
		}
	}

}

class Bank implements Runnable {

	private Account account;

	public Bank(Account account) {
		super();
		this.account = account;
	}

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			account.substactAmount(1000);
		}

	}

}

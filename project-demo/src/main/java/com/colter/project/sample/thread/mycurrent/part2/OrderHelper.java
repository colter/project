package com.colter.project.sample.thread.mycurrent.part2;

import lombok.Data;

/**
 * @author LC
 * 2018/2/17
 */
@Data
public class OrderHelper {
    private boolean isA;
    private boolean isB;
    private boolean isC;
    private int num;
}

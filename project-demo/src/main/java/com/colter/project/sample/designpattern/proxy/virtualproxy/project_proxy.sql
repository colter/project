/*
Navicat MySQL Data Transfer

Source Server         : LocalHost
Source Server Version : 50528
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50528
File Encoding         : 65001

Date: 2016-01-25 15:06:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for project_proxy
-- ----------------------------
DROP TABLE IF EXISTS `project_proxy`;
CREATE TABLE `project_proxy` (
  `account` varchar(10) DEFAULT NULL COMMENT '账号',
  `name` varchar(25) DEFAULT NULL COMMENT '主持人姓名',
  `project` varchar(255) DEFAULT NULL COMMENT '项目名称',
  `content` varchar(255) DEFAULT NULL COMMENT '项目主要内容',
  `plan` varchar(255) DEFAULT NULL COMMENT '项目计划'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_proxy
-- ----------------------------
INSERT INTO `project_proxy` VALUES ('1000', 'A', '电脑开发', '研究新一代的电脑，研究很多电脑问题', '1月1号：开始 <br/> 1月2号：结束');
INSERT INTO `project_proxy` VALUES ('1001', 'B', '数学研究', '数学研究，提高效率', '1月3号：开始 <br/> 1月4号：结束');
INSERT INTO `project_proxy` VALUES ('1002', 'C', '化学实验', '做一些化学实验', '1月5号：开始 <br/> 1月6号：结束');

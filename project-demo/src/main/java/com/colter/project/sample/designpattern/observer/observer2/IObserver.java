package com.colter.project.sample.designpattern.observer.observer2;

public interface IObserver<T> {
	public void refresh(T data);
}

package com.colter.project.sample.dbcompare;

/**
 * @author liangchao03
 *         2017/10/24
 */
public class CompareConstant {
    public static final String DB1_DRIVER = "db1.driver";
    public static final String DB1_URL = "db1.url";
    public static final String DB1_USERNAME = "db1.username";
    public static final String DB1_PASSWORD = "db1.password";
    public static final String DB1_SQL = "db1.sql";

    public static final String DB2_DRIVER = "db2.driver";
    public static final String DB2_URL = "db2.url";
    public static final String DB2_USERNAME = "db2.username";
    public static final String DB2_PASSWORD = "db2.password";
    public static final String DB2_SQL = "db2.sql";

    public static final String DB_TYPES = "db.types";
    public static final String DB_RULES = "db.rules";
    public static final String DB_RESULT_PATH = "db.result.path";

}

package com.colter.project.sample.dbcompare;

import java.io.*;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by LC on 2017/10/24.
 */
public class DBCompareUtil {
    private static List<CompareResult> compareResultList = new ArrayList<>();

    public static void compareAndWriteFile(List<List<Object>> result1, List<List<Object>> result2) {
        boolean aBigger = result1.size() > result2.size();
        boolean bBigger = result1.size() < result2.size();
        int aSize = result1.size();
        int bSize = result2.size();

        int size = aSize;
        if (bBigger) {
            size = bSize;
        }

        for (int i = 0; i < size; i++) {
            if (aBigger) {
                if (i >= bSize) {
                    compare(result1.get(i), null);
                } else {
                    compare(result1.get(i), result2.get(i));
                }
            } else if (bBigger) {
                if (i >= aSize) {
                    compare(null, result2.get(i));
                } else {
                    compare(result1.get(i), result2.get(i));
                }
            } else {
                compare(result1.get(i), result2.get(i));
            }

        }

        appendToFile();
    }

    private static void compare(List<Object> list1, List<Object> list2) {
        CompareResult compareResult = new CompareResult(list1, list2);
        compareResult.compare();
        compareResultList.add(compareResult);
    }

    private static void appendToFile() {
        File file = new File(Config.get(CompareConstant.DB_RESULT_PATH));
        if (!file.exists()) {
            file.mkdirs();
        }
        File result = new File(file.getAbsolutePath() + "\\" + ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd-HHmmss")) + ".csv");
        OutputStreamWriter writer = null;
        try {
            writer = new OutputStreamWriter(new FileOutputStream(result),"utf-8");
            writer.write(title());
            writer.write("\r\n");
            for (int i = 0; i < compareResultList.size(); i++) {
                writer.write(compareResultList.get(i).toCsv());
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }


    }

    private static String title() {
        CompareResult r = compareResultList.get(0);
        int size = r.getList1() == null?r.getList2().size():r.getList1().size();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            sb.append("A" + (i + 1) + ",");
        }
        for (int i = 0; i < size; i++) {
            sb.append("B" + (i + 1) + ",");
        }
        sb.append("Different Index");
        return sb.toString();
    }

}

class CompareResult {

    private List<Object> list1;
    private List<Object> list2;
    private List<Integer> errorIndex = new ArrayList<>();


    public List<Object> getList1() {
        return list1;
    }

    public void setList1(List<Object> list1) {
        this.list1 = list1;
    }

    public List<Object> getList2() {
        return list2;
    }

    public void setList2(List<Object> list2) {
        this.list2 = list2;
    }

    public List<Integer> getErrorIndex() {
        return errorIndex;
    }

    public void setErrorIndex(List<Integer> errorIndex) {
        this.errorIndex = errorIndex;
    }

    public CompareResult(List<Object> list1, List<Object> list2) {
        this.list1 = list1;
        this.list2 = list2;
    }

    public String toCsv() {
        StringBuilder sb = new StringBuilder();
        if (list1 == null) {
            list1 = new ArrayList<>();
            list2.stream().forEach(item -> list1.add(""));
        }
        if (list2 == null) {
            list2 = new ArrayList<>();
            list1.stream().forEach(item -> list2.add(""));
        }
        list1.stream().forEach(item -> sb.append(item == null ? " " : item + ","));
        list2.stream().forEach(item -> sb.append(item == null ? " " : item + ","));
        if (errorIndex.size() != 0) {
            errorIndex.stream().forEach(item -> sb.append(item + " "));
        }
        sb.append("\r\n");

        return sb.toString();
    }

    public void compare() {
        if (list1 == null) {
            for (int i = 0; i < list2.size(); i++) {
                this.errorIndex.add(i + 1);
            }
        } else if (list2 == null) {
            for (int i = 0; i < list1.size(); i++) {
                this.errorIndex.add(i + 1);
            }
        } else {
            for (int i = 0; i < list1.size(); i++) {
                String type = CompareConfig.types.get(i);
                if (type.equalsIgnoreCase("int")) {
                    Integer int1 = Integer.parseInt(String.valueOf(list1.get(i)));
                    Integer int2 = Integer.parseInt(String.valueOf(list2.get(i)));
                    boolean equals = int1.intValue() == int2.intValue();
                    if (!equals) {
                        this.errorIndex.add(i + 1);
                    }
                } else if (type.equalsIgnoreCase("string")) {
                    String str1 = String.valueOf(list1.get(i));
                    String str2 = String.valueOf(list2.get(i));
                    boolean equals = str1.equals(str2);
                    if (!equals) {
                        this.errorIndex.add(i + 1);
                    }
                } else if(type.equalsIgnoreCase("ignore")){
                    //忽略字段，认为相等
                }
            }
        }
    }
}

package com.colter.project.sample.designpattern.chain.filter;

/**
 * @author liangchao03
 * @date 2017/7/18
 */
public class LowerCaseFilter extends  Filter {

    @Override
    public void doFilter(String request, String response, FilterChain filterChain) {
        request = request.toUpperCase();
        filterChain.doFilter(request,response);
    }
}

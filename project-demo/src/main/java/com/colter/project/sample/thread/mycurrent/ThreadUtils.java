package com.colter.project.sample.thread.mycurrent;

import org.apache.commons.lang.StringUtils;

/**
 * @author LC
 * 2018/2/17
 */
public class ThreadUtils {

    public static void print(String... args) {
        System.out.println(StringUtils.join(args, " ") + " " + Thread.currentThread().getName());
    }
}

package com.colter.project.sample.designpattern.vistor;

/**
 * @author liangchao03
 * @date 2017/7/18
 */
public class Loli implements  IPeople{
    int age;
    String name;
    @Override
    public int getAge() {
        return this.age;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Object accept(IVistor vistor) {
        return vistor.visit(this);
    }
}

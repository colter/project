package com.colter.project.sample.designpattern.observer.observer2;

public class Observer2<T> implements IObserver<T> {

	@Override
	public void refresh(T data) {
		System.out.println("Oberser two :I received data:"+data);
	}

}

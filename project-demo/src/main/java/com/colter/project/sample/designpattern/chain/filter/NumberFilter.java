package com.colter.project.sample.designpattern.chain.filter;

/**
 * @author liangchao03
 * @date 2017/7/18
 */
public class NumberFilter  extends  Filter{

    @Override
    public void doFilter(String request, String response, FilterChain filterChain) {
        request = request.replaceAll("\\d","");
        filterChain.doFilter(request,response);
    }
}

package com.colter.project.sample.thread.part07.test1;

import java.util.Date;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

//ThreadFactory接口生成定制线程
public class Test4 {
    //TODO remove mainTue Dec 31 13:13:22 CST 2019
//========
     //public static void main(String[] args) {
         //MyThreadFactory myFactory = new MyThreadFactory("MyThreadFactory");
         //MyTask task = new MyTask();
         //Thread thread = myFactory.newThread(task);
         //thread.start();
         //try {
             //thread.join();
         //} catch (InterruptedException e) {
             //e.printStackTrace();
         //}
         //System.out.println("Main:Thread information.");
         //System.out.println(thread);
         //System.out.println("Mian:End.");
     //}
//========
}

class MyThread extends Thread {
	private Date creationDate;
	private Date startDate;
	private Date finishDate;

	public MyThread(Runnable target, String name) {
		super(target, name);
		setCreationDate();
	}

	@Override
	public void run() {
		setStartDate();
		super.run();
		setFinishDate();
	}

	public void setCreationDate() {
		creationDate = new Date();
	}

	public void setStartDate() {
		startDate = new Date();
	}

	public void setFinishDate() {
		finishDate = new Date();
	}

	public long getExecutionTime() {
		return finishDate.getTime() - startDate.getTime();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getName());
		sb.append(":");
		sb.append("Create Date:" + creationDate);
		sb.append(" : Running time:" + getExecutionTime());
		sb.append(" Milliseconds:");
		return sb.toString();
	}
}

class MyThreadFactory implements ThreadFactory {
	private int counter;
	private String prefix;

	public MyThreadFactory(String prefix) {
		super();
		this.prefix = prefix;
		counter = 1;
	}

	@Override
	public Thread newThread(Runnable r) {

		MyThread myThread = new MyThread(r, prefix + "-" + counter);
		counter++;
		return myThread;
	}

}

class MyTask implements Runnable {

	@Override
	public void run() {
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}

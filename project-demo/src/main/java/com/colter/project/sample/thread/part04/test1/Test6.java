package com.colter.project.sample.thread.part04.test1;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

//执行器取消任务
public class Test6 {
    //TODO remove mainTue Dec 31 13:13:21 CST 2019
//========
     //public static void main(String[] args) {
         //ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
         //Task6 task = new Task6();
         //Future<?> result = executor.submit(task);
         //System.out.println("Mian: Cacelling the task. ");
         //result.cancel(true);
         //System.out.println("Main: cancelled:" + result.isCancelled());
         //System.out.println("Main: done:" + result.isDone());
 //
         //executor.shutdown();
         //System.out.println("Main: executor finished.");
 //
     //}
//========
}

class Task6 implements Runnable {

	@Override
	public void run() {
		while (true) {
			System.out.println("Task test.");
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				System.out.println("Task exception");
				e.printStackTrace();
			}
		}
	}

}

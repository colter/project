package com.colter.project.sample.test.execute;


import com.colter.project.util.excel.ExcelUtil;
import com.colter.project.util.file.FileUtil;
import com.colter.project.util.ip.IPUtil;
import com.colter.project.util.qrcode.QRCodeUtil;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ExcelToQRCode {
    //TODO remove mainTue Dec 31 13:13:20 CST 2019
//========
     //public static void main(String[] args) {
         //String qrPre = "D:\\OneDrive\\邮箱\\QRCode\\";
         //String prefix = "D:\\OneDrive\\邮箱\\";
         //String qrSuffix = ".png";
         //String suffix = ".xlsx";
         //String fileName = "账号密码";
 //
         //String mac = IPUtil.getLocalMac();
         //if (!mac.equalsIgnoreCase("64-51-06-5A-A0-67")) {
             //qrPre = "F:\\OneDrive\\邮箱\\QRCode\\";
             //prefix = "F:\\OneDrive\\邮箱\\";
         //}
 //
         //SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
         //String dateStr = sd.format(new Date());
         //String backUp = prefix + dateStr + suffix;
 //
         //String filePath = prefix + fileName + suffix;
 //
         //if (!new File(backUp).exists()) {
             //FileUtil.copyFile(filePath, backUp);
         //}
 //
         //List<List<Object>> list = ExcelUtil.changeExcelToList(filePath, 2);
 //
         //for (List<Object> temp : list) {
             //User user = new User();
             //user.setAppName(String.valueOf(temp.get(0)));
             //user.setLoginName(String.valueOf(temp.get(1)));
             //user.setNickName(String.valueOf(temp.get(2)));
             //user.setPassword(String.valueOf(temp.get(3)));
             //user.setMarks(String.valueOf(temp.get(4)));
             //user.setEmail(String.valueOf(temp.get(5)));
             //user.setMobile(String.valueOf(temp.get(6)));
             //user.setAnswer(String.valueOf(temp.get(7)));
             //user.setOthers(String.valueOf(temp.get(8)));
             //String path = qrPre + user.getAppName() + qrSuffix;
             //QRCodeUtil.createQRCode(user.toString(), path);
         //}
 //
     //}
//========
}

class User {
	private String appName;
	private String loginName;
	private String nickName;
	private String password;
	private String marks;
	private String email;
	private String mobile;
	private String answer;
	private String others;

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMarks() {
		return marks;
	}

	public void setMarks(String marks) {
		this.marks = marks;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getOthers() {
		return others;
	}

	public void setOthers(String others) {
		this.others = others;
	}

	@Override
	public String toString() {
		return "应用 : " + appName + "\r\n用户名 : " + loginName + "\r\n昵称 : " + nickName + "\r\n密码:" + password
				+ "\r\n备注 : " + marks + "\r\n安全邮箱 : " + email + "\r\n安全手机 : " + mobile + "\r\n问答:" + answer
				+ "\r\n其他 : " + others;
	}

}

package com.colter.project.sample.designpattern.proxy.jdkproxy.jdkproxy;

/**
 * @author liangchao03
 * @date 2017/8/10
 */
public class MethodMapper {
    public void m1(){
        System.out.println("m1");
    }
    public void m2(){
        System.out.println("m2");
    }
}

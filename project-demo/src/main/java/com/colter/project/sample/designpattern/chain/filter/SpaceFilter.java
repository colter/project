package com.colter.project.sample.designpattern.chain.filter;

/**
 * @author liangchao03
 * @date 2017/7/18
 */
public class SpaceFilter  extends  Filter{
    @Override
    public void doFilter(String request, String response, FilterChain filterChain) {
        if(request.indexOf(" ")!= -1){
            request = request.replaceAll(" ","");
        }
        filterChain.doFilter(request,response);
    }
}

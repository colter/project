package com.colter.project.sample.test.plan.anno;

import java.lang.reflect.Field;

public class TestField {
	
	public TestField(){
		Class<TestField> c = TestField.class;
		Field[] fields = c.getDeclaredFields();
		for(Field f : fields){
			if(f.isAnnotationPresent(AnnoField.class)){
				AnnoField annoField = f.getAnnotation(AnnoField.class);
				String value = annoField.value();
				this.name = value;
			}
		}
	}
	
	@AnnoField("My Name")
	public String name;

}

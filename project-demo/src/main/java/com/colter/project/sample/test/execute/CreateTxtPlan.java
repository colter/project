package com.colter.project.sample.test.execute;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
/**
 * 英语计划表 生成txt原文本。
 * @author user
 *
 */
public class CreateTxtPlan {
    //TODO remove mainTue Dec 31 13:13:20 CST 2019
//========
     //public static void main(String[] args) {
         //List<String> contents = getContents();
         //try {
             //RandomAccessFile randomFile = new RandomAccessFile("c:\\plan.txt", "rw");
             //long fileLenght = randomFile.length();
             //randomFile.seek(fileLenght);
             //for (String content : contents) {
                 //// randomFile.writeBytes(content + "\r\n");
                 //randomFile.write((content + "\r\n").getBytes("utf-8"));
             //}
             //randomFile.close();
         //} catch (FileNotFoundException e) {
             //e.printStackTrace();
         //} catch (IOException e) {
             //e.printStackTrace();
         //}
 //
         //System.out.println("Done.");
     //}
//========

	public static List<String> getContents() {
		List<String> list = new ArrayList<>();
		// String start = "03-10";
		// String start2 = "20161222";
		// int k = 132;

		String start = "10-17";
		String start2 = "20161011";
		int k = 60;
		// String content = "专四词汇第132天 file new\\专业四级英语132_20161222.docx";
		SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sd2 = new SimpleDateFormat("MM-dd");

		try {
			Date date = sd.parse("2017-" + start);
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);

			SimpleDateFormat sd3 = new SimpleDateFormat("yyyyMMdd");
			Date date2 = sd3.parse(start2);
			Calendar cal2 = Calendar.getInstance();
			cal2.setTime(date2);

			for (int i = 0; i < 122; i++) {
				String dateStr = sd2.format(cal.getTime());
				String content = "专四词汇第" + (k + i) + "天  file new\\专业四级英语" + (k + i) + "_" + sd3.format(cal2.getTime())
						+ ".docx";
				list.add(dateStr);
				list.add(content);

				cal.add(Calendar.DAY_OF_YEAR, 2);
				cal2.add(Calendar.DAY_OF_YEAR, 1);
			}

		} catch (ParseException e) {
			e.printStackTrace();
		}

		return list;
	}
}

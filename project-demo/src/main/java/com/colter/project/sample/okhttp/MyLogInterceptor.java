package com.colter.project.sample.okhttp;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * @author liangchao03
 * 2019/1/9
 */
public class MyLogInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        long start = System.currentTimeMillis();
        Response response = chain.proceed(request);
        long end = System.currentTimeMillis();
        System.out.println(end - start);
        System.out.println("Request:" + request.url());
        System.out.println("Header:" + request.headers());
        System.out.println("Response:" + response.peekBody(Long.MAX_VALUE).string());
        return response;
    }
}

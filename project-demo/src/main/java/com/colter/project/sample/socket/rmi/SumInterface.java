package com.colter.project.sample.socket.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface SumInterface extends Remote {
	public int getSum(int a, int b) throws RemoteException;
}

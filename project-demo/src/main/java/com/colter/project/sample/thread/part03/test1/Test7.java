package com.colter.project.sample.thread.part03.test1;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Exchanger;

//并发任务间的数据交换  Exchanger
public class Test7 {
    //TODO remove mainTue Dec 31 13:13:21 CST 2019
//========
     //public static void main(String[] args) {
         //List<String> buffer1 = new ArrayList<>();
         //List<String> buffer2 = new ArrayList<>();
         //Exchanger<List<String>> exchanger = new Exchanger<>();
         //
         //Producer producer = new Producer(buffer1, exchanger);
         //Consumer consumer = new Consumer(buffer2, exchanger);
         //
         //new Thread(producer).start();
         //new Thread(consumer).start();
     //}
//========
}

class Producer implements Runnable {
	private List<String> buffer;
	private final Exchanger<List<String>> exchanger;

	public Producer(List<String> buffer, Exchanger<List<String>> exchanger) {
		super();
		this.buffer = buffer;
		this.exchanger = exchanger;
	}

	@Override
	public void run() {
		int cycle = 1;
		for (int i = 0; i < 10; i++) {
			System.out.println("Producer cycle: " + cycle);
			for (int j = 0; j < 10; j++) {
				String message = "Event" + (i * 10 + j);
				System.out.println("Producer:" + message);
				buffer.add(message);
			}
			try {
				buffer = exchanger.exchange(buffer);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			System.out.println("exchange done." + buffer.size());
			cycle++;
		}

	}
}

class Consumer implements Runnable {
	private List<String> buffer;
	private final Exchanger<List<String>> exchanger;

	public Consumer(List<String> buffer, Exchanger<List<String>> exchanger) {
		super();
		this.buffer = buffer;
		this.exchanger = exchanger;
	}

	@Override
	public void run() {
		int cycle = 1;
		for (int i = 0; i < 10; i++) {
			System.out.println("Consumer cycle:" + cycle);

			try {
				buffer = exchanger.exchange(buffer);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			System.out.println("Consumer:" + buffer.size());
			for (int j = 0; j < 10; j++) {
				String message = buffer.get(0);
				System.out.println("consumer:" + message);
				buffer.remove(0);
			}
			cycle++;
		}
	}

}

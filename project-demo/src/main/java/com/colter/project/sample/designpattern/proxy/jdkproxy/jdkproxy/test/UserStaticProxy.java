package com.colter.project.sample.designpattern.proxy.jdkproxy.jdkproxy.test;

public class UserStaticProxy implements UserService {
	private UserService service;

	public UserStaticProxy(UserService service) {
		this.service = service;
	}

	@Override
	public void addUser() {
		System.out.println("Static Proxy Pre add.");
		service.addUser();
		System.out.println("Static Proxy After add.");
		System.out.println();
	}

	@Override
	public void delUser() {
		System.out.println("Static Proxy Pre del.");
		service.delUser();
		System.out.println("Static Proxy After del.");
		System.out.println();

	}

	@Override
	public void updateUser() {
		System.out.println("Static Proxy Pre update.");
		service.updateUser();
		System.out.println("Static Proxy After update.");
		System.out.println();

	}

}

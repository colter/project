package com.colter.project.sample.event;

import java.util.EventObject;

/**
 * @author liangchao03
 * @date 2017/7/24
 */
public class ColterEvent extends EventObject {
    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public ColterEvent(Object source) {
        super(source);
    }
}

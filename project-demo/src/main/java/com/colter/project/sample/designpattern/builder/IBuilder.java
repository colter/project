package com.colter.project.sample.designpattern.builder;

import com.colter.project.sample.designpattern.builder.entity.Product;

public interface IBuilder {
	public void createUnit1();
	public void createUnit2();
	public void createUnit3();
	public Product composite();
}

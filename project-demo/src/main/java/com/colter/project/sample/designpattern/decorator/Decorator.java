package com.colter.project.sample.designpattern.decorator;

public abstract class Decorator implements ILog {
	protected ILog log;
	
	public Decorator(ILog log) {
		this.log = log;
	}
	
}

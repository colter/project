package com.colter.project.sample.test.plan.english;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import com.colter.project.util.file.FileUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class EnglishCreateTemplate {
    //TODO remove mainTue Dec 31 13:13:21 CST 2019
//========
     //public static void main(String[] args) {
         //createTemplate();
     //}
//========

	private static void createTemplate() {
		String outPath = getPrefix() + "plan.xlsx";

		Workbook wb = null;
		Sheet sheet = null;
		Row r1 = null;

		boolean isXlsx = outPath.endsWith("xlsx");
		try {
			if (isXlsx) {
				wb = new XSSFWorkbook();
			} else {
				wb = new HSSFWorkbook();
			}
			// CellStyle cellStyle = wb.createCellStyle();
			// cellStyle.setWrapText(true);
			// cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
			// cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
			// cellStyle.setBorderBottom(CellStyle.BORDER_THIN); // 下边框
			// cellStyle.setBorderLeft(CellStyle.BORDER_THIN);// 左边框
			// cellStyle.setBorderTop(CellStyle.BORDER_THIN);// 上边框
			// cellStyle.setBorderRight(CellStyle.BORDER_THIN);// 右边框

			sheet = wb.createSheet();

			Calendar c1 = getStartDate();
			Calendar c2 = getEndDate();

			SimpleDateFormat sdWeek = new SimpleDateFormat(getWeek(), geteLocale());
			SimpleDateFormat sdMonth = new SimpleDateFormat(getMonth(), geteLocale());

			int rowNum = 0;
			int planNum = getPlanNum();
			int maxPlanNum = getMaxPlanNum();
			int width = getWidth();
			int blank = getBlank();
			for(int i = 2; i < maxPlanNum + 2; i++){
				sheet.setColumnWidth(i, width);
			}
			
			while (c1.compareTo(c2) < 1) {
				r1 = createRow(sheet, rowNum);
				
				Cell cWeek = r1.createCell(0);
				Cell cDate = r1.createCell(1);
				cWeek.setCellValue(sdWeek.format(c1.getTime()));
				cDate.setCellValue(sdMonth.format(c1.getTime()));

				if (planNum > maxPlanNum) {
					planNum = 1;
				}
				int cellNum = planNum + 1;

				

				CellStyle style1 = wb.createCellStyle();
				Font font = wb.createFont();
				font.setColor(IndexedColors.RED.getIndex());
				style1.setFont(font);
				
				CellStyle style2 = wb.createCellStyle();
				Font font2 = wb.createFont();
				font2.setColor(IndexedColors.GREEN.getIndex());
				style2.setFont(font2);

				Row r2 = createRow(sheet, rowNum + 1);
				Row r4 = createRow(sheet, rowNum + 3);
				Row r7 = createRow(sheet, rowNum + 6);
				Row r15 = createRow(sheet, rowNum + 14);
				
				System.out.println( rowNum % blank);
				if (getShow() == null || (getShow() != null && getShow().intValue() == rowNum % blank) ) {
					Cell cell1 = r1.createCell(cellNum);
					cell1.setCellValue(planNum);
					cell1.setCellStyle(style1);

					Cell cell2 = r2.createCell(cellNum);
					cell2.setCellValue(planNum);
					cell2.setCellStyle(style2);

					Cell cell4 = r4.createCell(cellNum);
					cell4.setCellValue(planNum);
					cell4.setCellStyle(style2);

					Cell cell7 = r7.createCell(cellNum);
					cell7.setCellValue(planNum);
					cell7.setCellStyle(style2);

					Cell cell15 = r15.createCell(cellNum);
					cell15.setCellValue(planNum);
					cell15.setCellStyle(style2);
				}
				c1.add(Calendar.DAY_OF_YEAR, 1);
				planNum++;
				rowNum++;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (!new File(outPath).exists()) {
			FileUtil.createFileIfNotExists(outPath);
		} else {
			FileUtil.copyFile(outPath, outPath + System.currentTimeMillis() + ".bak");
		}

		// 输出Excel文件
		FileOutputStream output;
		try {
			output = new FileOutputStream(outPath);
			wb.write(output);
			output.flush();
			wb.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("test");
	}

	private static Row createRow(Sheet sheet, int rowNum) {
		Row row = sheet.getRow(rowNum);
		if (row == null) {
			row = sheet.createRow(rowNum);
		}
		return row;
	}

	private static String getPrefix() {
		String re = "c:\\";
		String path = re;
		try {
			path = URLDecoder.decode(EnglishCreateTemplate.class.getProtectionDomain().getCodeSource().getLocation().getPath(),
					"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		System.out.println("path:" + path);
		File f = new File(path);
		if (f.exists()) {
			re = f.getParent() + "\\";
		}
		System.out.println("文件路径：" + re);
		return re;
	}

	private static Properties loadProperty() {
		Properties prop = new Properties();
		try {
			// FileInputStream is = new FileInputStream("config.properties");
			InputStream is = EnglishCreateTemplate.class.getResourceAsStream("/config.properties");
			InputStreamReader io = new InputStreamReader(is, "UTF-8");
			prop.load(io);
			is.close();
			io.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop;
	}

	static {
		Properties prop = loadProperty();
		week = prop.getProperty("week", "EEEE");
		month = prop.getProperty("month", "MM-dd");

		startDate = prop.getProperty("startDate", null);
		endDate = prop.getProperty("endDate", null);
		locale = prop.getProperty("locale", "CHINA");
		if (prop.containsKey("planNum")) {
			try {
				planNum = Integer.parseInt(prop.getProperty("planNum"));
			} catch (Exception e) {
				planNum = 1;
			}
		}

		if (prop.containsKey("maxPlanNum")) {
			try {
				maxPlanNum = Integer.parseInt(prop.getProperty("maxPlanNum"));
			} catch (Exception e) {
				maxPlanNum = 20;
			}
		}
		
		if (prop.containsKey("width")) {
			try {
				width = Double.parseDouble(prop.getProperty("width"));
			} catch (Exception e) {
				planNum = 8;
			}
		}
		
		
		if (prop.containsKey("show")) {
			try {
				show = new Integer(prop.getProperty("show"));
			} catch (Exception e) {
				show = null;
			}
		}
		if (prop.containsKey("blank")) {
			try {
				blank = new Integer(prop.getProperty("blank"));
			} catch (Exception e) {
				blank = 1;
			}
		}

	}

	public static int getPlanNum() {
		return planNum;
	}

	public static int getMaxPlanNum() {
		return maxPlanNum;
	}

	public static String getWeek() {
		return week;
	}

	public static String getMonth() {
		return month;
	}
	
	public static Integer getShow(){
		return show;
	}
	
	public static Integer getBlank(){
		return blank;
	}

	public static Locale geteLocale() {
		Locale deLo = Locale.CHINA;
		if (locale.equalsIgnoreCase("CHINA")) {
			deLo = Locale.CHINA;
		}

		if (locale.equalsIgnoreCase("US")) {
			deLo = Locale.US;
		}

		return deLo;
	}

	public static Calendar getStartDate() {
		Calendar c1 = Calendar.getInstance();
		c1.set(Calendar.MONTH, Calendar.JANUARY);
		c1.set(Calendar.DAY_OF_MONTH, c1.getMinimum(Calendar.DAY_OF_MONTH));
		Date date = null;
		try {
			date = new SimpleDateFormat("yyyy-MM-ddd", Locale.US).parse(startDate);
			c1.setTime(date);
		} catch (Exception e) {

		}
		return c1;
	}
	

	public static Calendar getEndDate() {
		Calendar c2 = Calendar.getInstance();
		c2.set(Calendar.MONTH, Calendar.DECEMBER);
		c2.set(Calendar.DAY_OF_MONTH, c2.getMaximum(Calendar.DAY_OF_MONTH));
		Date date = null;
		try {
			date = new SimpleDateFormat("yyyy-MM-ddd", Locale.US).parse(endDate);
			c2.setTime(date);
		} catch (Exception e) {

		}
		return c2;
	}
	
	public static int getWidth(){
		return (int)width * 256;
	}

	private static String week;
	private static String month;
	private static int planNum;
	private static int maxPlanNum;
	private static String startDate;
	private static String endDate;
	private static String locale;
	private static double width;
	private static Integer show;
	private static Integer blank;

}

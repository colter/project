package com.colter.project.sample.socket.rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class SumImpl extends UnicastRemoteObject implements SumInterface {

	protected SumImpl() throws RemoteException {
		super();
	}

	@Override
	public int getSum(int a, int b) throws RemoteException {
		return a + b;
	}
}

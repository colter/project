package com.colter.project.sample.designpattern.chain.filter;

/**
 * @author liangchao03
 * @date 2017/7/18
 */
public class Servlet {
    public void doService(String request){
        System.out.println("Servlet处理："+request);
    }
}

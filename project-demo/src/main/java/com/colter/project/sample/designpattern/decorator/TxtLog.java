package com.colter.project.sample.designpattern.decorator;

public class TxtLog implements ILog{

	@Override
	public void log(String msg) {
		System.out.println();
		System.out.println("******txt******");
		System.out.println(msg);
		System.out.println("******txt******");
		System.out.println();
	}
	

}

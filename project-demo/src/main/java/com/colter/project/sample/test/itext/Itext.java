package com.colter.project.sample.test.itext;

import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.List;
import com.itextpdf.layout.element.ListItem;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;

import java.io.File;
import java.io.IOException;

/**
 * @author liangchao03
 * @date 2017/7/24
 */
public class Itext {
    //TODO remove mainTue Dec 31 13:13:21 CST 2019
//========
     //public static void main(String[] args) throws IOException {
         //File dest = new File("C:\\Users\\liangchao03\\Documents\\test.pdf");
         //PdfWriter pdfWriter = new PdfWriter(dest);
         //PdfDocument pdfDocument = new PdfDocument(pdfWriter);
 //
         //List list = new List()
                 //.setSymbolIndent(12)
                 //.setListSymbol("\u2022");
 //// Add ListItem objects
         //list.add(new ListItem("Never gonna give you up"))
                 //.add(new ListItem("Never gonna let you down"))
                 //.add(new ListItem("Never gonna run around and desert you"))
                 //.add(new ListItem("Never gonna make you cry"))
                 //.add(new ListItem("Never gonna say goodbye"))
                 //.add(new ListItem("Never gonna tell a lie and hurt you"));
 //
         //Document document = new Document(pdfDocument, PageSize.A4.rotate());
         //document.add(new Paragraph("Hello World!"));
 //
         //document.setMargins(20, 20, 20, 20);
         //PdfFont font = PdfFontFactory.createFont(FontConstants.HELVETICA);
         //PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
         //Table table = new Table(new float[]{2,3,3,4});
         //table.setWidthPercent(100);
         //table.addHeaderCell(new Paragraph("ID"));
         //table.addHeaderCell(new Paragraph("NAME"));
         //table.addHeaderCell(new Paragraph("AGE"));
         //table.addHeaderCell(new Paragraph("EMAIL"));
 //
         //for (int i=0;i<10;i++){
             //User   u = new User(i,"name:"+i,i,i+"@qq.com");
             //table.addCell(new Paragraph(String.valueOf(u.getId())));
             //table.addCell(new Paragraph(u.getName()));
             //table.addCell(new Paragraph(String.valueOf(u.getAge())));
             //table.addCell(new Paragraph(u.getEmail()));
         //}
 //
 //
         //document.add(table);
         //document.add(list);
         //document.close();
         //System.out.println("Done.");
     //}
//========
}

class User{
    private int id;
    private String name;
    private int age;
    private String email;

    public User(int id, String name, int age, String email) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.email = email;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", email='" + email + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

package com.colter.project.sample.designpattern.vistor;

public class CommonVistor implements IVistor {

	@Override
	public Object visit(Student student) {
		return "Common:"+ student.getName() + " "+ student.getAge();
	}

	@Override
	public Object visit(Loli loli) {
		return "Common:"+ loli.getName() + " "+ loli.getAge() +" Loli should not change.";
	}
}

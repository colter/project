package com.colter.project.sample.test.plan.ele;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class Ele {

    //TODO remove mainTue Dec 31 13:13:21 CST 2019
//========
     //public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
         //String prefix = Ele.class.getProtectionDomain().getCodeSource().getLocation().getPath();
         //System.out.println(prefix);
         //String path = prefix.substring(0, prefix.lastIndexOf("/") + 1) + "test.txt";
         //// String path = prefix + "test.txt";
         //File file = new File(path);
         //System.out.println(path);
         //BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "utf-8"));
         //String result = null;
         //List<String> list = new ArrayList<>();
         //try {
             //while ((result = br.readLine()) != null) {
                 //if (result != null && !result.trim().equals("")) {
                     //if (result.startsWith("\uFEFF")) {
                         //result = result.replace("\uFEFF", "");
                     //}
                     //list.add(result);
                 //}
             //}
         //} catch (IOException e) {
             //e.printStackTrace();
         //} finally {
             //try {
                 //br.close();
             //} catch (IOException e) {
                 //e.printStackTrace();
             //}
         //}
         //if (list.size() >= 2) {
             //String moneyStr = list.get(0);
             //String realTotalStr = list.get(1);
 //
             //String[] temp = moneyStr.split(" ");
             //List<Double> beforeMoenyList = new ArrayList<>();
             //List<EveryOne> everyOneList = new ArrayList<>();
 //
             //double totalCount = 0;
             //double realTotal = Double.parseDouble(realTotalStr);
 //
             //for (int i = 0; i < temp.length; i++) {
                 //String nT = temp[i];
                 //if (nT != null && !"".equals(nT.trim())) {
                     //double beforeMoney = Double.parseDouble(nT);
                     //beforeMoenyList.add(beforeMoney);
                     //totalCount += beforeMoney;
                 //}
             //}
 //
             //for (int i = 0; i < beforeMoenyList.size(); i++) {
                 //double beforeMoeny = beforeMoenyList.get(i);
                 //EveryOne one = new EveryOne();
                 //one.setBeforeMoney(beforeMoeny);
                 //one.setTotalCount(totalCount);
                 //one.setRealTotal(realTotal);
 //
                 //everyOneList.add(one);
             //}
 //
             //StringBuilder sb = new StringBuilder();
             //sb.append("\r\n");
             //for (int i = 0; i < everyOneList.size(); i++) {
                 //sb.append(everyOneList.get(i));
                 //System.out.println(everyOneList.get(i));
             //}
             //String content = sb.toString();
 //
             //RandomAccessFile randomFile = new RandomAccessFile(path, "rw");
             //try {
                 //long fileLength = randomFile.length();
                 //randomFile.seek(fileLength);
                 //randomFile.write(content.getBytes("utf-8"));
                 //randomFile.close();
             //} catch (IOException e) {
                 //e.printStackTrace();
             //}
 //
         //} else {
             //System.out.println("数据不足。");
         //}
     //}
//========
}

class EveryOne {
	private double beforeMoney;
	private double afterMoeny;
	private double totalCount;
	private double realTotal;
	private double percent;

	public double getBeforeMoney() {
		return beforeMoney;
	}

	public void setBeforeMoney(double beforeMoney) {
		this.beforeMoney = beforeMoney;
	}

	public double getAfterMoeny() {
		return getPercent() * realTotal;
	}

	public double getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(double totalCount) {
		this.totalCount = totalCount;
	}

	public double getRealTotal() {
		return realTotal;
	}

	public void setRealTotal(double realTotal) {
		this.realTotal = realTotal;
	}

	public double getPercent() {
		return getBeforeMoney() / totalCount;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("实际价格：" + String.format("%.1f", this.getAfterMoeny()));
		sb.append(" 实际总价：" + String.format("%.1f", this.getRealTotal()));
		sb.append(" [原价：" + String.format("%.1f", this.getBeforeMoney()));
		sb.append(" 百分比：" + String.format("%.1f", this.getPercent() * 100) + "%");
		sb.append(" 原价总计：" + String.format("%.1f", this.getTotalCount()));
		sb.append("] \r\n");
		return sb.toString();
	}

}

package com.colter.project.sample.designpattern.proxy.jdkproxy.jdkproxy.test;

public class UserImpl implements UserService {

	@Override
	public void addUser() {
		System.out.println("Add");
	}

	@Override
	public void delUser() {
		System.out.println("Del");
	}

	@Override
	public void updateUser() {
		System.out.println("Update");
	}

}

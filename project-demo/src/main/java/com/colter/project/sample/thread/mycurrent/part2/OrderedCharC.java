package com.colter.project.sample.thread.mycurrent.part2;

/**
 * @author LC
 * 2018/2/14
 */
public class OrderedCharC implements Runnable {
    public OrderedCharC(OrderHelper helper) {
        this.helper = helper;
    }

    private OrderHelper helper;

    @Override
    public void run() {
        synchronized (helper) {
            for (int i = 0; i <helper.getNum() ; ) {
                while (helper.isC()) {
                    System.out.println("C " + Thread.currentThread().getName());
                    helper.setC(false);
                    helper.setA(true);
                    helper.notifyAll();
                    i++;
                }
                try {
                    Thread.sleep(100);
                    helper.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("输出C完毕。" + Thread.currentThread().getName());
            helper.notifyAll();
        }

    }
}

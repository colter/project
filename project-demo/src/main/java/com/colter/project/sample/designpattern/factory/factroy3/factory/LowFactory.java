package com.colter.project.sample.designpattern.factory.factroy3.factory;

import com.colter.project.sample.designpattern.factory.factroy3.entity.LowCar;
import com.colter.project.sample.designpattern.factory.factroy3.entity.ICar;

public class LowFactory extends AbstractFactory2{

	@Override
	public ICar createCar() {
		return new LowCar();
	}

}

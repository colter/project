package com.colter.project.sample.event;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liangchao03
 * @date 2017/7/24
 */
public class ColterPublisher {
    private List<ColterListener> list = new ArrayList<>();

    public void regist(ColterListener listener){
        list.add(listener);
    }
    public void deregist(ColterListener listener){
        list.remove(listener);
    }

    public void publishSource(Object source){
        ColterEvent event = new ColterEvent(source);
        list.forEach(colterListener -> {
            colterListener.doEvent(event);
        });
    }
}

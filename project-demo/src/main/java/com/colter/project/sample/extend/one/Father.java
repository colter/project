package com.colter.project.sample.extend.one;

public class Father {
	public int a = 1;
	protected int b = 2;
	int c = 3;
	private int d = 4;

	public void test() {
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);
	}
}

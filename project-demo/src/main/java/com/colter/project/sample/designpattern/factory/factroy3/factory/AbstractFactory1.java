package com.colter.project.sample.designpattern.factory.factroy3.factory;

import com.colter.project.sample.designpattern.factory.factroy3.entity.IBus;
import com.colter.project.sample.designpattern.factory.factroy3.entity.ICar;

public abstract class AbstractFactory1  extends AbstractFactory{
	public abstract ICar createCar();
	public abstract IBus createBus();
}

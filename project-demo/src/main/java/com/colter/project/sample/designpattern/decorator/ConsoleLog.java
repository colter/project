package com.colter.project.sample.designpattern.decorator;

public class ConsoleLog implements ILog {

	@Override
	public void log(String msg) {
		System.out.println();
		System.out.println("--------console----------");
		System.out.println(msg);
		System.out.println("--------console----------");
		System.out.println();

	}

}

package com.colter.project.sample.designpattern.observer.observer2;

public class Observer<T> implements IObserver<T> {

	@Override
	public void refresh(T data) {
		System.out.println("Oberser :I received data:"+data);
	}

}

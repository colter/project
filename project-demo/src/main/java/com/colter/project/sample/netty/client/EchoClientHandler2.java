package com.colter.project.sample.netty.client;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.CharsetUtil;

import java.util.Date;

/**
 * @author liangchao
 * creation time:  2019/12/15 13:20
 * desc:
 */
@ChannelHandler.Sharable
public class EchoClientHandler2 extends SimpleChannelInboundHandler<ByteBuf> {

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("客户端传给服务端");
        String string = "to Server";
        ByteBuf buf = Unpooled.buffer(string.length());
        buf.writeBytes(string.getBytes());
        ctx.writeAndFlush(buf);
    }

    // 在到服务器的连接已经建立之后将被调用
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.out.println("Client Write Data");
        ctx.channel().writeAndFlush(Unpooled.copiedBuffer("Netty clients!", CharsetUtil.UTF_8));
    }

    // 当从服务器接收到一条消息时被调用
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
        System.out.println("Client Received:" + msg.toString(CharsetUtil.UTF_8));
    }

    // 在处理过程中引发异常时被调用
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }

}

package com.colter.project.sample.test.plan.anno;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Test {
    //TODO remove mainTue Dec 31 13:13:21 CST 2019
//========
     //public static void main(String[] args) {
 ////		testClass();
 ////		testMethod();
 ////		testParameter();  //TODO
 ////		testField();
 ////		TestField testField = new TestField();
 ////		System.out.println(testField.name);
     //
         //
     //}
//========
	
	public static void testField(){
		Class<TestField> c = TestField.class;
		Field[] fields = c.getDeclaredFields();
		for(Field f : fields){
			if(f.isAnnotationPresent(AnnoField.class)){
				AnnoField annoField = f.getAnnotation(AnnoField.class);
				String value = annoField.value();
				System.out.println(value);
			}
		}
	}
	
	
	public static void testParameter(){
		Class<TestParameter> c = TestParameter.class;
		Method[] methods = c.getDeclaredMethods();
		for (Method m : methods) {
			Annotation[][] annos = m.getParameterAnnotations();
			AnnoParameter a1 = (AnnoParameter) annos[0][0];
			AnnoParameter a2 = (AnnoParameter) annos[1][0];

			String value1 = a1.value();
			String value2 = a2.value();

			try {
				m.invoke(TestParameter.class.newInstance(), value1, value2);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void testMethod(){
		Class<TestMethod> c = TestMethod.class;
		Method[] methods =  c.getDeclaredMethods();
		for(Method m : methods){
			System.out.println(m.getName());
			if(m.isAnnotationPresent(AnnoMethod.class)){
				AnnoMethod testAnno1 = m.getAnnotation(AnnoMethod.class);
				String name = testAnno1.name();
				String value = testAnno1.value();
				System.out.println(name +" " + value);
			}
		}
		
		try {
			System.out.println("----------");
			Method me = c.getMethod("test1");
			if(me.isAnnotationPresent(AnnoMethod.class)){
				AnnoMethod testAnno1 = me.getAnnotation(AnnoMethod.class);
				String name = testAnno1.name();
				String value = testAnno1.value();
				System.out.println(name +" " + value);
			}
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
	}
	
	public static void testClass(){
		Class<TestClass> c = TestClass.class;
		if(c.isAnnotationPresent(AnnoType.class)){
			AnnoType testAnno = c.getAnnotation(AnnoType.class);
			String[] values = testAnno.value();
			for(String s : values){
				System.out.println(s);
			}
		}
	}
	
}

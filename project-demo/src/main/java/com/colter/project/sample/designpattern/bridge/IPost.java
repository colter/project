package com.colter.project.sample.designpattern.bridge;

public interface IPost {
	public void post();
}

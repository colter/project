package com.colter.project.sample.designpattern.factory.factroy3.factory;

import com.colter.project.sample.designpattern.factory.factroy3.entity.IBus;
import com.colter.project.sample.designpattern.factory.factroy3.entity.TopCar;
import com.colter.project.sample.designpattern.factory.factroy3.entity.ICar;
import com.colter.project.sample.designpattern.factory.factroy3.entity.TopBus;

public class TopFactory extends AbstractFactory1 {

	@Override
	public ICar createCar() {
		return new TopCar();
	}

	@Override
	public IBus createBus() {
		return new TopBus()
		;
	}
	
}

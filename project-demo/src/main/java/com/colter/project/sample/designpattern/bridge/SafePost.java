package com.colter.project.sample.designpattern.bridge;

public class SafePost implements IPost {

	@Override
	public void post() {
		System.out.println("safe post...");
	}

}

package com.colter.project.sample.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

public class IOStream {
    //TODO remove mainTue Dec 31 13:13:20 CST 2019
//========
     //public static void main(String[] args) {
         //String fileStr = "1.jpg";
 ////		copy1(fileStr);
 ////		copy2(fileStr);
         //
         //
         //String file = "c:\\test\\obj.xt";
         //copy3(file);
     //}
//========

	public static void copy1(String str) {
		File file = new File("c:\\test\\"+str);
		File destFile = new File("c:\\test\\1-" + file.getName());
		FileReader reader = null;
		BufferedReader br = null;
		FileWriter writer = null;
		BufferedWriter bw = null;

		try {
			reader = new FileReader(file);
			br = new BufferedReader(reader);
			String temp = null;

			writer = new FileWriter(destFile);
			bw = new BufferedWriter(writer);
			while ((temp = br.readLine()) != null) {
				bw.write(temp);
				bw.newLine();
			}
			bw.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
				br.close();
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("Done.");
		}
	}

	public static void copy2(String str) {
		File file = new File("c:\\test\\"+str);
		File destFile = new File("c:\\test\\2-" + file.getName());
		FileInputStream in = null;
		BufferedInputStream bi = null;
		FileOutputStream out = null;
		BufferedOutputStream bo = null;

		try {
			in = new FileInputStream(file);
			bi = new BufferedInputStream(in);
			out = new FileOutputStream(destFile);
			bo = new BufferedOutputStream(out);
			byte[] b = new byte[512];
			int len = 0;
			while ((len = bi.read(b)) != -1) {
//				bi.read(b, 0, len);
//				bo.write(b, 0, len);
				
				//FileOutputStream 可以用
//				out.write(b, 0, len);
//				out.flush();
				
				//BufferedOutputSteam也可以用
//				bo.write(b, 0, len);
//				bo.flush();
				
				
			}
			bo.flush();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				bo.close();
				out.close();
				bi.close();
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("Done.");
		}
	}
	
	public static void copy3(String fileName){
		Map<String,String> map = new HashMap<>();
		map.put("age", "123");
		map.put("name","222");
		ObjectOutputStream out = null;
		try {
			out = new ObjectOutputStream(new FileOutputStream(fileName));
			out.writeObject(map);
			out.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			try {
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		ObjectInputStream in = null;
		try {
			in = new ObjectInputStream(new FileInputStream(fileName));
			Object obj = in.readObject();
			System.out.println(obj +  " "+obj.getClass());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally{
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}

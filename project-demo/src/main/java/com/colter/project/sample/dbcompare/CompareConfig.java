package com.colter.project.sample.dbcompare;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * @author liangchao03
 *         2017/10/24
 */
public class CompareConfig {
    public static List<String> types;
    public static int colums = 0;
    public static List<Comparator> rules;

    static{
        String type = Config.get(CompareConstant.DB_TYPES);
        types = Arrays.asList(type.split(","));
        colums = types.size();
    }
}

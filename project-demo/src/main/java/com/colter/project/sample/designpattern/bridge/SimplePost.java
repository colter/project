package com.colter.project.sample.designpattern.bridge;

public class SimplePost implements IPost{

	@Override
	public void post() {
		System.out.println("simple post...");
	}

}

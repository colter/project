package com.colter.project.sample.designpattern.chain.filter;

/**
 * @author liangchao03
 * @date 2017/7/18
 */
public abstract class Filter {
    public abstract void doFilter(String request,String response,FilterChain filterChain);
}

package com.colter.project.sample.test.plan;

import java.util.EnumMap;
import java.util.EnumSet;



public class TestEnum {
    //TODO remove mainTue Dec 31 13:13:21 CST 2019
//========
     //public static void main(String[] args) {
         //// 常量
         //System.out.println(Color.RED);
 //
         //// switch
         //Color color = Color.GREEN;
         //switch (color) {
         //case GREEN:
             //System.out.println("green...");
             //break;
         //case YELLOW:
             //System.out.println("yellow...");
             //break;
         //case RED:
             //System.out.println("red...");
             //break;
         //default:
             //System.out.println("default...");
             //break;
         //}
         //System.out.println("--------------------");
         ////枚举添加新方法
         //System.out.println(ConEnum.RED.getName()+" "+ConEnum.RED.getIndex());
         //System.out.println(ConEnum.getName(2));
         //System.out.println("--------------------");
         //System.out.println(IEnum.T1.A);
         //System.out.println(IEnum.T2.E);
         //
         ////EnumMap
         //EnumMap<Color, String> currentEnum = new EnumMap<>(Color.class);
         //currentEnum.put(Color.GREEN, "abc");
         //currentEnum.put(Color.YELLOW, "def");
         //currentEnum.put(Color.RED, "ghi");
         //currentEnum.put(Color.GREEN, "aaa");
         //System.out.println(currentEnum.get(Color.GREEN));
         //
         //System.out.println("---------------------");
         //
         //// EnumSet
         //EnumSet<ConEnum> set = EnumSet.allOf(ConEnum.class);
         //for (ConEnum e : set) {
             //System.out.println(e+ " "+e.getName());
         //}
         //
 //
     //}
//========
}

enum Color {
	GREEN, YELLOW, RED
}

enum ConEnum {
	GREEN("绿色", 7), YELLOW("黄色", 6), RED("红色", 2);

	private String name;
	private int index;

	private ConEnum(String name, int index) {
		this.name = name;
		this.index = index;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	
    public static String getName(int index) {
        for (ConEnum c : ConEnum.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }
}

interface IEnum{
	enum T1 implements IEnum{
		A,B,C
	}
	
	enum T2 implements IEnum{
		D,E,F
	}
}

package com.colter.project.sample.designpattern.proxy.jdkproxy.jdkproxy;

public interface BookFacadeService {
	 void add();
	 void plus();
}

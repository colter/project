package com.colter.project.sample.thread.part06.test1;

import java.util.concurrent.ThreadLocalRandom;

//并发随机数 ThreadLocalRandom
public class Test7 {
    //TODO remove mainTue Dec 31 13:13:22 CST 2019
//========
     //public static void main(String[] args) {
         //for (int i = 0; i < 3; i++) {
             //TaskRandom task = new TaskRandom();
             //Thread t = new Thread(task);
             //t.start();
         //}
     //}
//========
}

class TaskRandom implements Runnable {
	private ThreadLocalRandom random;

	public TaskRandom() {
		super();
		this.random = ThreadLocalRandom.current();
	}

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			System.out.println("Name:" + Thread.currentThread().getName() + "Random:" + random.nextInt(100));
		}
	}

}

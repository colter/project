package com.colter.project.sample.designpattern.chain;

/**
 * @author liangchao03
 * @date 2017/7/18
 */
public class SubCalc extends Calculator {
    @Override
    public int handle(int first, int last, String sign) {
        if ("-".equals(sign)) {
            System.out.println(this.getClass() + " 处理了。");
            return first - last;
        } else {
            if (getNextCalculator() != null) {
                return getNextCalculator().handle(first, last, sign);
            }
            System.out.println("没有可以处理的Calculator");
            return 0;
        }
    }
}

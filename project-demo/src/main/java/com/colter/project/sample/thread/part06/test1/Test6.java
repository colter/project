package com.colter.project.sample.thread.part06.test1;

import java.util.concurrent.ConcurrentSkipListMap;

//使用线程安全可遍历映射
public class Test6 {
    //TODO remove mainTue Dec 31 13:13:22 CST 2019
//========
     //public static void main(String[] args) {
         //ConcurrentSkipListMap<String, Contact> map = new ConcurrentSkipListMap<>();
         //Thread[] threads = new Thread[25];
         //int counter = 0;
         //for (char i = 'A'; i < 'Z'; i++) {
             //Task6 task = new Task6(String.valueOf(i), map);
             //threads[counter] = new Thread(task);
             //threads[counter].start();
             //counter++;
         //}
 //
         //for (int i = 0; i < threads.length; i++) {
             //try {
                 //threads[i].join();
             //} catch (InterruptedException e) {
                 //e.printStackTrace();
             //}
         //}
 //
         //System.out.println("Main:Size:" + map.size());
         //for (String s : map.keySet()) {
             //System.out.println("key:" + s + " value:" + map.get(s));
         //}
     //}
//========
}

class Contact {
	private String name;
	private String phone;

	public Contact(String name, String phone) {
		super();
		this.name = name;
		this.phone = phone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "Contact [name=" + name + ", phone=" + phone + "]";
	}

}

class Task6 implements Runnable {
	private String id;
	private ConcurrentSkipListMap<String, Contact> map;

	public Task6(String id, ConcurrentSkipListMap<String, Contact> map) {
		super();
		this.id = id;
		this.map = map;
	}

	@Override
	public void run() {
		for (int i = 0; i < 100; i++) {
			Contact contact = new Contact(id, String.valueOf(i + 1000));
			map.put(id + contact.getPhone(), contact);
		}
	}

}

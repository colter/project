package com.colter.project.sample.designpattern.chain;

/**
 * @author liangchao03
 * @date 2017/7/18
 */
public abstract class Calculator {
    private Calculator nextCalculator;

    public Calculator getNextCalculator() {
        return nextCalculator;
    }

    public void setNextCalculator(Calculator nextCalculator) {
        this.nextCalculator = nextCalculator;
    }

    public abstract int handle(int first,int last,String sign);
}

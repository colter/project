package com.colter.project.sample.designpattern.proxy.jdkproxy.jdkproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class BookFacadeProxy implements InvocationHandler {
	public Object bind() {
		return Proxy.newProxyInstance(BookFacadeService.class.getClassLoader(), new Class[]{BookFacadeService.class}, this);
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		Object result = null;
		System.out.println("Start.");
		if(method.getName().equals("add")){
			new MethodMapper().m1();
		}else if(method.getName().equals("plus")){
			new MethodMapper().m2();
		}else{
			System.out.println("None.");
		}
		System.out.println("End.");
		return result;
	}

}

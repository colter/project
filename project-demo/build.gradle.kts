dependencies {
    implementation(project(":project-util"))
    //Active MQ
    implementation("org.apache.activemq:activemq-core:5.7.0")
    implementation("org.apache.activemq:activemq-pool:5.14.0")

    //Redis
    implementation("redis.clients:jedis:2.9.0")

    //Rabbit MQ
    implementation ("com.rabbitmq:amqp-client:4.2.0")

    //PDF
    implementation ("com.itextpdf:itext7-core:7.0.3")

    //Email
    implementation("javax.mail:mail:1.5.0-b01")

    //http
    implementation("com.squareup.okhttp3:okhttp:3.12.1")
    //netty
    implementation("io.netty:netty-all:4.1.43.Final")
}
package com.colter.project.util.file;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.jupiter.api.Test;

@Ignore
public class FileUtilTest {

    @Test
    public void cut() {
        String filePath = "/home/liangchao/test/ow.mp4";
        FileUtil.cut(filePath, ".bak", 7);
    }

    @Test
    public void merge() {
        String pathPrefix = "/home/liangchao/test";
        FileUtil.merge(pathPrefix, ".bak", 10, "ow.mp4", "/home/liangchao/test/result");
    }
}
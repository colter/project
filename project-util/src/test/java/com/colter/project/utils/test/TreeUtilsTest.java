package com.colter.project.utils.test;

import com.colter.project.data.structure.heap.LimitHeap;
import com.colter.project.data.structure.heap.MaxHeap;
import com.colter.project.data.structure.heap.MinHeap;
import com.colter.project.data.structure.tree.BinaryTree;
import com.colter.project.util.common.TreeUtil;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author LC
 * 2018/2/2
 */
@Ignore
public class TreeUtilsTest {

    @Test
    public void testMaxHeap() {
        MaxHeap<Integer> maxHeap = new MaxHeap<>();

        IntStream.range(0, 20).forEach(i -> {
            maxHeap.add((int) (Math.random() * 20));
        });

        List<Integer> list = new LinkedList<>();
        while (!maxHeap.isEmpty()) {
            list.add(maxHeap.removeAndReturnTop());
        }
        List<Integer> list2 = list.stream().sorted(Integer::compare).collect(Collectors.toList());
        Collections.reverse(list2);
        System.out.println(list);
        System.out.println(list2);
        assertEquals(list, list2);

    }

    @Test
    public void testMinHeap() {
        MinHeap<Integer> minHeap = new MinHeap<>();

        IntStream.range(0, 20).forEach(i -> {
            minHeap.add((int) (Math.random() * 20));
        });

        List<Integer> list = new LinkedList<>();

        while (!minHeap.isEmpty()) {
            list.add(minHeap.removeAndReturnTop());
        }
        List<Integer> list2 = list.stream().sorted(Integer::compare).collect(Collectors.toList());
        Collections.sort(list2);
        System.out.println(list);
        System.out.println(list2);
        assertTrue(Objects.equals(list, list2));
    }

    @Test
    public void testLimitMinHeap() {
        List<Integer> list = new LinkedList<>();
        IntStream.range(0, 1000).forEach(i -> {
            list.add((int) (Math.random() * 1000));
        });

        System.out.println(System.currentTimeMillis());
        LimitHeap<Integer> lmax = new LimitHeap<>(LimitHeap.HeapType.FIND_MIN, 5);
        LimitHeap<Integer> lmin = new LimitHeap<>(LimitHeap.HeapType.FIND_MAX, 5);

        list.forEach(i -> {
            lmax.add(i);
            lmin.add(i);
        });
        System.out.println(System.currentTimeMillis());

        System.out.println(lmax);
        System.out.println(lmin);
        Collections.sort(list);
        System.out.println(list.subList(0, 5));
        System.out.println(list.subList(list.size() - 5, list.size()));

        assertTrue(mockEquals(lmin, list.subList(list.size() - 5, list.size())));
        assertTrue(mockEquals(lmax, list.subList(0, 5)));
    }

    private boolean mockEquals(List<Integer> list1, List<Integer> list2) {
        if (list1 == null && list2 == null) {
            return true;
        }
        if (!(list1 != null && list2 != null)) {
            return false;
        }
        if (list1.size() != list2.size()) {
            return false;
        }

        list1.forEach(e1 -> {
            list2.remove(e1);
        });

        return list2.size() == 0;

    }


    @Test
    public void testBinaryTree() {
        BinaryTree<String> t1 = new BinaryTree<>("A");
        BinaryTree<String> t2 = new BinaryTree<>("B");
        BinaryTree<String> t3 = new BinaryTree<>("C");
        BinaryTree<String> t4 = new BinaryTree<>("D");
        BinaryTree<String> t5 = new BinaryTree<>("E");
        BinaryTree<String> t6 = new BinaryTree<>("F");
        BinaryTree<String> t7 = new BinaryTree<>("G");
        BinaryTree<String> t8 = new BinaryTree<>("H");

        t1.setLeftChild(t2);
        t1.setRightChild(t3);
        t2.setLeftChild(t4);
        t2.setRightChild(t5);
        t3.setLeftChild(t6);
        t3.setRightChild(t7);
        t4.setRightChild(t8);

        TreeUtil.visitTreePreOrder(t1);
        System.out.println();
        TreeUtil.visitTreeMidOrder(t1);
        System.out.println();
        TreeUtil.visitTreeLevelOrder(t1);

    }

    @Test
    public void testBuildTree() {
        _testA();
        _testABC();
        _testALeftB();
        _testARightB();
        _testABCDEFG();
    }

    private void _testA() {
        BinaryTree<String> t1 = new BinaryTree<>("A");

        List<String> list1 = new ArrayList<>();
        list1.add("A");

        List<String> list2 = new ArrayList<>();
        list2.add("A");

        List<String> list3 = new ArrayList<>();
        list3.add("A");

        BinaryTree<String> tree1 = TreeUtil.buildTreeFromPreAndMidVisit(list1, list2);
        BinaryTree<String> tree2 = TreeUtil.buildTreeFromPostAndMidVisit(list3, list2);
        assertEquals(tree1, tree2);
        assertEquals(t1, tree2);
        assertEquals(t1, tree1);
    }

    private void _testALeftB() {
        BinaryTree<String> t1 = new BinaryTree<>("A");
        BinaryTree<String> t2 = new BinaryTree<>("B");
        t1.setLeftChild(t2);

        List<String> list1 = new ArrayList<>();
        list1.add("A");
        list1.add("B");

        List<String> list2 = new ArrayList<>();
        list2.add("B");
        list2.add("A");

        List<String> list3 = new ArrayList<>();
        list3.add("B");
        list3.add("A");

        BinaryTree<String> tree1 = TreeUtil.buildTreeFromPreAndMidVisit(list1, list2);
        BinaryTree<String> tree2 = TreeUtil.buildTreeFromPostAndMidVisit(list3, list2);
        assertEquals(tree1, tree2);
        assertEquals(t1, tree2);
        assertEquals(t1, tree1);
    }

    private void _testARightB() {
        BinaryTree<String> t1 = new BinaryTree<>("A");
        BinaryTree<String> t2 = new BinaryTree<>("B");
        t1.setRightChild(t2);

        List<String> list1 = new ArrayList<>();
        list1.add("A");
        list1.add("B");

        List<String> list2 = new ArrayList<>();
        list2.add("A");
        list2.add("B");

        List<String> list3 = new ArrayList<>();
        list3.add("B");
        list3.add("A");

        BinaryTree<String> tree1 = TreeUtil.buildTreeFromPreAndMidVisit(list1, list2);
        BinaryTree<String> tree2 = TreeUtil.buildTreeFromPostAndMidVisit(list3, list2);
        assertEquals(tree1, tree2);
        assertEquals(t1, tree2);
        assertEquals(t1, tree1);
    }

    private void _testABC() {
        BinaryTree<String> t1 = new BinaryTree<>("A");
        BinaryTree<String> t2 = new BinaryTree<>("B");
        BinaryTree<String> t3 = new BinaryTree<>("C");
        t1.setLeftChild(t2);
        t1.setRightChild(t3);


        List<String> list1 = new ArrayList<>();
        list1.add("A");
        list1.add("B");
        list1.add("C");


        List<String> list2 = new ArrayList<>();
        list2.add("B");
        list2.add("A");
        list2.add("C");

        List<String> list3 = new ArrayList<>();
        list3.add("B");
        list3.add("C");
        list3.add("A");

        BinaryTree<String> tree1 = TreeUtil.buildTreeFromPreAndMidVisit(list1, list2);
        BinaryTree<String> tree2 = TreeUtil.buildTreeFromPostAndMidVisit(list3, list2);
        assertEquals(tree1, tree2);
        assertEquals(t1, tree2);
        assertEquals(t1, tree1);
    }

    private void _testABCDEFG() {
        BinaryTree<String> t1 = new BinaryTree<>("A");
        BinaryTree<String> t2 = new BinaryTree<>("B");
        BinaryTree<String> t3 = new BinaryTree<>("C");
        BinaryTree<String> t4 = new BinaryTree<>("D");
        BinaryTree<String> t5 = new BinaryTree<>("E");
        BinaryTree<String> t6 = new BinaryTree<>("F");
        BinaryTree<String> t7 = new BinaryTree<>("G");
        BinaryTree<String> t8 = new BinaryTree<>("H");

        t1.setLeftChild(t2);
        t1.setRightChild(t3);
        t2.setLeftChild(t4);
        t2.setRightChild(t5);
        t3.setLeftChild(t6);
        t3.setRightChild(t7);
        t4.setRightChild(t8);

        List<String> list1 = new ArrayList<>();

        list1.add("A");
        list1.add("B");
        list1.add("D");
        list1.add("H");
        list1.add("E");
        list1.add("C");
        list1.add("F");
        list1.add("G");

        List<String> list2 = new ArrayList<>();

        list2.add("D");
        list2.add("H");
        list2.add("B");
        list2.add("E");
        list2.add("A");
        list2.add("F");
        list2.add("C");
        list2.add("G");


        List<String> list3 = new ArrayList<>();
        list3.add("H");
        list3.add("D");
        list3.add("E");
        list3.add("B");
        list3.add("F");
        list3.add("G");
        list3.add("C");
        list3.add("A");


        BinaryTree<String> tree1 = TreeUtil.buildTreeFromPreAndMidVisit(list1, list2);
        BinaryTree<String> tree2 = TreeUtil.buildTreeFromPostAndMidVisit(list3, list2);
        assertEquals(tree1, tree2);
        assertEquals(t1, tree2);
        assertEquals(t1, tree1);
    }

}

package com.colter.project.utils.test;


import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.time.Instant;

/**
 * @author liangchao
 * 5/5/18 11:10 PM
 */
//TODO 只写了一点
public class NioUtil {
    //TODO remove mainTue Dec 31 13:13:22 CST 2019
//========
     //public static void main(String[] args) throws UnsupportedEncodingException {
         //for (int i = 0; i < 5; i++) {
             //Instant inst1 = Instant.now();
             //FileChannel inChannel = null;
             //FileChannel outChannel = null;
             //try {
                 //inChannel = FileChannel.open(Paths.get("/home/liangchao/Videos/The.Pursuit.of.Happyness.2006.当幸福来敲门.双语字幕.HR-HDTV.AC3.1024X576.x264-人人影视制作.mkv"), StandardOpenOption.READ);
                 //outChannel = FileChannel.open(Paths.get("/home/liangchao/Videos/" + i + ".mkv"), StandardOpenOption.WRITE, StandardOpenOption.CREATE);
 //
                 //inChannel.transferTo(0, inChannel.size(), outChannel);
 ////                ByteBuffer buffer = ByteBuffer.allocate(1024);
 ////
 ////                while (inChannel.read(buffer) != -1) {
 ////                    buffer.flip();
 ////                    outChannel.write(buffer);
 ////                    buffer.clear();
 ////                }
             //} catch (IOException e) {
                 //e.printStackTrace();
             //} finally {
                 //if (outChannel != null) {
                     //try {
                         //outChannel.close();
                     //} catch (IOException e) {
                         //e.printStackTrace();
                     //}
                 //}
                 //if (inChannel != null) {
                     //try {
                         //inChannel.close();
                     //} catch (IOException e) {
                         //e.printStackTrace();
                     //}
                 //}
 //
             //}
             //Instant inst2 = Instant.now();
             //Duration duration = Duration.between(inst1, inst2);
 //
             //System.out.println("Done." + duration.getSeconds());
         //}
     //}
//========

    public void client() throws IOException {
        SocketChannel socketChannel = SocketChannel.open(new InetSocketAddress("127.0.0.1", 8088));
        ByteBuffer buffer = ByteBuffer.allocate(1024 * 1024);

        FileChannel inChannel = FileChannel.open(Paths.get("/home/liangchao/Videos/a.mp4"), StandardOpenOption.READ);
        while (inChannel.read(buffer) != -1) {
            buffer.flip();
            socketChannel.write(buffer);
            buffer.clear();
        }

        socketChannel.shutdownOutput();

        int len = 0;
        while ((len = socketChannel.read(buffer)) != -1) {
            buffer.flip();
            String str = new String(buffer.array(), 0, len);
            System.out.println(str);
            buffer.clear();
        }

        inChannel.close();
        socketChannel.close();
    }

    @Test
    public void server() throws IOException {
        ServerSocketChannel serverChannel = ServerSocketChannel.open();
        FileChannel outChannel = FileChannel.open(Paths.get("/home/liangchao/Videos/b.mp4"), StandardOpenOption.WRITE, StandardOpenOption.CREATE);
        ByteBuffer buf = ByteBuffer.allocate(1024 * 1024);

        serverChannel.bind(new InetSocketAddress(8088));
        SocketChannel socketChannel = serverChannel.accept();

        while (socketChannel.read(buf) != -1) {
            buf.flip();
            outChannel.write(buf);
            buf.clear();
        }

        //反馈

        buf.put("接受完毕.".getBytes());
        buf.flip();
        socketChannel.write(buf);

        socketChannel.close();
        outChannel.close();
        serverChannel.close();


    }
}

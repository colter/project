package com.colter.project.utils.test;

import com.colter.project.data.structure.tree.RedBlackTree;
import com.colter.project.util.common.TreeUtil;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.jupiter.api.Test;


/**
 * @author liangchao
 * 2018/03/31
 */
@Ignore
public class RedBlackTreeTest {
    @Test
    public void testLeftRotate() {
        RedBlackTree<String> root = new RedBlackTree<>("root");
        RedBlackTree<String> x = new RedBlackTree<>("X");
        RedBlackTree<String> y = new RedBlackTree<>("Y");
        RedBlackTree<String> a = new RedBlackTree<>("a");
        RedBlackTree<String> b = new RedBlackTree<>("b");
        RedBlackTree<String> c = new RedBlackTree<>("c");

        root.setLeftChild(x);
        x.setLeftChild(a);
        x.setRightChild(y);
        y.setLeftChild(b);
        y.setRightChild(c);

        TreeUtil.printTree(root);
        x.leftRotate();
        TreeUtil.printTree(root);
        y.rightRotate();
        TreeUtil.printTree(root);
    }

    @Test
    public void testInsert() {
        RedBlackTree<Integer> root = new RedBlackTree<>(5, true);
        root.insert(8);
        root.insert(7);
        root.insert(11);
        root.insert(3);
        root.insert(6);
        root.insert(12);
        root.insert(9);
        root.insert(4);

        root.remove(4);
        root.remove(8);
        root.remove(7);

        TreeUtil.printTree(root);
    }

    @Test
    public void testRemove() {
        RedBlackTree<Integer> root = new RedBlackTree<>(5, true);
        root.insert(8);
        root.insert(7);
        root.insert(4);

        root.remove(4);
        root.remove(5);
        TreeUtil.printTree(root);
    }
}


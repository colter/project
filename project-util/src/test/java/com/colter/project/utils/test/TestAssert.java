package com.colter.project.utils.test;

import com.colter.project.util.string.StringUtils;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class TestAssert {
	@Test
	public void testStringHelper(){
		assertNull(StringUtils.trimToNull("        "));
		assertNull(StringUtils.trimToNull(null));
		assertEquals("abc", StringUtils.trimToNull("    abc     "));
		
		assertTrue(StringUtils.isBlank("    "));
		assertTrue(StringUtils.isBlank(null));
		assertEquals(false, StringUtils.isBlank("         a     "));

		assertNull(StringUtils.trim(null));
		assertEquals("", StringUtils.trim("      "));
		assertEquals("abc", StringUtils.trim("   abc     "));

		String[] arr1 = new String[]{"a","b","c"};
		assertArrayEquals(arr1, StringUtils.changeToArray("a,b,c", ","));
		assertNull(StringUtils.changeToArray(null, ","));

		List<String> list = new ArrayList<>();
		list.add("a");
		list.add("b");
		list.add("c");
		assertEquals(list, StringUtils.changeToList("a,b,c", ","));
		assertNull(StringUtils.changeToList(null, ","));


		assertEquals("abc ", StringUtils.addStrings("a","b","c "));
		assertEquals("a,b,c ", StringUtils.addStringsByDelimiter(",","a","b","c "));
		assertEquals("a-===b-===c -===d", StringUtils.addStringsByDelimiter("-===","a","b","c ","d"));

	}
}

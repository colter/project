package com.colter.project.util.qrcode;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;

public class QRCodeUtil {
	public static void createQRCode(String text, String filePath) {
		int width = 300;
		int height = 300;
		// 二维码的图片格式
		String format = "png";
		/**
		 * 设置二维码的参数
		 */
		HashMap<EncodeHintType, Object> hints = new HashMap<>();
		// 内容所使用编码
		hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
		// 生成二维码
		try {
			BitMatrix bitMatrix = new MultiFormatWriter().encode(text, BarcodeFormat.QR_CODE, width, height, hints);
			MatrixToImageWriter.writeToPath(bitMatrix, format, Paths.get(filePath));
			System.out.println("-----generate qrcode success-----");
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (WriterException e) {
			e.printStackTrace();
		}

	}
}

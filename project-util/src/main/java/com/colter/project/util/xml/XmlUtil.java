package com.colter.project.util.xml;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class XmlUtil {
	public static List<List<String>> ALL_LIST = new ArrayList<>();

	public static void changeXmlToList(String filePath) throws DocumentException {
		File file = new File(filePath);
		SAXReader reader = new SAXReader();
		Document document = reader.read(file);
		Element root = document.getRootElement();
		getNamedNode(root);
		System.out.println(ALL_LIST);
	}

	@SuppressWarnings("unchecked")
	public static void getNamedNode(Element node) {
		List<Element> list = node.elements();
		for (Element e : list) {
			if (e.getName().equals("Row")) {
				ArrayList<String> one = new ArrayList<>();
				List<Element> cells = e.elements();
				for (Element cell : cells) {
					String value = getFirstChildValue(cell);
					one.add(value);
				}
				ALL_LIST.add(one);
			} else {
				getNamedNode(e);
			}
		}

	}

	// Get the first value of its first child
	@SuppressWarnings("unchecked")
	public static String getFirstChildValue(Element node) {
		List<Element> list = node.elements();
		if (list != null && list.size() > 0) {
			Element e = list.get(0);
			return getFirstChildValue(e);
		} else {
			return formatValue(node.getTextTrim());
		}
	}

	public static String formatValue(String str) {
		return str.replace("<strong>", "").replace("</strong>", "").replace("&nbsp;", " ").replace("<br/>", "\r\n")
				.replace("<br />", "\r\n").replace("<p>", "").replace("</p>", "")
				.replace("<span style=\"white-space:nowrap;\">", "")
				.replace("</span>", "")
				;
	}

	@Deprecated
	@SuppressWarnings("unchecked")
	public static void getNodes(Element node) {
		System.out.println("---------start---------");
		System.out.println("当前节点：" + node.getName() + " " + node.getTextTrim());
		List<Element> list = node.elements();
		ArrayList<Object> row = new ArrayList<>();
		for (Element e : list) {
			List<Element> temp = e.elements();
			if (temp == null || temp.size() == 0) {
				row.add(e.getTextTrim());
			} else {
				getNodes(e);
			}

		}
	}
	
    //TODO remove mainTue Dec 31 13:13:22 CST 2019
//========
     //public static void main(String[] args) {
         //HSSFWorkbook wb = new HSSFWorkbook();
     //}
//========
	
}

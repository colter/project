package com.colter.project.util.common;

import com.colter.project.data.structure.tree.BinaryTree;
import com.colter.project.data.structure.tree.RedBlackTree;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;

/**
 * @author liangchao03
 * 2018/2/7
 */
public class TreeUtil {

    private static int length;

    public static <T> BinaryTree<T> reversTree(BinaryTree<T> old) {
        if (old == null) {
            return null;
        }
        BinaryTree<T> left = old.getLeftChild();
        BinaryTree<T> right = old.getRightChild();
        reversTree(left);
        reversTree(right);
        old.setRightChild(left);
        old.setLeftChild(right);
        return old;
    }

    public static <T> void visitTreeMidOrder(BinaryTree<T> source) {
        if (source != null) {
            visitTreeMidOrder(source.getLeftChild());
            visit(source);
            visitTreeMidOrder(source.getRightChild());
        }
    }

    public static <T> void visitTreePreOrder(BinaryTree<T> source) {
        if (source != null) {
            visit(source);
            visitTreePreOrder(source.getLeftChild());
            visitTreePreOrder(source.getRightChild());
        }
    }

    public static <T> void visitTreePostOrder(BinaryTree<T> source) {
        if (source != null) {
            visitTreePostOrder(source.getLeftChild());
            visitTreePostOrder(source.getRightChild());
            visit(source);
        }
    }

    public static <T> void visitTreeLevelOrder(BinaryTree<T> source) {
        Queue<BinaryTree<T>> queue = new LinkedList<>();
        if (source != null) {
            queue.offer(source);
            while (!queue.isEmpty()) {
                BinaryTree<T> pop = queue.poll();
                visit(pop);
                if (pop.getLeftChild() != null) {
                    queue.offer(pop.getLeftChild());
                }
                if (pop.getRightChild() != null) {
                    queue.offer(pop.getRightChild());
                }
            }

        }
    }

    private static <T> void visit(BinaryTree<T> source) {
        if (source != null) {
            System.out.println(source);
        } else {
            System.out.println("None");
        }
    }

    /**
     * 根据 前序遍历和中序遍历  还原二叉树
     *
     * @param preList
     * @param midList
     * @param <T>
     * @return
     */
    public static <T> BinaryTree<T> buildTreeFromPreAndMidVisit(List<T> preList, List<T> midList) {

        if (preList.size() == 0) {
            return null;
        }

        T rootNode = preList.get(0);
        BinaryTree<T> root = new BinaryTree<>(rootNode);
        int indexOfMid = midList.indexOf(rootNode);

        int newPreStartIndexLeft = 1;
        int newPreEndIndexLeft = 1 + indexOfMid;

        int newMidStartIndexLeft = 0;
        int newMidEndIndexLeft = indexOfMid;

        List<T> subLeftPre = preList.subList(newPreStartIndexLeft, newPreEndIndexLeft);
        List<T> subLeftMid = midList.subList(newMidStartIndexLeft, newMidEndIndexLeft);
        BinaryTree<T> leftChild = buildTreeFromPreAndMidVisit(subLeftPre, subLeftMid);


        int newPreStartIndexRight = newPreEndIndexLeft;
        int newPreEndIndexRight = preList.size();

        int newMidStartIndexRight = indexOfMid + 1;
        int newMidEndIndexRight = midList.size();

        List<T> subRightPre = preList.subList(newPreStartIndexRight, newPreEndIndexRight);
        List<T> subRightMid = midList.subList(newMidStartIndexRight, newMidEndIndexRight);
        BinaryTree<T> rightChild = buildTreeFromPreAndMidVisit(subRightPre, subRightMid);

        root.setLeftChild(leftChild);
        root.setRightChild(rightChild);

        return root;
    }

    /**
     * 根据 后序遍历和中序遍历  还原二叉树
     *
     * @param postList
     * @param midList
     * @param <T>
     * @return
     */
    public static <T> BinaryTree<T> buildTreeFromPostAndMidVisit(List<T> postList, List<T> midList) {

        if (postList.size() == 0) {
            return null;
        }

        T rootNode = postList.get(postList.size() - 1);
        BinaryTree<T> root = new BinaryTree<>(rootNode);
        int indexOfMid = midList.indexOf(rootNode);

        int newPostStartIndexLeft = 0;
        int newPostEndIndexLeft = indexOfMid;

        int newMidStartIndexLeft = 0;
        int newMidEndIndexLeft = indexOfMid;

        List<T> subLeftPost = postList.subList(newPostStartIndexLeft, newPostEndIndexLeft);
        List<T> subLeftMid = midList.subList(newMidStartIndexLeft, newMidEndIndexLeft);
        BinaryTree<T> leftChild = buildTreeFromPostAndMidVisit(subLeftPost, subLeftMid);


        int newPostStartIndexRight = newPostEndIndexLeft;
        int newPostEndIndexRight = postList.size() - 1;

        int newMidStartIndexRight = indexOfMid + 1;
        int newMidEndIndexRight = midList.size();

        List<T> subRightPost = postList.subList(newPostStartIndexRight, newPostEndIndexRight);
        List<T> subRightMid = midList.subList(newMidStartIndexRight, newMidEndIndexRight);
        BinaryTree<T> rightChild = buildTreeFromPostAndMidVisit(subRightPost, subRightMid);

        root.setLeftChild(leftChild);
        root.setRightChild(rightChild);

        return root;
    }

    //TODO 二叉树的最大的层数
    private static int maxLevel(BinaryTree bInaryTree) {
        return 0;
    }

    //TODO
    public static List<List<BinaryTree>> getAllPaths(BinaryTree binaryTree) {
        return null;
    }


    private static RedBlackTree findRoot(RedBlackTree node) {
        while (node.getParent() != null) {
            node = node.getParent();
        }
        return node;
    }

    public static void printTree(RedBlackTree root) {
        root = findRoot(root);
        length = root.toString().length();
        printTree(newArrayList(root));
    }

    private static String getPad() {
        int left, right;
        if (length >= 3) {
            if ((length - 3) % 2 == 0) {
                left = (length - 3) / 2;
                right = (length - 3) / 2;
            } else {
                left = (length - 3) / 2;
                right = (length - 3) / 2 + 1;
            }
        } else {
            left = 0;
            right = 0;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < left; i++) {
            sb.append("*");
        }
        sb.append("NIL");
        for (int i = 0; i < right; i++) {
            sb.append("*");
        }
        return sb.toString();
    }

    private static void printTree(List<RedBlackTree> list) {
        boolean isLast = list.stream().allMatch(tree -> tree == null || (tree.getLeftChild() == null && tree.getRightChild() == null));
        list.forEach(tree -> {
            System.out.print(tree == null ? getPad() : tree);
            System.out.print("\t");
        });
        System.out.println();
        if (isLast) {
            System.out.println();
            return;
        } else {
            List<RedBlackTree> result = list.stream().flatMap(tree -> {
                List<RedBlackTree> binary = newArrayList();
                if (tree == null) {
                    binary.add(null);
                    binary.add(null);
                } else {
                    binary.add(tree.getLeftChild());
                    binary.add(tree.getRightChild());
                }
                return binary.stream();
            }).collect(Collectors.toList());
            printTree(result);
        }
    }
}

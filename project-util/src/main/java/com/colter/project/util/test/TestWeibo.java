package com.colter.project.util.test;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class TestWeibo {
    //TODO remove mainTue Dec 31 13:13:22 CST 2019
//========
     //public static void main(String[] args) throws UnsupportedEncodingException, NoSuchAlgorithmException, Exception {
         //HttpClient client = new DefaultHttpClient();
         //SSLContext sslContext = SSLContext.getInstance("TLS");
 //
         //// TrustManagerFactory tmf =
         //// TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
         //TrustManager tmf = new X509TrustManager() {
 //
             //@Override
             //public X509Certificate[] getAcceptedIssuers() {
                 //// TODO Auto-generated method stub
                 //return null;
             //}
 //
             //@Override
             //public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                 //// TODO Auto-generated method stub
 //
             //}
 //
             //@Override
             //public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                 //// TODO Auto-generated method stub
 //
             //}
         //};
 //
         //sslContext.init(null, new TrustManager[] { tmf }, null);
         //SSLSocketFactory sf = new SSLSocketFactory(sslContext);
 //
         //Scheme scheme = new Scheme("https", sf, 443);
 //
         //client.getConnectionManager().getSchemeRegistry().register(scheme);
 //
 ////        HttpGet httpGet = new HttpGet("https://api.weibo.com/oauth2/authorize?client_id=874176112&redirect_uri=http://roadshow.sseinfo.com");
 ////
 ////        HttpResponse httpResponse = client.execute(httpGet);
 ////        HttpEntity entity = httpResponse.getEntity(); // 获取响应实体
 ////		String respContent = null; // 响应内容
 ////
 ////        if (null != entity) {
 ////			// 使用EntityUtils.getContentCharSet(entity)也可以获取响应编码,但从4.1.3开始不建议使用这种方式
 ////			respContent = EntityUtils.toString(entity, "utf-8");
 ////			// Consume response content
 ////		}
 ////        System.out.println(respContent);
         ////912c3f0e7c047e1004fbe7abbbf42875
         //
 ////        String code = "4f92a4ae671a8a1b17d3ae200f78ca5f";
 ////        System.out.println("------------------------------------------");
 ////        String url = "https://api.weibo.com/oauth2/access_token?client_id=874176112"
 ////        		+ "&client_secret=2f292726be1c5474a9b8841446b659e6"
 ////        		+ "&grant_type=authorization_code"
 ////        		+ "&code="+code
 ////        		+ "&redirect_uri=http://roadshow.sseinfo.com";
 ////
 ////        String result = post(client, url);
 ////        System.out.println(result);
 ////        JSONObject obj = JSONObject.fromObject(result);
 ////        String access_token = obj.getString("access_token");
 ////        System.out.println(access_token);
         //
         //String access_token = "2.002MIbpB0iIxJxcb17277a617fXNUC";
         //
         //String url2 = "https://api.weibo.com/2/statuses/user_timeline.json?access_token="+access_token;
         //System.out.println(url2);
         //String result2 = get(client, url2);
         //System.out.println(result2);
         //
         //String url3 = "https://api.weibo.com/2/statuses/home_timeline.json?access_token="+access_token;
         //System.out.println("----------------------");
         //String result3 = get(client, url3);
         //System.out.println(result3);
      ////   {"access_token":"2.002MIbpB0iIxJxcb17277a617fXNUC","remind_in":"157679999","expires_in":157679999,"uid":"1678575621"}
         //
         //
         //
     //}
//========
	
	public static String post(HttpClient client,String reqURL) throws ParseException, IOException{
		    HttpPost post = new HttpPost(reqURL);
	        HttpResponse httpResponse2 = client.execute(post);
	        HttpEntity entity2 = httpResponse2.getEntity(); // 获取响应实体
			String respContent2 = null; // 响应内容

	        if (null != entity2) {
				// 使用EntityUtils.getContentCharSet(entity)也可以获取响应编码,但从4.1.3开始不建议使用这种方式
				respContent2 = EntityUtils.toString(entity2, "utf-8");
				// Consume response content
			}
	        return respContent2;
	}
	
	public static String get(HttpClient client,String reqURL) throws ParseException, IOException{
	    HttpGet  get = new HttpGet(reqURL);
        HttpResponse httpResponse2 = client.execute(get);
        HttpEntity entity2 = httpResponse2.getEntity(); // 获取响应实体
		String respContent2 = null; // 响应内容

        if (null != entity2) {
			// 使用EntityUtils.getContentCharSet(entity)也可以获取响应编码,但从4.1.3开始不建议使用这种方式
			respContent2 = EntityUtils.toString(entity2, "utf-8");
			// Consume response content
		}
        return respContent2;
}

	public static String sendGetRequest(String reqURL) {
		long respLen = 0; // 响应长度
		String respContent = null; // 响应内容
		Charset respCharset = null; // 响应编码
		HttpClient httpClient = new DefaultHttpClient(); // 创建默认的httpClient实例
		HttpGet httpGet = new HttpGet(reqURL); // 创建org.apache.http.client.methods.HttpGet
		try {
			HttpResponse response = httpClient.execute(httpGet); // 执行GET请求
			HttpEntity entity = response.getEntity(); // 获取响应实体
			if (null != entity) {
				// 使用EntityUtils.getContentCharSet(entity)也可以获取响应编码,但从4.1.3开始不建议使用这种方式
				respLen = entity.getContentLength();
				respContent = EntityUtils.toString(entity, "utf-8");
				// Consume response content
			}
			// System.out.println("-----------------------------------------------------------------------");
			// System.out.println("请求地址: " + httpGet.getURI());
			// System.out.println("响应状态: " + response.getStatusLine());
			// System.out.println("响应长度: " + respLen);
			// System.out.println("响应编码: " + respCharset);
			// System.out.println("响应内容: " + respContent);
			// System.out.println("-----------------------------------------------------------------------");
		} catch (ClientProtocolException e) {
			// 该异常通常是协议错误导致:比如构造HttpGet对象时传入协议不对(将'http'写成'htp')or响应内容不符合HTTP协议要求等
		} catch (ParseException e) {
		} catch (IOException e) {
			// 该异常通常是网络原因引起的,如HTTP服务器未启动等
		} finally {
			// 关闭连接,释放资源
			httpClient.getConnectionManager().shutdown();
		}
		return respContent;
	}

}

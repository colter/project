package com.colter.project.util.numric;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author liangchao03
 * 2018/2/26
 */
public class RandomNumUtil {

    public static List<Integer> generate(int min, int max, int total) {
        assert min >= 0;
        assert max >= min;
        assert total >= 0;
        List<Integer> list = new ArrayList<>(total);
        Random random = new Random();
        int count = 1;
        while (count <= total) {
            list.add(getNum(min, max, random));
            count++;
        }
        return list;
    }


    private static int getNum(int min, int max, Random random) {
        return random.nextInt(max - min + 1) + min;
    }

}

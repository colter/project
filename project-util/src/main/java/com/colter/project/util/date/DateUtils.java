package com.colter.project.util.date;

import com.colter.project.util.string.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {
	public static final String DEFAULT_PATTERN = "yyyy-MM-dd";
	public static final Locale DEFAULT_LOCAL = Locale.US;

	public static Date parseToDate(String dateStr) {
		return parseToDate(dateStr, DEFAULT_PATTERN, DEFAULT_LOCAL);
	}

	public static Date parseToDate(String dateStr, String pattern, Locale locale) {
		if (StringUtils.trimToNull(dateStr) == null) {
			return null;
		}
		SimpleDateFormat sd = new SimpleDateFormat(pattern, locale);
		try {
			return sd.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String formatDate(Date date) {
		return formatDate(date, DEFAULT_PATTERN, DEFAULT_LOCAL);
	}

	public static String formatDate(Date date, String pattern, Locale locale) {
		String result = null;
		SimpleDateFormat sd = new SimpleDateFormat(pattern, locale);
		if (date != null) {
			result = sd.format(date);
		}
		return result;
	}

}

package com.colter.project.util.string;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StringUtils {

	/**
	 * trim the source, after that if the source equal "" then return null. else
	 * return the trimmed string
	 * 
	 * @param source
	 * @return
	 */
	public static String trimToNull(String source) {
		String target = null;
		if (source != null) {
			target = source.trim();
		}

		if (!"".equals(target)) {
			return target;
		}

		return null;
	}

	/**
	 * if the string is null or blank string, it will return true.
	 * 
	 * @param source
	 * @return
	 */
	public static boolean isBlank(String source) {
		boolean result = true;
		if (source != null) {
			result = "".equals(source.trim());
		}
		return result;
	}

	/**
	 * This method will trim the string even the string is null.(if the string
	 * is null, then the result after execute this method is also null)
	 * 
	 * @param source
	 * @return
	 */
	public static String trim(String source) {
		if (source != null) {
			return source.trim();
		} else {
			return null;
		}

	}

	/**
	 * change string to string array by delimiter
	 * 
	 * @param source
	 * @param delimiter
	 * @return
	 */
	public static String[] changeToArray(String source, String delimiter) {
		if (source != null) {
			String[] dest = source.split(delimiter);
			return dest;
		}
		return null;
	}

	/**
	 * change string to list by delimiter
	 * 
	 * @param source
	 * @param delimiter
	 * @return
	 */
	public static List<String> changeToList(String source, String delimiter) {
		List<String> list = null;
		if (source != null) {
			String[] dest = changeToArray(source, delimiter);
			list = Arrays.asList(dest);
		}
		return list;
	}

	/**
	 * add many strings to one;
	 * 
	 * @param strings
	 * @return
	 */
	public static String addStrings(String... strings) {
		return addStringsByDelimiter("", strings);
	}

	/**
	 * add many strings by delimiter
	 * 
	 * @param delimiter
	 * @param strings
	 * @return
	 */
	public static String addStringsByDelimiter(String delimiter, String... strings) {
		StringBuilder sb = new StringBuilder();

		for (String str : strings) {
			sb.append(str);
			sb.append(delimiter);
		}

		if (sb.length() > delimiter.length()) {
			return sb.substring(0, sb.length() - delimiter.length());
		}

		return sb.toString();

	}

	/**
	 * split spring by separator
	 */
	public static String[] split2(String source, String... separators) {
		List<String> ss = new ArrayList<>();
		ss.add(source);
		for (String s : separators) {
			List<String> temp = new ArrayList<>();
			for (String str : ss) {
				temp.addAll(splitOne(str, s));
			}
			ss = new ArrayList<>();
			ss.addAll(temp);
		}
		String[] array = new String[ss.size()];
		array = ss.toArray(array);
		return array;
	}

	private static List<String> splitOne(String source, String separator) {
		List<String> list = new ArrayList<>();
		String[] temp;
		temp = source.split(separator);
		list.addAll(Arrays.asList(temp));
		return list;
	}

	public static void print(String[] source) {
		if (source != null) {
			for (String s : source) {
				System.out.println(s + " ");
			}
		}
	}

	public static String[] split(String source, String... separators) {
		if (source == null) {
			return null;
		}

		if (separators != null && separators.length > 0) {
			String s1 = separators[0];
			for (int i = 0; i < separators.length; i++) {
				if (i == 0) {
					continue;
				}
				source = source.replace(separators[i], s1);
			}
			if ("\\".equals(s1)) {
				s1 = "\\\\";
			} else if (".".equals(s1)) {
				s1 = "\\.";
			}

			return source.split(s1);
		}

		return null;
	}

	public static String string2Unicode(String string) {

		StringBuffer unicode = new StringBuffer();

		for (int i = 0; i < string.length(); i++) {

			// 取出每一个字符
			char c = string.charAt(i);

			// 转换为unicode
			unicode.append("\\u" + Integer.toHexString(c));
		}

		return unicode.toString();
	}

	public static String unicode2String(String unicode) {

		StringBuffer string = new StringBuffer();

		String[] hex = unicode.split("\\\\u");

		for (int i = 1; i < hex.length; i++) {

			// 转换出每一个代码点
			int data = Integer.parseInt(hex[i], 16);

			// 追加成string
			string.append((char) data);
		}

		return string.toString();
	}
	
}

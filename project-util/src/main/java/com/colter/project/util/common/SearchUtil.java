package com.colter.project.util.common;

import com.colter.project.util.numric.RandomNumUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author liangchao03
 * 2018/2/27
 */
public class SearchUtil {

    public static List find(int result, Integer[]... a) {
        AtomicInteger atomicInteger = new AtomicInteger(0);
        ExecutorService pool = Executors.newFixedThreadPool(3);
        List<Object> list = new ArrayList<>();
        for (Integer[] array : a) {
            pool.submit(() -> {
                for (int i = 0; i < array.length; i++) {
                    System.out.println(array[i] + " " + Arrays.toString(array));
                    if (array[i] == result) {
                        if (atomicInteger.get() != 0) {
                            break;
                        }
                        if (atomicInteger.compareAndSet(0, i)) {
                            list.add(i);
                            list.add(Arrays.toString(array));
                            break;
                        }
                    }
                }
            });
        }
        pool.shutdown();
        try {
            pool.awaitTermination(1000, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return list;
    }

    //TODO remove mainTue Dec 31 13:13:22 CST 2019
//========
     //public static void main(String[] args) {
         //Integer[][] result = new Integer[3][1];
         //for (int i = 0; i < 3; i++) {
             //List<Integer> list = RandomNumUtil.generate(8,10,10);
             //Integer[] arr = list.toArray(new Integer[list.size()] );
             //result[i] = arr;
         //}
 //
         //List list = find(10,result);
         //System.out.println(list);
     //}
//========

}

package com.colter.project.util.httpclient;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.util.EntityUtils;

public class HttpClientUtil {
	/**
	 * 
	 * @param url
	 * @param method
	 * @return
	 * @throws Exception
	 */
	public static String executeSshGetIgnoreCert(String url) throws Exception {
		HttpClient client = new DefaultHttpClient();
		SSLContext sslContext = SSLContext.getInstance("TLS");

		TrustManager tmf = new X509TrustManager() {

			@Override
			public X509Certificate[] getAcceptedIssuers() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				// TODO Auto-generated method stub

			}

			@Override
			public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				// TODO Auto-generated method stub

			}
		};

		sslContext.init(null, new TrustManager[] { tmf }, null);
		SSLSocketFactory sf = new SSLSocketFactory(sslContext);

		Scheme scheme = new Scheme("https", sf, 443);

		client.getConnectionManager().getSchemeRegistry().register(scheme);

		HttpResponse httpResponse = null;
		String responseContent = null;
		HttpGet httpGet = new HttpGet(url);
		httpResponse = client.execute(httpGet);

		if (httpResponse != null) {
			HttpEntity entity = httpResponse.getEntity(); // 获取响应实体
			if (null != entity) {
				responseContent = EntityUtils.toString(entity, "UTF-8");
			}
		}
		return responseContent;
	}

	public static String executeSshPostIgnoreCert(String url, Map<String, String> params) throws Exception {
		HttpClient client = new DefaultHttpClient();
		SSLContext sslContext = SSLContext.getInstance("TLS");

		TrustManager tmf = new X509TrustManager() {

			@Override
			public X509Certificate[] getAcceptedIssuers() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				// TODO Auto-generated method stub

			}

			@Override
			public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				// TODO Auto-generated method stub

			}
		};

		sslContext.init(null, new TrustManager[] { tmf }, null);
		SSLSocketFactory sf = new SSLSocketFactory(sslContext);

		Scheme scheme = new Scheme("https", sf, 443);

		client.getConnectionManager().getSchemeRegistry().register(scheme);

		String responseContent = null;

		client.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 1000);
		client.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 2000);
		HttpPost httpPost = new HttpPost(url);
		if (null != params) {
			List<NameValuePair> formParams = new ArrayList<NameValuePair>();
			for (Map.Entry<String, String> entry : params.entrySet()) {
				formParams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
			}
			httpPost.setEntity(new UrlEncodedFormEntity(formParams, "UTF-8"));
		}
		HttpResponse response = client.execute(httpPost);
		HttpEntity entity = response.getEntity();
		if (null != entity) {
			responseContent = EntityUtils.toString(entity, "UTF-8");
		}
		return responseContent;
	}

    //TODO remove mainTue Dec 31 13:13:22 CST 2019
//========
     //public static void main(String[] args) throws Exception {
         //System.out.println(executeSshGetIgnoreCert("https://www.amazon.cn"));
     //}
//========
}

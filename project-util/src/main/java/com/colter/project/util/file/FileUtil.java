package com.colter.project.util.file;

import com.colter.project.util.string.StringUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

public class FileUtil {

    /**
     * 拆分文件
     *
     * @param filePath 文件路径
     * @param count    拆分总数
     */
    public static void cut(String filePath, String suffix, int count) {
        assert count > 0;
        try {
            RandomAccessFile file = new RandomAccessFile(filePath, "rw");
            long length = file.length();
            //修正长度，避免多出来的一截
            long part = (length / count) + 1;
            int start = 0;
            byte[] buffer = new byte[1024];
            while (start < count) {
                long actualBlockSize = part;
                file.seek(start * part);
                String partPath = filePath + "-" + start + suffix;
                RandomAccessFile partFile = new RandomAccessFile(partPath, "rw");
                int len = 0;
                while (-1 != (len = file.read(buffer))) {
                    if (actualBlockSize - len >= 0) {//查看是否读够
                        //写出
                        partFile.write(buffer, 0, len);
                        actualBlockSize -= len;//剩余量
                    } else {//写出最后一次的剩余量
                        partFile.write(buffer, 0, (int) actualBlockSize);
                        break;
                    }
                }
                partFile.close();
                start++;
            }
            file.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Cut Done.");
        }
    }

    public static void merge(String pathPrefix, String suffix, int count, String fileName, String destPath) {
        SequenceInputStream seq = null;
        FileOutputStream fos = null;
        try {
            List<InputStream> lists = newArrayList();
            for (int i = 0; i < count; i++) {
                String sourcePath = pathPrefix + "/" + fileName + "-" + i + suffix;
                File file = new File(sourcePath);
                if (file.exists()) {
                    lists.add(new FileInputStream(file));
                }
            }
            seq = new SequenceInputStream(Collections.enumeration(lists));
            createFileIfNotExists(destPath);
            fos = new FileOutputStream(destPath + "/" + fileName);
            int len = 0;
            byte buf[] = new byte[1024];
            while ((len = seq.read(buf)) != -1) {
                fos.write(buf, 0, len);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
                if (seq != null) {
                    seq.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("Merge Done.");
        }
    }

    public static List<String> readFileByLine(String path) {
        List<String> results = new ArrayList<>();
        BufferedReader br = null;
        try {
            File file = new File(path);
            br = new BufferedReader(new FileReader(file));
            String temp;
            while ((temp = br.readLine()) != null) {
                results.add(temp);
            }
            return results;
        } catch (IOException e) {
            return new ArrayList<>();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {

                }
            }
        }

    }

    public static void writeFileByLine(List<String> lines, String path) {
        createFileIfNotExists(path);
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(path));
            for (String line : lines) {
                bw.write(line);
                bw.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void writeFile(String content, String path) {
        writeFileByLine(newArrayList(content), path);
    }

    public static void copyFolderIgnoreFolder(String srcFolderPath, String destFolderPath, List<String> ignoreFolder) {
        copyFolderIgnoreFolder(srcFolderPath, destFolderPath, srcFolderPath, ignoreFolder, newArrayList());
    }

    public static void copyFolderIgnoreFile(String srcFolderPath, String destFolderPath, List<String> ignoreFiles) {
        copyFolderIgnoreFolder(srcFolderPath, destFolderPath, srcFolderPath, newArrayList(), ignoreFiles);
    }

    public static void copyFolder(String srcFolderPath, String destFolderPath) {
        copyFolderIgnoreFolder(srcFolderPath, destFolderPath, srcFolderPath, newArrayList(), newArrayList());
    }

    private static void copyFolderIgnoreFolder(String srcFolderPath, String destFolderPath, String prefix, List<String> ignoreFolder, List<String> ignoreFiles) {
        File srcFolder = new File(srcFolderPath);
        File destFolder = new File(destFolderPath);
        assert srcFolder.isDirectory();
        assert destFolder.isDirectory();
        File[] files = srcFolder.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                boolean shouldIgnore = ignoreFolder.stream().anyMatch(ignore -> file.getAbsolutePath().endsWith(ignore));
                if (!shouldIgnore) {
                    copyFolderIgnoreFolder(file.getAbsolutePath(), destFolderPath, prefix, ignoreFolder, ignoreFiles);
                }
            } else {
                String srcPath = file.getAbsolutePath().replace("\\", "/");
                String destPath = srcPath.replace(prefix, destFolderPath);
                boolean shouldIgnore = ignoreFiles.stream().anyMatch(ignore -> srcPath.endsWith(ignore));
                if (!shouldIgnore) {
                    copyFile(srcPath, destPath);
                }
            }
        }
    }

    /**
     * If the pathName is a folder,then create the folder.If the pathName is a
     * file,create the parent folder and the file.
     *
     * @param pathName
     * @return
     */
    public static boolean createFileIfNotExists(String pathName) {
        File file = new File(pathName);
        if (file.exists()) {
            return true;
        }

        if (pathName.lastIndexOf("\\") < pathName.lastIndexOf(".")
                || pathName.lastIndexOf("/") < pathName.lastIndexOf(".")) {
            try {
                String[] array = StringUtils.split(pathName, "\\");
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < array.length; i++) {
                    if (i != array.length - 1) {
                        sb.append(array[i]);
                        sb.append("\\");
                    }
                }

                File tmp = new File(sb.toString());
                if (!tmp.exists()) {
                    tmp.mkdirs();
                }

                sb.append(array[array.length - 1]);
                tmp = new File(sb.toString());
                if (!tmp.exists()) {
                    tmp.createNewFile();
                }

                return file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return file.mkdirs();
        }

    }

    /**
     * Get the all the folder and file name under this path.
     *
     * @param pathName
     * @return
     */
    public static List<String> getAllAbsolutePathUnderFolder(String pathName) {
        List<String> list = new ArrayList<>();
        File parent = new File(pathName);
        iteratorFolder(list, parent, null, null, false);
        return list;
    }

    public static List<String> getAllFileNameUnderFolder(String pathName) {
        List<String> list = new ArrayList<>();
        File parent = new File(pathName);
        iteratorFolder(list, parent, null, null, true);
        return list;
    }

    public static List<String> getAllFileNameUnderFolderExcept(String pathName, String except) {
        List<String> list = new ArrayList<>();
        File parent = new File(pathName);
        iteratorFolder(list, parent, except, null, true);
        return list;
    }


    public static List<String> getAllAbsolutePathUnderFolderEndWith(String pathName, String suffix) {
        List<String> list = new ArrayList<>();
        File parent = new File(pathName);
        iteratorFolder(list, parent, null, suffix, false);
        return list;
    }

    public static List<String> getAllFileNameUnderFolderEndWith(String pathName, String suffix) {
        List<String> list = new ArrayList<>();
        File parent = new File(pathName);
        iteratorFolder(list, parent, null, suffix, true);
        return list;
    }

    /**
     * Get the all the folder and file name under this path except the given path.
     *
     * @param pathName
     * @param except
     * @return
     */
    public static List<String> getAllAbsolutePathUnderFolderExcept(String pathName, String except) {
        List<String> list = new ArrayList<>();
        File parent = new File(pathName);
        iteratorFolder(list, parent, except, null, false);
        return list;
    }

    private static void iteratorFolder(List<String> list, File parent, String except, String suffix, boolean onlyFileName) {
        if (!parent.exists()) {
            return;
        }

        if (except == null) {
            if (parent.isDirectory()) {
                File[] children = parent.listFiles();
                if (children == null || children.length == 0) {
                    resolveFileName(list, parent, suffix, onlyFileName);
                } else {
                    for (File child : children) {
                        iteratorFolder(list, child, except, suffix, onlyFileName);
                    }
                }

            } else {
                resolveFileName(list, parent, suffix, onlyFileName);
            }
        } else {
            if (parent.isDirectory()) {
                File[] children = parent.listFiles();
                if (children == null || children.length == 0) {
                    if (!parent.getAbsolutePath().endsWith(except)) {
                        if (suffix == null) {
                            list.add(parent.getName());
                        } else if (parent.getName().endsWith(suffix)) {
                            list.add(parent.getName());
                        }
                    }
                } else {
                    for (File child : children) {
                        if (!child.getAbsolutePath().endsWith(except)) {
                            iteratorFolder(list, child, except, suffix, onlyFileName);
                        }
                    }
                }
            } else {
                if (!parent.getAbsolutePath().endsWith(except)) {
                    resolveFileName(list, parent, suffix, onlyFileName);
                }
            }
        }

    }

    private static void resolveFileName(List<String> list, File parent, String suffix, boolean onlyFileName) {
        if (onlyFileName) {
            if (suffix == null) {
                list.add(parent.getName());
            } else if (parent.getName().endsWith(suffix)) {
                list.add(parent.getName());
            }
        } else {
            if (suffix == null) {
                list.add(parent.getAbsolutePath());
            } else if (parent.getAbsolutePath().endsWith(suffix)) {
                list.add(parent.getAbsolutePath());
            }
        }
    }

    public static boolean copyFileToFolder(String source, String folder) {
        if (folder.endsWith("\\") || folder.endsWith("/")) {
            folder = folder.substring(0, folder.length() - 1);
        }
        String name = new File(source).getName();
        return copyFile(source, folder + "/" + name);
    }

    public static boolean copyFile(String source, String target) {
        String full = target;
        int index = full.lastIndexOf("\\");
        if (index == -1) {
            index = full.lastIndexOf("/");
        }
        String folder = full.substring(0, index);
        File fo = new File(folder);
        if (!fo.isDirectory()) {
            fo.mkdirs();
        }

        File fileOld = new File(source);
        if (fileOld.exists()) {
            File fileTarget = new File(target);
            BufferedOutputStream stream = null;
            FileOutputStream fstream = null;
            InputStream in = null;
            try {
                in = new FileInputStream(fileOld); // 读入原文件
                byte[] bytes = toByteArray(in);
                fstream = new FileOutputStream(fileTarget);
                stream = new BufferedOutputStream(fstream);
                stream.write(bytes);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("生成文件错误： source:" + source + " target:" + target);
                return false;
            } finally {
                try {
                    in.close();
                    stream.close();
                    fstream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println("关闭发生问题.");
                    return false;
                }
            }
        } else {
            System.out.println("源文件不存在：" + source);
        }
        return true;
    }

    /**
     * These method copy from apache-io-util
     *
     * @param input
     * @return
     * @throws IOException
     */
    // apache io util  start
    private static byte[] toByteArray(InputStream input) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        copy(input, output);
        return output.toByteArray();
    }

    private static int copy(InputStream input, OutputStream output) throws IOException {
        long count = copyLarge(input, output);
        if (count > 2147483647L) {
            return -1;
        }
        return (int) count;
    }

    private static long copyLarge(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[4096];
        long count = 0L;
        int n = 0;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
            count += n;
        }
        return count;
    }

    // apache io util  end

}

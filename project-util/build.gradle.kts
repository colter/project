dependencies {
    api(project(":project-data"))

    //编码
    api("commons-codec:commons-codec:1.10")
    api("commons-lang:commons-lang:2.6")

    //POI Excel
    api("org.apache.poi:poi:3.12")
    api("org.apache.poi:poi-ooxml:3.12")
    api("org.apache.poi:poi-ooxml-schemas:3.12")

    //HTTP
    api("org.apache.httpcomponents:httpclient:4.0")

    //QR Code
    api("com.google.zxing:javase:3.2.0")

    //Dom4j
    api("dom4j:dom4j:1.6")
}
val projectIds = mapOf("group" to "com.colter.project", "version" to "1.0")
val kotlinVersion = "1.3.61"

description = """"""

buildscript {
    val kotlinVersion = "1.3.61"
    repositories {
        mavenCentral()
    }

    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion")
    }

    repositories {
        mavenLocal()
        maven(url = "http://maven.aliyun.com/nexus/content/groups/public")
        mavenCentral()
        jcenter()
    }
}


allprojects {

}


subprojects {
    apply(plugin = "java")
    apply(plugin = "maven")
    apply(plugin = "kotlin")
    group = "$projectIds.group"
    version = "$projectIds.version"

    apply {
        plugin("org.jetbrains.kotlin.jvm")
    }

    repositories {
        mavenLocal()
        maven(url = "http://maven.aliyun.com/nexus/content/groups/public")
        mavenCentral()
        jcenter()
    }
    val implementation by configurations
    val testImplementation by configurations
    val annotationProcessor by configurations
    val api by configurations
    val compileOnly by configurations

    dependencies {
        //        compileOnly("org.projectlombok:lombok:1.18.10")
//        annotationProcessor("org.projectlombok:lombok:1.18.10")
        //log
        implementation("org.slf4j:slf4j-api:1.7.30")
        testImplementation("org.apache.logging.log4j:log4j-slf4j-impl:2.13.0")
        implementation("org.apache.logging.log4j:log4j-api:2.13.0")
        implementation("org.apache.logging.log4j:log4j-core:2.13.0")

        implementation("com.google.guava:guava:28.1-jre")
        testImplementation("org.junit.platform:junit-platform-launcher:1.2.0")
        testImplementation("org.junit.jupiter:junit-jupiter-engine:5.2.0")

        //添加Kotlin标准库依赖
        implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlinVersion")
    }

    //每个module创建src文件夹和resource文件夹
//    task ("create") {
//        doLast {
//            sourceSets*.kotlin.srcDirs*.each { it.mkdirs() }
//            sourceSets*.resources.srcDirs*.each { it.mkdirs() }
//            print "Create Done!"
//        }
//    }

    tasks.withType<JavaCompile> {
        sourceCompatibility = "1.8"
        targetCompatibility = "1.8"
        options.encoding = "UTF-8"
    }
}


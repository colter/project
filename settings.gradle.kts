rootProject.buildFileName = "build.gradle.kts"
rootProject.name = "project"
include("project-util")
include("project-data")
include("project-demo")
include("project-problem")

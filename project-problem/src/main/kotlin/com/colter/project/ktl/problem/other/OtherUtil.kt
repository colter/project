package com.colter.project.ktl.problem.other

import java.io.File
import java.util.*

/**
 * @author liangchao
 * created on 2019/12/31 2:16
 * desc:
 *
 */

fun formatJavaFile(current: File) {
    if (current.isDirectory) {
        for (f: File in current.listFiles()) {
            formatJavaFile(f)
        }
    } else {
        formatMain(current)
    }
}

fun formatMain(current: File) {
    println(current.absolutePath)
    if (!current.name.endsWith(".java")) {
        return
    }

    var fileResults: MutableList<String> = current.readLines().toMutableList()
    var count = 0
    var edited = false
    var addFlag = false
    for ((index, line) in fileResults.withIndex()) {
        if (line.indexOf("public static void main(String[] args)") != -1) {
            val left = sumBlank(line)
            fileResults[index] = "    //TODO remove main" + Date() + "\r\n//========\r\n" + padding(left) + "//" + line.trim() + "\r\n"
            addFlag = true
            edited = true
            count = 1
        } else {
            if (addFlag && count >= 0) {
                val left = sumBlank(line)
                fileResults[index] = padding(left) + "//" + line.trim() + "\r\n"
                count += line.toCharArray().filter { it == '{' }.count()
                count -= line.toCharArray().filter { it == '}' }.count()
                if (count == 0) {
                    addFlag = false
                    fileResults[index] = fileResults[index] + "//========\n"
                }
            } else {
                //if (index != fileResults.size - 1)
                fileResults[index] = line + "\r\n"
            }
        }
    }
    if (edited) {
        writeContent(current, fileResults)
    }
}

fun writeContent(current: File, fileResults: MutableList<String>) {
    current.writeText(listToString(fileResults))
}

fun listToString(fileResults: MutableList<String>): String {
    var result = ""
    for (line in fileResults) {
        result += line
    }
    return result
}

fun sumBlank(line: String): Int {
    var temp = line.replace("\t", "    ") + "A"
    return temp.length - temp.trim().length
}

fun padding(left: Int): String {
    var result = ""
    for (i in 0..left) {
        result += " "
    }
    return result
}

package com.colter.project.ktl.problem.other

import java.io.File


/**
 * @author liangchao
 * creation time:  2019/12/15 16:36
 * desc:
 */

fun main() {
    test2()
}


fun test2() {
    var file = File("C:\\test.txt")
    var lists = file.readLines()
    var next = -1;
    var count = mutableListOf<String>()
    var result = mutableListOf<String>()
    for ((index, e) in lists.withIndex()) {
        if (e.trim() == "卡号") {
            next = index + 1
        }
        if (index == next) {
            result.add(e.trim())
        }

        if (e.trim().startsWith("JDV")) {
            count.add(e.trim())
        }
    }

    result.forEach { println(it) }
    println(count.size)
}

fun test1() {
    var file = File("C:\\test.txt")
    var lists = file.readLines()
    var card: String = ""
    var password: String = ""
    var resultList = mutableListOf<String>()
    for (str in lists) {
        if (str.startsWith("JDV")) {
            card = str
        }

        if (!str.startsWith("202") && str.indexOf("-") != -1) {
            password = str
            resultList.add("$password")
        }

        if (str.indexOf("111111") != -1) {
            resultList.stream().forEach { println(it) }
            println()
            resultList.clear()
        }

    }
}
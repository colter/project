package com.colter.project.years.algorithm;

/**
 * @author liangchao03
 * @date 2017/7/17
 */

/**
 * 1 2 5组成100，一共有多少种组法。比如100个1,80个1，2个5等等
 *
 * @param
 */
public class Algorithm5 {
    public int getTimes(int total) {
        int sum = 0;
        int start = 0;
        while (start <= total / 5) {
            int remind = total - start * 5;
            sum = sum + (remind / 2 + 1);
            start ++;
        }
        return sum;
    }

    public int getTestTimes(int total) {
        int test = 0;
        for (int i = 0; i <= total; i++) {
            for (int j = 0; j <= total / 2; j++) {
                for (int k = 0; k <= total / 5; k++) {
                    if (i * 1 + j * 2 + k * 5 == total) {
                        test++;
                    }
                }
            }
        }
        return test;
    }
}

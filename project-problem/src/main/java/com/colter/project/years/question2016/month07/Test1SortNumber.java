package com.colter.project.years.question2016.month07;

import java.util.Set;
import java.util.TreeSet;

/**
 * 【程序11】 题目：有1、2、3、4个数字，能组成多少个互不相同且无重复数字的三位数？都是多少？
 * 1.程序分析：可填在百位、十位、个位的数字都是1、2、3、4。组成所有的排列后再去 掉不满足条件的排列。
 * 
 * @author user
 *
 */
public class Test1SortNumber {
    //TODO remove mainTue Dec 31 13:13:22 CST 2019
//========
     //public static void main(String[] args) {
         //System.out.println(getNums().size() + " " + getNums());
     //}
//========

	public static Set<Integer> getNums() {
		Set<Integer> set = new TreeSet<>();
		for (int i = 1; i <= 4; i++) {
			for (int j = 1; j < 4; j++) {
				for (int k = 1; k < 4; k++) {
					if (i != j && j != k && i != k) {
						int result = i * 100 + j * 10 + k * 1;
						set.add(result);
					}
				}
			}
		}
		return set;
	}
}

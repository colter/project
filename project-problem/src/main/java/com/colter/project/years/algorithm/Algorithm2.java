package com.colter.project.years.algorithm;

import java.util.List;

/**
 * 2给定一颗二叉树，要求输出二叉树的深度以及中序遍历二叉树得到的序列。本题假设二叉树的结点数不超过1000。
 * Input
 * 输入数据分为多组，第一行是测试数据的组数n，下面的n行分别代表一棵二叉树。每棵二叉树的结点均为正整数，数据为0代表当前结点为空，数据为-1代表二叉树数据输入结束，-1不作处理。二叉树的构造按照层次顺序（即第1层1个整数，第2层2个，第3层4个，第4层8个…….，如果某个结点不存在以0代替）
 * Output
 * 输出每棵二叉树的深度以及中序遍历二叉树得到的序列。
 * 输入示例：
 * 2
 * 1 -1
 * 1 2 0 3 4 -1
 * 输出示例：
 * 1 1
 * 3 3 2 4 1
 */
public class Algorithm2 {
    public Entry get(int[] arr) {
        int length = arr.length;
        int now = 0;

        return null;
    }

    private static int getLeftChildIndex(int now) {
        return now * 2 + 1;
    }

    private static int getRightChildIndex(int now) {
        return now * 2 + 2;
    }
}

class Entry {
    private int deep;
    private List<Integer> list;

    public int getDeep() {
        return deep;
    }

    public void setDeep(int deep) {
        this.deep = deep;
    }

    public List<Integer> getList() {
        return list;
    }

    public void setList(List<Integer> list) {
        this.list = list;
    }
}
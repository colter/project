package com.colter.project.years.algorithm;

/**
 * @author liangchao03
 * @date 2017/7/17
 */

/**
 * 斐波那契数列 f(1) = 1 f(2) = 2
 * 从0 开始
 * f(m)为跳到m上，那么等于f(m-1) +f(m-2)（跳到m-1，跳一阶到m，或者跳到m-2 跳2阶到m）
 */
public class Algorithm4 {
    public int getTimes(int m) {
        if (m == 1) {
            return 1;
        }
        if (m == 2) {
            return 2;
        }
        return getTimes(m - 1) + getTimes(m - 2);
    }
}

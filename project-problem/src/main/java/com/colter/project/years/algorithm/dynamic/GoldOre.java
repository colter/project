package com.colter.project.years.algorithm.dynamic;

/**
 * @author liangchao
 * creation time:  2020/2/6 19:27
 * desc:
 */

public class GoldOre {

    public static int getMaxGold(int worker, int[] needWorker, int[] goldOre) {
        return getMaxGold(worker, goldOre.length, needWorker, goldOre);
    }

    private static int getMaxGold(int worker, int canDigOre, int[] needWorker, int[] goldOre) {
        if (worker == 0 || canDigOre == 0) {
            return 0;
        }

        if (worker < needWorker[canDigOre - 1]) {
            return getMaxGold(worker, canDigOre - 1, needWorker, goldOre);
        }

        int digIt = getMaxGold(worker - needWorker[canDigOre - 1], canDigOre - 1, needWorker, goldOre) + goldOre[canDigOre - 1];
        int notDigIt = getMaxGold(worker, canDigOre - 1, needWorker, goldOre);
        return Math.max(digIt, notDigIt);
    }
}


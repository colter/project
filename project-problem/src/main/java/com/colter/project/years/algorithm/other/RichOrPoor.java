package com.colter.project.years.algorithm.other;

import java.util.Arrays;
import java.util.Random;

/**
 * @Author liangchao
 * created on 2019/3/23 20:06
 * desc: 100个人，每人100元，每次随机给另外一个人。最后贫富差距越来越大
 */
public class RichOrPoor {

    //TODO remove mainTue Dec 31 13:13:22 CST 2019
//========
     //public static void main(String[] args) {
         //int[] money = new int[100];
         //for (int i = 0; i < 100; i++) {
             //money[i] = 100;
         //}
         //for (int i = 0; i < 10000; i++) {
             //changeMoney(money);
         //}
         //Arrays.sort(money);
         //System.out.println(Arrays.toString(money));
     //}
//========


    public static void changeMoney(int[] money) {
        for (int i = 0; i < money.length; i++) {
            Random random = new Random();
            int choose;
            do {
                choose = random.nextInt(money.length);
            } while (choose == i);

            money[i] = money[i] - 1;
            money[choose] = money[choose] + 1;
        }
    }

}

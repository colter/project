package com.colter.project.years.algorithm.hanoitower;

import java.util.Objects;

public class GoldCircle implements Comparable<GoldCircle> {

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    private int size;

    public GoldCircle(int size) {
        this.size = size;
    }

    @Override
    public int compareTo(GoldCircle o) {
        if (this.size > o.getSize()) {
            return 1;
        } else if (this.size < o.getSize()) {
            return -1;
        }
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GoldCircle that = (GoldCircle) o;
        return size == that.size;
    }

    @Override
    public int hashCode() {

        return Objects.hash(size);
    }

    @Override
    public String toString() {
        return String.valueOf(size);
    }
}

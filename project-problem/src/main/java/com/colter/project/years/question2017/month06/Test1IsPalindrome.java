package com.colter.project.years.question2017.month06;

/**
 * Created by LC on 2017/6/22.
 */
//【程序25】   题目：一个5位数，判断它是不是回文数。即12321是回文数，个位与万位相同，十位与千位相同。
public class Test1IsPalindrome {
    public static boolean isPalindrome(String str) {
        if (str == null) {
            return true;
        }
        int i = 0;
        int j = str.length() - 1;
        while (i < j) {
            if (str.charAt(i) != str.charAt(j)) {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }
}

package com.colter.project.years.algorithm.hanoitower;

import com.google.common.collect.Lists;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 汉诺塔
 */
public class HanoiTower {

    public static List<GoldCircle> generateGoldCircle(int lengh) {
        List<GoldCircle> goldCircles = Lists.newArrayList();
        if (lengh <= 0) {
            return goldCircles;
        }

        for (int i = lengh; i > 0; i--) {
            GoldCircle goldCircle = new GoldCircle(i);
            goldCircles.add(goldCircle);
        }
        return sort(goldCircles);
    }

    private static List<GoldCircle> sort(List<GoldCircle> circles) {
        Comparator<GoldCircle> comparator = GoldCircle::compareTo;
        return circles.stream().sorted(comparator.reversed()).collect(Collectors.toList());
    }

    public static List<GoldCircle> listA = Lists.newArrayList();
    public static List<GoldCircle> listB = Lists.newArrayList();
    public static List<GoldCircle> listC = Lists.newArrayList();

    public static void move() {
        listA = generateGoldCircle(5);
        move(listA.size(),listA,listB,listC);
    }

    private static void move(int n, List<GoldCircle> from, List<GoldCircle> buffer, List<GoldCircle> to) {
        if (n == 1) {
            GoldCircle g = from.get(from.size()-1);
            from.remove(g);
            to.add(g);
            System.out.println(listA);
            System.out.println(listB);
            System.out.println(listC);
            System.out.println("-----------");
        } else {
            move(n - 1, from, to, buffer);
            move(1, from, buffer, to);
            move(n - 1, buffer, from, to);
        }
    }

    //TODO remove mainTue Dec 31 13:13:22 CST 2019
//========
     //public static void main(String[] args) {
         //move();
     //}
//========
}

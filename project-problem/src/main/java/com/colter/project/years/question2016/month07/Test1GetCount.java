package com.colter.project.years.question2016.month07;

/**
 * Given a non-negative integer n, count all numbers with unique digits, x,
 * where 0 ≤ x < 10n.
 * 
 * Example: Given n = 2, return 91. (The answer should be the total numbers in
 * the range of 0 ≤ x < 100, excluding [11,22,33,44,55,66,77,88,99])
 * 
 * Show Hint
 * 
 * @author user
 *
 */
public class Test1GetCount {
    //TODO remove mainTue Dec 31 13:13:22 CST 2019
//========
     //public static void main(String[] args) {
         //Test1GetCount t = new Test1GetCount();
         //System.out.println(t.countNumbersWithUniqueDigits(3));
     //}
//========

	public int countNumbersWithUniqueDigits(int n) {
		int max = (int) Math.pow(10, n);
		int count = 0;
		for (int i = 0; i < max; i++) {
			if (isCount(i)) {
				count++;
			}
		}
		return count;
	}

	public boolean isCount(int num) {
		String str = String.valueOf(num);
		if (str.length() > 1) {
			for (int i = 0; i < str.length(); i++) {
				String s = String.valueOf(str.charAt(i));
				if (str.indexOf(s) != str.lastIndexOf(s)) {
					return false;
				}
			}
		}
		return true;
	}
}

package com.colter.project.years.question2016.month07;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/***
 * 题目：将一个正整数分解质因数。例如：输入90,打印出90=2*3*3*5。 程序分析：对n进行分解质因数，应先找到一个最小的质数k，然后按下述步骤完成：
 * (1)如果这个质数恰等于n，则说明分解质因数的过程已经结束，打印出即可。 (2)如果n <>
 * k，但n能被k整除，则应打印出k的值，并用n除以k的商,作为新的正整数你,重复执行第一步。
 * (3)如果n不能被k整除，则用k+1作为k的值,重复执行第一步。
 * 
 * @author LC
 *
 */
public class Test1GetPrime {
	@SuppressWarnings("rawtypes")
	public static List list = new ArrayList();
	
    //TODO remove mainTue Dec 31 13:13:22 CST 2019
//========
     //public static void main(String[] args) {
         //Scanner scanner = new Scanner(System.in);
         //int result = scanner.nextInt();
         //if(isPrime(result)){
             //System.out.println("质数，不能分解。");
         //}
         //getPrime(result);
         //System.out.println(list);
         //
         //scanner.close();
     //}
//========

	@SuppressWarnings("unchecked")
	public static void getPrime(int result){
		int start = 2;
		while(start <= result){
			if(result % start == 0){
				list.add(start);
				result = result / start;
			}else{
				start = getNextPrime(start);
			}
		}

	}
	
	public static boolean isPrime(int num) {
		boolean flag = true;
		if (num < 2) {
			return false;
		} else if (num == 2) {
			return true;
		}
		int j = 2;
		while (j < num) {
			if (num % j == 0) {
				return false;
			}
			j++;
		}
		return flag;
	}

	public static int getNextPrime(int num) {
		num++;
		while (!isPrime(num)) {
			num++;
		}
		return num;
	}
}

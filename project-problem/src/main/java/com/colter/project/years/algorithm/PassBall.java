package com.colter.project.years.algorithm;

import java.util.Random;

/**
 * Created by liangchao on 17-12-4.
 * A B C 围成一圈 相互传球，只能传给左边或者右边， 传递6次之后  到A手里的概率是多少。
 * A B C D 传8次
 */
public class PassBall {
    private static Random random = new Random();
    //TODO remove mainTue Dec 31 13:13:22 CST 2019
//========
     //public static void main(String[] args) {
         //People a = new People("a");
         //People b = new People("b");
         //People c = new People("c");
         //a.setPrev(c);
         //a.setNext(b);
 //
         //b.setPrev(a);
         //b.setNext(c);
 //
         //c.setNext(a);
         //c.setPrev(b);
 //
         //int count = 0;
 //
         //for (int i = 0; i < 1000000; i++) {
             //People result = passNTimes(a, 6);
             //if (result.equals(a)) {
                 //count++;
             //}
         //}
         //System.out.println(count);
 //
 //
 //
 //
         //People a1 = new People("a");
         //People b1 = new People("b");
         //People c1 = new People("c");
         //People d1 = new People("d");
 //
         //a1.setPrev(d1);
         //a1.setNext(b1);
 //
         //b1.setPrev(a1);
         //b1.setNext(c1);
 //
         //c1.setNext(c1);
         //c1.setPrev(b1);
 //
         //d1.setNext(a1);
         //d1.setPrev(c1);
 //
         //int count1 = 0;
 //
         //for (int i = 0; i < 10000000; i++) {
             //People result = passNTimes(a1, 8);
             //if (result.equals(a1)) {
                 //count1++;
             //}
         //}
         //System.out.println(count1);
 //
 //
 //
     //}
//========

    private static People passNTimes(People now, int n) {
        People temp = now;
        for (int i = 0; i < n; i++) {
            boolean next = random.nextBoolean();
            temp = getNext(temp, next);
        }
        return temp;

    }

    private static People getNext(People now, boolean next) {
        if (next) {
            return now.getNext();
        } else {
            return now.getPrev();
        }
    }
}


class People {
    private String name;
    private People prev;
    private People next;

    public People(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public People getPrev() {
        return prev;
    }

    public void setPrev(People prev) {
        this.prev = prev;
    }

    public People getNext() {
        return next;
    }

    public void setNext(People next) {
        this.next = next;
    }

    @Override
    public String toString() {
        return "People{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        People people = (People) o;

        if (name != null ? !name.equals(people.name) : people.name != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        return result;
    }
}

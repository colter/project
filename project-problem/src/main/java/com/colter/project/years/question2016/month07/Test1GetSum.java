package com.colter.project.years.question2016.month07;

/**
 * 【程序8】 题目：求s=a+aa+aaa+aaaa+aa...a的值，其中a是一个数字。例如2+22+222+2222+22222(此时共有5个数相加)，
 * 几个数相加有键盘控制。 1.程序分析：关键是计算出每一项的值。
 * 
 * @author user
 *
 */
public class Test1GetSum {
    //TODO remove mainTue Dec 31 13:13:22 CST 2019
//========
     //public static void main(String[] args) {
         //System.out.println(getNum(1,5));
     //}
//========

	public static int getNum(int num,int pow){
		int result = 0;
		StringBuilder sb = new StringBuilder();
		while(pow > 0){
			int temp = getTemp(num, pow);
			result += temp;
			sb.append(temp);
			pow --;
			if(pow > 0){
				sb.append("+");
			}
		}
		System.out.println(sb);
		return result;
	}
	public static int getTemp(int value, int pow) {
		int result = value;
		while (pow > 1) {
			result += value * Math.pow(10,pow - 1);
			pow--;
		}
		return result;
	}
}

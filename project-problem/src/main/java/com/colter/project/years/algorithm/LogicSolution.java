package com.colter.project.years.algorithm;

import java.util.Arrays;

import static com.google.common.collect.Lists.newArrayList;
/**
 * 2018年刑侦科推理试题：
 * 1 这道题的答案是：
 *  A. A     B.B     C.C     D.D
 * 2. 第五题的答案是：
 *  A.C      B.D     C.A     D.B
 * 3. 以下选项中哪一题的答案与其他三项不同：
 *  A.3      B.6     C.2     D.4
 * 4.以下选项中哪两题的答案相同：
 *  A.1,5    B.2,7   C.1,9   D.6,10
 * 5. 一下选项中哪一题的答案与本题相同：
 *  A.8      B.4     C.9     D.7
 * 6. 以下选项中哪两题的答案与第8题相同：
 *  A.2,4    B.1,6  C.3,10  D.5,9
 * 7. 在此十道题中，被选中次数最少的选项字母是：
 *  A.C     B.B     C.A     D.D
 * 8. 以下选项中哪一题的答案与第一题的答案在字母中不相邻：
 *  A.7     B.5     C.2     D.10
 * 9.已知“第1题与第6题的答案相同” 与 “第X题与第5题的答案相同” 的真假性相反，那么X为：
 *  A.6     B.10        C.2     D.9
 * 10. 在此10道题中，ABCD四个字母出现次数最多与最少的差为：
 *  A.3     B.2     C.4     D.1
 * @author colter
 * 2018/4/20
 */
public class LogicSolution {

    private int[][] board = new int[10][4];
    //解法的数量
    private int total = 0;

    private int max = board.length;

    public void chooseK(int k) {
    //    System.out.println(String.format("=============   %s    ===============", k));

        if (k >= max) {
            //找到一个解，打印出来。
            total++;
            System.out.println(String.format("=============%s===============", total));
            for (int i = 0; i < max; i++) {
                System.out.println(Arrays.toString(board[i]));
            }
            System.out.println("=============================");
        } else {
            for (int i = 0; i < board[0].length; i++) {
                    board[k][i] = 1;
                    if(check()){
                        chooseK(k + 1);
                    }
                    board[k][i] = 0;
            }
        }
    }

    private boolean check() {
        return a1() && a2() && a3() && a4() && a5() && a6() && a7() && a8() && a9() && a10();
    }


    //TODO remove mainTue Dec 31 13:13:22 CST 2019
//========
     //public static void main(String[] args) {
         //LogicSolution ls = new LogicSolution();
         //ls.chooseK(0);
     //}
//========


    private boolean a1() {
        return getIndex(1) != null;
    }

    private boolean a2() {
        int a2 = getIndex(2);
        int a5 = getIndex(5);
        if (a2 == -1 || a5 == -1) {
            return true;
        }
        if (a2 == 0) {
            return a5 == 2;
        }
        if (a2 == 1) {
            return a5 == 3;
        }
        if (a2 == 2) {
            return a5 == 0;
        }
        if (a2 == 3) {
            return a5 == 1;
        }
        return true;
    }

    private boolean a3() {
        int index3 = getIndex(3);
        int index6 = getIndex(6);
        int index2 = getIndex(2);
        int index4 = getIndex(4);
        if (newArrayList(index3, index6, index2, index4).contains(-1)) {
            return true;
        }

        if (index3 == 0) {
            return index6 == index2 && index2 == index4 && index4 != index3;
        }
        if (index3 == 1) {
            return index3 == index2 && index2 == index4 && index4 != index3;
        }

        if (index3 == 2) {
            return index3 == index6 && index6 == index4 && index3 != index2;
        }

        if (index3 == 3) {
            return index3 == index6 && index6 == index2 && index3 != index4;
        }
        return true;
    }

    private boolean a4() {
        int a1 = getIndex(1);
        int a5 = getIndex(5);

        int a2 = getIndex(2);
        int a7 = getIndex(7);

        int a9 = getIndex(9);
        int a6 = getIndex(6);

        int a10 = getIndex(10);

        int a4 = getIndex(4);


        if (a4 == 0) {
            if (newArrayList(a1, a5).contains(-1)) {
                return true;
            }
            return a1 == a5;
        }
        if (a4 == 1) {
            if (newArrayList(a2, a7).contains(-1)) {
                return true;
            }
            return a2 == a7;
        }

        if (a4 == 2) {
            if (newArrayList(a1, a9).contains(-1)) {
                return true;
            }
            return a1 == a9;
        }
        if (a4 == 3) {
            if (newArrayList(a6, a10).contains(-1)) {
                return true;
            }
            return a6 == a10;
        }

        return true;
    }

    private boolean a5() {
        int a5 = getIndex(5);
        int a8 = getIndex(8);
        int a4 = getIndex(4);
        int a9 = getIndex(9);
        int a7 = getIndex(7);

        if (a5 == 0) {
            if (newArrayList(a8, a5).contains(-1)) {
                return true;
            }
            return a8 == a5;
        }
        if (a5 == 1) {
            if (newArrayList(a4, a5).contains(-1)) {
                return true;
            }
            return a4 == a5;
        }

        if (a5 == 2) {
            if (newArrayList(a9, a5).contains(-1)) {
                return true;
            }
            return a9 == a5;
        }
        if (a5 == 3) {
            if (newArrayList(a7, a5).contains(-1)) {
                return true;
            }
            return a7 == a5;
        }
        return true;
    }

    private boolean a6() {
        int a1 = getIndex(1);
        int a2 = getIndex(2);
        int a3 = getIndex(3);
        int a4 = getIndex(4);
        int a5 = getIndex(5);
        int a10 = getIndex(10);
        int a6 = getIndex(6);
        int a8 = getIndex(8);
        int a9 = getIndex(9);

        if (a6 == 0) {
            if (newArrayList(a2, a4, a8).contains(-1)) {
                return true;
            }
            return a8 == a2 && a8 == a4;
        }
        if (a6 == 1) {
            if (newArrayList(a1, a6, a8).contains(-1)) {
                return true;
            }
            return a8 == a1 && a8 == a6;
        }
        if (a6 == 2) {
            if (newArrayList(a3, a10, a8).contains(-1)) {
                return true;
            }
            return a8 == a3 && a8 == a10;
        }
        if (a6 == 3) {
            if (newArrayList(a5, a9, a8).contains(-1)) {
                return true;
            }
            return a8 == a5 && a8 == a9;
        }
        return true;
    }

    private boolean a7() {
        int a = getTimes("A");
        int b = getTimes("B");
        int c = getTimes("C");
        int d = getTimes("D");
        int a7 = getIndex(7);
        int a8 = getIndex(8);
        int a9 = getIndex(9);
        int a10 = getIndex(10);

        if (newArrayList(a10, a9, a8).contains(-1)) {
            return true;
        }
        if (a7 == 0) {
            return c < a && c < b && c < d;
        }
        if (a7 == 1) {
            return b < a && b < c && b < d;
        }
        if (a7 == 2) {
            return a < b && a < c && a < d;
        }
        if (a7 == 3) {
            return d < a && d < b && d < c;
        }
        return true;
    }

    private boolean a8() {
        int a1 = getIndex(1);
        int a2 = getIndex(2);
        int a5 = getIndex(5);
        int a10 = getIndex(10);
        int a7 = getIndex(7);
        int a8 = getIndex(8);


        int a9 = getIndex(9);
        if (newArrayList(a10, a9, a8).contains(-1)) {
            return true;
        }
        if (a8 == 0) {
            if (newArrayList(a1, a7).contains(-1)) {
                return true;
            }
            return Math.abs(a7-a1) != 1 &&  Math.abs(a5-a1) == 1 &&  Math.abs(a2-a1) == 1 &&  Math.abs(a10-a1) == 1;
        }
        if (a8 == 1) {
            if (newArrayList(a1, a5).contains(-1)) {
                return true;
            }
            return  Math.abs(a5-a1) != 1 &&  Math.abs(a7-a1) == 1 &&  Math.abs(a2-a1) == 1 &&  Math.abs(a10-a1) == 1;
        }
        if (a8 == 2) {
            if (newArrayList(a1, a2).contains(-1)) {
                return true;
            }
            return Math.abs(a2-a1) != 1 &&  Math.abs(a5-a1) == 1 &&  Math.abs(a7-a1) == 1 &&  Math.abs(a10-a1) == 1;
        }
        if (a8 == 3) {
            if (newArrayList(a1, a10).contains(-1)) {
                return true;
            }
            return Math.abs(a10-a1) != 1 &&  Math.abs(a5-a1) == 1 &&  Math.abs(a7-a1) == 1 &&  Math.abs(a2-a1) == 1;
        }
        return true;
    }

    private boolean a9() {
        int a1 = getIndex(1);
        int a6 = getIndex(6);


        int a2 = getIndex(2);
        int a5 = getIndex(5);
        int a10 = getIndex(10);
        int a9 = getIndex(9);

        if (newArrayList(a10, a9).contains(-1)) {
            return true;
        }

        boolean test1 = a1 == a6;

        if (a9 == 0) {
            if (newArrayList(a5, a6).contains(-1)) {
                return true;
            }
            return (a6 == a5) == !test1;
        }
        if (a9 == 1) {
            if (newArrayList(a5, a10).contains(-1)) {
                return true;
            }
            return (a10 == a5) == !test1;
        }
        if (a9 == 2) {
            if (newArrayList(a2, a5).contains(-1)) {
                return true;
            }
            return (a2 == a5) == !test1;
        }
        if (a9 == 3) {
            if (newArrayList(a5, a9).contains(-1)) {
                return true;
            }
            return (a9 == a5) == !test1;
        }
        return true;
    }

    private boolean a10() {
        int a = getTimes("A");
        int b = getTimes("B");
        int c = getTimes("C");
        int d = getTimes("D");

        int a10 = getIndex(10);
        if(a10 == -1){
            return true;
        }
        int max = (a > b ? a : b) > (c > d ? c : d) ? (a > b ? a : b) : (c > d ? c : d);
        int min = (a < b ? a : b) < (c < d ? c : d) ? (a < b ? a : b) : (c < d ? c : d);


        if (a10 == 0) {
            return max - min == 3;
        }
        if (a10 == 1) {
            return max - min == 2;
        }
        if (a10 == 2) {
            return max - min == 4;
        }
        if (a10 == 3) {
            return max - min == 1;
        }
        return true;
    }

    private int getTimes(String str) {
        int n = 0;
        int total = 0;
        if ("A".equals(str)) {
            n = 0;
        } else if ("B".equals(str)) {
            n = 1;
        } else if ("C".equals(str)) {
            n = 2;
        } else if ("D".equals(str)) {
            n = 3;
        }
        for (int i = 0; i < board.length; i++) {
            if (board[i][n] == 1) {
                total++;
            }
        }
        return total;
    }

    private Integer getIndex(int n) {
        int index = -1;
        for (int i = 0; i < board[0].length; i++) {
            if (board[n - 1][i] == 1) {
                index = i;
                break;
            }
        }
        return index;
    }
}

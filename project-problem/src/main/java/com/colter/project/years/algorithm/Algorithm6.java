package com.colter.project.years.algorithm;

import java.util.ArrayList;
import java.util.List;

/**
 *
 A[n,m]是一个 n 行 m 列的矩阵，a[i,j] 表示 A 的第 i 行 j 列的元素，定义 x[i,j] 为 A 的第 i 行和第 j 列除了 a[i,j] 之外所有元素(共n+m-2个)的乘积，即x[i,j]=a[i,1]*a[i,2]*...*a[i,j-1]*...*a[i,m]*a[1,j]*a[2,j]...*a[i-1,j]*a[i+1,j]...*a[n,j],
 现输入非负整形的矩阵 A[n,m]，求 MAX(x[i,j])，即所有的 x[i,j] 中的最大值。
 输入描述:
 第一行两个整数n和m。之后n行输入矩阵，均为非负整数。

 */

/**
 * @author liangchao03
 * @date 2017/7/18
 */
public class Algorithm6 {
    public int[] getMax(List<MatrixEntry>[] lists, int[][] matrix) {
        int[] result = new int[3];
        int[][] resultMatrix = new int[matrix.length][matrix[0].length];
        List<MatrixEntry> list1 = lists[0];
        List<MatrixEntry> list2 = lists[1];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                int value = matrix[i][j];
                MatrixEntry e1 = (MatrixEntry) list1.get(i).clone();
                MatrixEntry e2 = (MatrixEntry) list2.get(j).clone();

                if (value == 0) {
                    resultMatrix[i][j] = e1.mul(e2.getTotal()).getTotal();
                } else {
                    if(e1.hasZero() || e2.hasZero()){
                        resultMatrix[i][j] = 0;
                    }else{
                        resultMatrix[i][j] = e1.mul(e2.getTotal()).getTotal() / (value * value);
                    }

                }
            }
        }



        for (int i = 0; i < resultMatrix.length; i++) {
            for (int j = 0; j < resultMatrix[i].length; j++) {
                int value = resultMatrix[i][j];
                System.out.print(value + " ");
                if (i == 0 && j == 0) {
                    result[0] = i;
                    result[1] = j;
                    result[2] = value;
                } else {
                    if (value > result[2]) {
                        result[0] = i;
                        result[1] = j;
                        result[2] = value;
                    }
                }
            }
            System.out.println();
        }
        return result;
    }

    public List<MatrixEntry>[] changeMatrix(int[][] matrix) {
        List<MatrixEntry> list1 = new ArrayList<>(matrix.length);
        for (int i = 0; i < matrix.length; i++) {
            list1.add(new MatrixEntry());
        }
        List<MatrixEntry> list2 = new ArrayList<>(matrix[0].length);
        for (int i = 0; i < matrix[0].length; i++) {
            list2.add(new MatrixEntry());
        }

        for (int i = 0; i < matrix.length; i++) {
            MatrixEntry temp = list1.get(i);
            for (int j = 0; j < matrix[i].length; j++) {
                temp = temp.mul(matrix[i][j]);
                list2.get(j).mul(matrix[i][j]);
            }
        }

        List<MatrixEntry>[] lists = new ArrayList[2];
        lists[0] = list1;
        lists[1] = list2;
        return lists;
    }
}

class Result {
    int i;
    int j;
    int result;
}

class MatrixEntry implements  Cloneable{
    private int total = -1;
    private int zeroCount = 0;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getZeroCount() {
        return zeroCount;
    }

    public void setZeroCount(int zeroCount) {
        this.zeroCount = zeroCount;
    }

    public boolean hasZero() {
        return this.zeroCount > 0;
    }

    public MatrixEntry mul(int num) {
        if (zeroCount == 0) {
            if (num == 0) {
                this.zeroCount += 1;
            } else {
                if (this.total == -1) {
                    this.total = num;
                } else {
                    this.total = this.total * num;
                }
            }
        } else {
            if (num == 0) {
                this.total = 0;
            } else {
                if (this.total == -1) {
                    this.total = num;
                } else {
                    this.total = this.total * num;
                }
            }
        }
        return this;
    }

    @Override
    public String toString() {
        return "MatrixEntry{" +
                "total=" + total +
                ", zeroCount=" + zeroCount +
                '}';
    }

    @Override
    protected Object clone(){
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return this;
    }
}
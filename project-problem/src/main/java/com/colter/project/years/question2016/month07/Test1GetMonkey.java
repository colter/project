package com.colter.project.years.question2016.month07;

/**
 * 【程序17】 题目：猴子吃桃问题：猴子第一天摘下若干个桃子，当即吃了一半，还不瘾，又多吃了一个
 * 第二天早上又将剩下的桃子吃掉一半，又多吃了一个。以后每天早上都吃了前一天剩下
 * 的一半零一个。到第10天早上想再吃时，见只剩下一个桃子了。求第一天共摘了多少。 1.程序分析：采取逆向思维的方法，从后往前推断。
 * 
 * @author user
 *
 */
public class Test1GetMonkey {
    //TODO remove mainTue Dec 31 13:13:22 CST 2019
//========
     //public static void main(String[] args) {
         //System.out.println(getTotal(1));
     //}
//========

	public static int getTotal(int day) {
		if (day == 10) {
			return 1;
		} else {
			return (getTotal(day + 1) + 1) * 2;
		}

	}
}

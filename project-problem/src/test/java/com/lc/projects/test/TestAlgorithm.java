package com.lc.projects.test;

import com.colter.project.years.algorithm.*;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author liangchao03
 * @date 2017/7/5
 */
public class TestAlgorithm {
    @Test
    public void testAlgorithm3() {
        Node node1 = new Node(1);
        Node node2 = new Node(2);
        Node node3 = new Node(3);
        Node node4 = new Node(4);
        node1.setNext(node2);
        node2.setNext(node3);
        node3.setNext(node4);

        Node result = Algorithm3.reverse(node1);
        Algorithm3.printNode(result);
    }

    @Test
    public void testAlgorithm4() {
        Algorithm4 algorithm4 = new Algorithm4();
        assertEquals(3, algorithm4.getTimes(3));
        assertEquals(5, algorithm4.getTimes(4));
        assertEquals(8, algorithm4.getTimes(5));
        assertEquals(13, algorithm4.getTimes(6));
        //   assertEquals(62, algorithm4.getTimes(10));
    }

    @Test
    public void testAlgorithm5() {
        Algorithm5 algorithm5 = new Algorithm5();
        assertEquals(541, algorithm5.getTimes(100));
        assertEquals(541, algorithm5.getTestTimes(100));
        assertEquals(algorithm5.getTimes(101), algorithm5.getTestTimes(101));
        for (int i = 100; i < 150; i++) {
            assertEquals(algorithm5.getTimes(i), algorithm5.getTestTimes(i));
        }
    }

    @Test
    public void testAlgorithm6() {
        Algorithm6 algorithm6 = new Algorithm6();
        int[][] matrix = new int[][]{{1, 0, 0, 4}
                , {1, 2, 3, 4}
                , {1, 2, 3, 4}

        };
        List[] lists = algorithm6.changeMatrix(matrix);
        lists[0].forEach(value -> {
            System.out.println(value);
        });
        lists[1].forEach(value -> {
            System.out.println(value);
        });
        int[] result = algorithm6.getMax(lists, matrix);
        System.out.println(Arrays.toString(result));
    }

    @Test
    public void testAlgorithm6_2() {
        Algorithm6 algorithm6 = new Algorithm6();
        int[][] matrix = new int[][]{
                {5, 1, 8, 5, 2}
                , {1, 3, 10, 3, 3}
                , {7, 8, 5, 5, 16}

        };
        List[] lists = algorithm6.changeMatrix(matrix);
        lists[0].forEach(value -> {
            System.out.println(value);
        });
        lists[1].forEach(value -> {
            System.out.println(value);
        });
        int[] result = algorithm6.getMax(lists, matrix);
        System.out.println(Arrays.toString(result));
    }
}

import com.colter.project.data.structure.mit.*;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;


/**
 * @author liangchao
 * creation time:  2019/8/7 10:22
 * desc:
 */

public class SortTest {


    private Comparable[][] parameters = {
            {9, 2, 5, 8},
            {8, 2, 2, 5, 6, 3, 4, 6},
            {3, 2, 3, 4, 6},
            {7, 2, 3, 4, 6, 1},
            {9, 8, 7, 6, 5, 4},
            {3},
            {}
    };

    private int[][] parameters2 = {
            {9, 2, 5, 8},
            {8, 2, 2, 5, 6, 3, 4, 6},
            {3, 2, 3, 4, 6},
            {7, 2, 3, 4, 6, 1},
            {9, 8, 7, 6, 5, 4},
            {3},
            {}
    };

    @Test
    public void insertion() {
        for (Comparable[] param : parameters) {
            Comparable[] expect = Arrays.copyOf(param, param.length);
            Arrays.sort(expect);
            Comparable[] result = InsertionSort.sort(param);
            assertArrayEquals(expect, result);
        }
    }

    @Test
    public void merge() {
        for (Comparable[] param : parameters) {
            Comparable[] expect = Arrays.copyOf(param, param.length);
            Arrays.sort(expect);
            Comparable[] result = MergeSort.sort(param);
            assertArrayEquals(expect, result);
        }
    }

    @Test
    public void quick() {
        for (Comparable[] param : parameters) {
            Comparable[] expect = Arrays.copyOf(param, param.length);
            Arrays.sort(expect);
            Comparable[] result = QuickSort.sort(param);
            assertArrayEquals(expect, result);
        }
    }

    @Test
    public void count() {
        for (int[] param : parameters2) {
            int[] expect = Arrays.copyOf(param, param.length);
            Arrays.sort(expect);
            int[] result = CountingSort.sort(param);
            assertArrayEquals(expect, result);
        }
    }

    @Test
    public void quickSort2() {
        for (int[] param : parameters2) {
            int[] expect = Arrays.copyOf(param, param.length);
            Arrays.sort(expect);
            int[] result = QuickSort2.sort(param);
            assertArrayEquals(expect, result);
        }
    }

    @Test
    public void findK() {
        for (int i = 0; i < 6; i++) {
            int[] param = parameters2[i];
            System.out.println(Arrays.toString(param));
            for (int j = 1; j < param.length + 1; j++) {
                int result = FindElement.findK(param, j);
                System.out.println(result);
            }

        }
    }

    @Test
    public void stairs() {
        StairProblem sp = new StairProblem();
        int start = 5;
        for (int i = start; i < start + 5; i++) {
            System.out.println("Calc:" + i);
            long now = System.currentTimeMillis();
            long count = sp.twoStairs(i);
            long now2 = System.currentTimeMillis();
            System.out.println(count + "\t" + (now2 - now));
            long now3 = System.currentTimeMillis();
            long count2 = sp.twoStairsRecursion(i);
            long now4 = System.currentTimeMillis();
            System.out.println(count2 + "\t" + (now4 - now3));
        }
    }
}

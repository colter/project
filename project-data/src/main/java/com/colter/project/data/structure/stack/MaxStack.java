package com.colter.project.data.structure.stack;

import java.util.Stack;

public class MaxStack<E extends Comparable> extends Stack<E> {

    private Stack<Integer> indexStack;

    public MaxStack() {
        indexStack = new Stack<>();
    }

    @Override
    public E push(E item) {
        int maxIndex = indexStack.peek();
        E max = elementAt(maxIndex);
        if (item.compareTo(max) >= 0) {
            indexStack.push(this.size());
        }
        return super.push(item);
    }

    @Override
    public synchronized E pop() {
        int size = this.size();
        Integer maxIndex = indexStack.peek();
        if (maxIndex == size - 1) {
            indexStack.pop();
        }
        return super.pop();
    }

    public synchronized E getMax() {
        return elementAt(indexStack.peek());
    }
}

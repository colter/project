package com.colter.project.data.structure.mit;

/**
 * @author liangchao
 * creation time:  2019/8/7 16:03
 * desc:
 */

public class StairProblem {

    private static final int ONE_STAIRS_COUNT = 1;
    private static final int TWO_STAIRS_COUNT = 2;


    public long twoStairsRecursion(int total) {
        if (total == 1) {
            return ONE_STAIRS_COUNT;
        }

        if (total == 2) {
            return TWO_STAIRS_COUNT;
        }

        return twoStairsRecursion(total - 2) + twoStairsRecursion(total - 1);

    }


    public long twoStairs(int total) {
        if (total == 1) {
            return ONE_STAIRS_COUNT;
        }

        if (total == 2) {
            return TWO_STAIRS_COUNT;
        }

        long beforeTwo = ONE_STAIRS_COUNT;
        long before = TWO_STAIRS_COUNT;
        long count = 0;
        long start = 3;
        while (start <= total) {
            count = beforeTwo + before;
            start++;
            beforeTwo = before;
            before = count;
        }
        return count;
    }
}

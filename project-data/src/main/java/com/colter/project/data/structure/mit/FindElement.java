package com.colter.project.data.structure.mit;

/**
 * @author liangchao
 * creation time:  2019/8/8 11:31
 * desc:
 */

public class FindElement {

    public static int findK(int[] array, int k) {
        return quickSort(array, 0, array.length - 1, k);
    }

    private static int quickSort(int[] array, int low, int high, int k) {
        if (low > high || low < 0 || high >= array.length) {
            throw new RuntimeException("Unexpected");
        }
        int key = array[low];
        int indexLessAndEqual = QuickSort2.getMiddleIndexAfterSwapElement(array, low, high);
        int leftElementsWithKey = indexLessAndEqual - low + 1;
        if (k > leftElementsWithKey) {
            return quickSort(array, indexLessAndEqual + 1, high, k - leftElementsWithKey);
        } else if (k < leftElementsWithKey) {
            return quickSort(array, low, indexLessAndEqual - 1, k);
        } else {
            return key;
        }
    }
}
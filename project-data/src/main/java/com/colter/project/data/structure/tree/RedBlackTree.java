package com.colter.project.data.structure.tree;

import java.util.Objects;

/**
 * https://www.cs.usfca.edu/~galles/visualization/RedBlack.html
 * https://zh.wikipedia.org/wiki/%E7%BA%A2%E9%BB%91%E6%A0%91
 *
 * @author colter
 * 2018/3/17
 * <p>
 * 1. 节点是红色或者是黑色；
 * 2. 根节点是黑色；
 * 3. 每个叶节点（叶子节点也就是 NIL或空节点）是黑色;
 * 4. 每个红色节点的两个子节点都是黑色的（也就是说不存在两个连续的红色节点）；
 * 5. 从任一节点到其没个叶节点的所有路径都包含相同数目的黑色节点;
 */

public class RedBlackTree<T extends Comparable<T>> {
    private T data;
    private RedBlackTree<T> leftChild;
    private RedBlackTree<T> rightChild;

    private RedBlackTree<T> parent;
    private Color color;

    public void setData(T data) {
        this.data = data;
    }

    public void setParent(RedBlackTree<T> parent) {
        this.parent = parent;
    }

    public T getData() {
        return data;
    }

    public RedBlackTree<T> getLeftChild() {
        return leftChild;
    }

    public RedBlackTree<T> getRightChild() {
        return rightChild;
    }

    public RedBlackTree<T> getParent() {
        return parent;
    }

    public RedBlackTree(T data) {
        this.data = data;
        this.color = Color.RED;
    }

    public RedBlackTree(T data, boolean root) {
        this.data = data;
        this.color = root ? Color.BLACK : Color.RED;
    }

    private RedBlackTree(T data, Color color) {
        this.data = data;
        this.color = color;
    }


    public void setLeftChild(RedBlackTree<T> leftChild) {
        this.leftChild = leftChild;
        if (leftChild != null) {
            leftChild.parent = this;
        }
    }

    public void setRightChild(RedBlackTree<T> rightChild) {
        this.rightChild = rightChild;
        if (rightChild != null) {
            rightChild.parent = this;
        }
    }


    public void insert(T data) {
        insertNode(new RedBlackTree<>(data, Color.RED));
    }

    private RedBlackTree<T> findRoot(RedBlackTree<T> node) {
        while (node.getParent() != null) {
            node = node.getParent();
        }
        return node;
    }

    private void insertNode(RedBlackTree<T> node) {
        RedBlackTree<T> parent = findRoot(this);
        RedBlackTree<T> root = findRoot(this);

        while (root != null) {
            parent = root;
            if (root.getData().compareTo(node.getData()) > 0) {
                root = root.getLeftChild();
            } else {
                root = root.getRightChild();
            }
        }

        if (parent.getData().compareTo(node.getData()) > 0) {
            parent.setLeftChild(node);
        } else {
            parent.setRightChild(node);
        }

        insertFixTree(node);
    }

    /**
     * 修正红黑树
     *
     * @param node 修正的节点
     */
    private void insertFixTree(RedBlackTree<T> node) {
        //1.插入的是根节点，染成黑色
        if (node.isLeft() == null) { //判断是左右儿子为空，说明是根节点
            node.markToBlack();
            return;
        }
        //2.插入节点的 父节点为黑色，满足所有条件
        if (node.getParent().isBlack()) {
            return;
        }
        //3. 插入节点的 父节点为红色
        if (node.getParent().isRed()) {
            //判断：父亲结点和叔叔结点均为红色
            //处理：父节点 叔叔节点 染为黑色，祖父节点染为红色
            if (node.uncleIsRed()) {
                node.getParent().markToBlack();
                node.getUncle().markToBlack();
                node.getParent().getParent().markToRed();
                //递归处理祖父节点
                insertFixTree(node.getParent().getParent());
            } else {
                //父节点为红色，叔叔节点为黑色，分两种情况

                //在余下的情形下，我们假定父节点P是其父亲G的左子节点。如果它是右子节点，情形4和情形5中的左和右应当对调。

                //当前节点的父节点是红色，叔叔节点是黑色，且当前节点是其父节点的左孩子
                RedBlackTree<T> parent = node.getParent();
                if (parent.isLeft()) {
                    //1. 新节点 是父节点左儿子：祖父节点G右旋 G染黑，F染红
                    if (node.isLeft()) {
                        RedBlackTree<T> grand = parent.getParent();
                        grand.rightRotate();
                        grand.markToBlack();
                        parent.markToRed();
                    } else {
                        //1. 新节点 是父节点右儿子：新节点的父节点左旋，新节点的新父节点右旋 新节点黑 新父节点 红（交换颜色）
                        parent.leftRotate();
                        RedBlackTree<T> newParent = node.getParent();
                        newParent.rightRotate();
                        node.markToBlack();
                        newParent.markToRed();
                    }
                } else {
                    //当前节点的父节点是红色，叔叔节点是黑色，且当前节点是其父节点的you孩子

                    // 1. 新节点 是父节点左儿子：新节点的父节点右旋，新节点的新父节点左旋  新节点黑 新父节点 红（jiaohuanyanse）
                    if (node.isLeft()) {
                        parent.rightRotate();
                        RedBlackTree<T> newParent = node.getParent();
                        newParent.leftRotate();
                        node.markToBlack();
                        newParent.markToRed();
                    } else {
                        //1. 新节点 是父节点右儿子：祖父节点G左旋，父黑 祖父红
                        RedBlackTree<T> grand = parent.getParent();
                        grand.leftRotate();
                        grand.markToRed();
                        parent.markToBlack();
                    }
                }
            }
        }
    }


    public void remove(T value) {
        RedBlackTree<T> result = findNode(value);
        if (result == null) {
            return;
        }
        removeNode(result);

    }

    private void removeNode(RedBlackTree<T> result) {
        //在删除带有两个非叶子儿子的节点的时候，我们找到要么在它的左子树中的最大元素、要么在它的右子树中的最小元素，并把它的值转移到要删除的节点中,删除复制的节点
        //我们只需要讨论删除只有一个儿子的节点
        if (result.leftChild != null && result.rightChild != null) {
            result.setData(result.getRightChild().getData());
            removeNode(result.getRightChild());
        }

        chooseDeleteCase(result);
    }

    private void chooseDeleteCase(RedBlackTree<T> result) {

        //删除节点是红色
        if (result.isRed()) {
            deleteCase1(result);
            return;
        }

        //删除节点是黑色，顶上来的儿子节点是红色
        if (result.isSonRed()) {
            deleteCase2(result);
            return;
        }

        //删除节点是黑色，顶上来的儿子节点也是黑色
        RedBlackTree<T> n = result.getOnlySon();
        RedBlackTree<T> parent = result.getParent();

        //情形1: N是新的根。在这种情形下，我们就做完了。我们从所有路径去除了一个黑色节点，而新根是黑色的，所以性质都保持着。
        if (parent == null) {
            n.parent = null;
            return;
        }

        RedBlackTree<T> s = null;
        if (n == null) {
            parent.setLeftChild(null);
            parent.setRightChild(null);
        } else {
            if (n.isLeft()) {
                parent.setLeftChild(n);
                s = parent.getRightChild();
            } else {
                parent.setRightChild(n);
                s = parent.getLeftChild();
            }
        }

        //情形2
        if (s != null && s.isRed()) {
            deleteCase3_2(n, parent, s);
            return;
        }

        //情形3
        if (parent.isBlack()
                && (s == null || s.getLeftChild() == null || s.getLeftChild().isBlack())
                && (s == null || s.getRightChild() == null || s.getRightChild().isBlack())) {
            deleteCase3_3(n, parent, s);
            return;
        }

        //情形4
        if (s.isBlack()
                && (s.getLeftChild() == null || s.getLeftChild().isBlack())
                && (s.getRightChild() == null || s.getRightChild().isBlack())
                && parent.isRed()) {
            deleteCase3_4(n, parent, s);
            return;
        }

        //情形5-1
        if (s.isBlack()
                && s.getLeftChild() != null && s.getLeftChild().isRed()
                && (s.getRightChild() == null || s.getRightChild().isBlack())
                && n.isLeft()
        ) {
            deleteCase3_5_1(n, parent, s);
            return;
        }
        //情形5-2
        if (s.isBlack()
                && s.getRightChild() != null && s.getRightChild().isRed()
                && (s.getLeftChild() == null || s.getLeftChild().isBlack())
                && n.isRight()
        ) {
            deleteCase3_5_2(n, parent, s);
            return;
        }

        //情形6-1
        if (s.isBlack()
                && s.getRightChild() != null && s.getRightChild().isRed()
                && n.isLeft()) {
            deleteCase3_6_1(n, parent, s);
            return;
        }

        //情形6-2
        if (s.isBlack()
                && s.getLeftChild() != null && s.getLeftChild().isRed()
                && n.isRight()) {
            deleteCase3_6_2(n, parent, s);
            return;
        }

    }

    private void deleteCase1(RedBlackTree<T> result) {
        RedBlackTree<T> child = result.getOnlySon();
        if (child == null) {
            if (result.isLeft()) {
                result.parent.leftChild = null;
            } else {
                result.parent.rightChild = null;
            }
            return;
        }
        if (child.isLeft()) {
            result.parent.setLeftChild(child);
        } else {
            result.parent.setRightChild(child);
        }
    }

    private void deleteCase2(RedBlackTree<T> result) {
        RedBlackTree<T> child = result.getOnlySon();
        RedBlackTree<T> parent = result.getParent();
        if (parent == null) {
            child.parent = null;
        } else {
            boolean isLeft = result.isLeft();
            if (isLeft) {
                parent.setLeftChild(child);
            } else {
                parent.setRightChild(child);
            }
        }
        child.markToBlack();
    }

    private void deleteCase3_1(RedBlackTree<T> n) {

    }

    private void deleteCase3_2(RedBlackTree<T> n, RedBlackTree<T> p, RedBlackTree<T> s) {
        if (n.isLeft()) {
            n.parent.leftRotate();
        } else {
            n.parent.rightRotate();
        }
        s.color = n.parent.color;
        n.parent.markToRed();

        deleteCase3_4(n, p, s);

        //情形4
        if (s.isBlack()
                && (s.getLeftChild() == null || s.getLeftChild().isBlack())
                && (s.getRightChild() == null || s.getRightChild().isBlack())
                && parent.isRed()) {
            deleteCase3_4(n, parent, s);
            return;
        }

        //情形5-1
        if (s.isBlack()
                && s.getLeftChild() != null && s.getLeftChild().isRed()
                && (s.getRightChild() == null || s.getRightChild().isBlack())
                && n.isLeft()
        ) {
            deleteCase3_5_1(n, parent, s);
            return;
        }
        //情形5-2
        if (s.isBlack()
                && s.getRightChild() != null && s.getRightChild().isRed()
                && (s.getLeftChild() == null || s.getLeftChild().isBlack())
                && n.isRight()
        ) {
            deleteCase3_5_2(n, parent, s);
            return;
        }

        //情形6-1
        if (s.isBlack()
                && s.getRightChild() != null && s.getRightChild().isRed()
                && n.isLeft()) {
            deleteCase3_6_1(n, parent, s);
            return;
        }

        //情形6-2
        if (s.isBlack()
                && s.getLeftChild() != null && s.getLeftChild().isRed()
                && n.isRight()) {
            deleteCase3_6_2(n, parent, s);
            return;
        }
    }

    private void deleteCase3_3(RedBlackTree<T> n, RedBlackTree<T> p, RedBlackTree<T> s) {
        s.markToRed();
        chooseDeleteCase(p);
    }

    private void deleteCase3_4(RedBlackTree<T> n, RedBlackTree<T> p, RedBlackTree<T> s) {
        p.markToBlack();
        s.markToRed();
    }


    private void deleteCase3_5_1(RedBlackTree<T> n, RedBlackTree<T> p, RedBlackTree<T> s) {
        s.rightRotate();
        s.color = s.parent.color;
        s.parent.markToBlack();
    }

    private void deleteCase3_5_2(RedBlackTree<T> n, RedBlackTree<T> p, RedBlackTree<T> s) {
        s.leftRotate();
        s.color = s.parent.color;
        s.parent.markToBlack();
    }

    private void deleteCase3_6_1(RedBlackTree<T> n, RedBlackTree<T> p, RedBlackTree<T> s) {
        p.leftRotate();
        s.color = p.color;
        p.markToBlack();

    }

    private void deleteCase3_6_2(RedBlackTree<T> n, RedBlackTree<T> p, RedBlackTree<T> s) {
        p.rightRotate();
        s.color = p.color;
        p.markToBlack();
    }

    //删除的时候，判断子节点是否为红色
    private boolean isSonRed() {
        if (this.getLeftChild() == null && this.getRightChild() == null) {
            return false;
        }
        if (this.getLeftChild() != null) {
            return this.getLeftChild().isRed();
        } else {
            return this.getRightChild().isRed();
        }
    }

    private RedBlackTree<T> getOnlySon() {
        return this.getLeftChild() == null ? this.getRightChild() : this.getLeftChild();
    }

    private RedBlackTree<T> findNode(T value) {
        RedBlackTree<T> root = findRoot(this);
        while (root != null && root.getData().compareTo(value) != 0) {
            if (root.getData().compareTo(value) > 0) {
                root = root.getLeftChild();
            } else if (root.getData().compareTo(value) < 0) {
                root = root.getRightChild();
            }
        }
        return root;
    }


    public Boolean isLeft() {
        if (this.parent == null) {
            return null;
        }
        if (this == this.parent.getLeftChild()) {
            return true;
        }
        return false;
    }

    public Boolean isRight() {
        return isLeft() == null ? null : !isLeft();
    }

    public RedBlackTree<T> getUncle() {
        if (this.parent != null && this.parent.isLeft() != null) {
            if (this.parent.isLeft()) {
                return this.parent.getParent().getRightChild();
            } else {
                return this.parent.getParent().getLeftChild();
            }
        }
        return null;
    }

    public boolean uncleIsRed() {
        RedBlackTree<T> uncle = getUncle();
        if (uncle == null) {
            return false;
        }
        return uncle.isRed();
    }

    /**
     * 左旋
     */
    public void leftRotate() {
        RedBlackTree<T> parentNode = this.parent;
        RedBlackTree<T> rightNode = this.rightChild;
        assert rightNode != null;

        //当前节点的右儿子的左儿子
        RedBlackTree<T> rightLeftNode = rightNode.getLeftChild();

        //右儿子替换自己的位置
        if (this.isLeft() == null) {
            rightNode.parent = null;
        } else if (this.isLeft()) {
            parentNode.setLeftChild(rightNode);
        } else {
            parentNode.setRightChild(rightNode);
        }

        //自己变成右儿子的左儿子
        rightNode.setLeftChild(this);

        //右儿子的左儿子挂到自己的左边
        this.setRightChild(rightLeftNode);

    }

    /**
     * 右旋
     */
    public void rightRotate() {
        RedBlackTree<T> parentNode = this.parent;
        RedBlackTree<T> leftNode = this.leftChild;
        //当前节点的  左儿子的右儿子
        RedBlackTree<T> leftRightNode = leftNode.rightChild;


        //左儿子替换自己的位置
        if (this.isRight() == null) {
            leftNode.parent = null;
        } else if (this.isLeft()) {
            parentNode.setLeftChild(leftNode);
        } else {
            parentNode.setRightChild(leftNode);
        }

        //自己变成左儿子的右儿子
        leftNode.setRightChild(this);

        //左儿子的右儿子 挂到自己的左边
        this.setLeftChild(leftRightNode);

    }

    private boolean isRed() {
        return this.color.isRed();
    }

    private boolean isBlack() {
        return this.color.isBlack();
    }

    private void changeColor() {
        if (this.color.isRed()) {
            this.color = Color.BLACK;
        } else {
            this.color = Color.RED;
        }
    }

    public void markToRed() {
        this.color = Color.RED;
    }

    public void markToBlack() {
        this.color = Color.BLACK;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        RedBlackTree<?> that = (RedBlackTree<?>) o;
        return Objects.equals(data, that.data) &&
                Objects.equals(leftChild, that.leftChild) &&
                Objects.equals(rightChild, that.rightChild) &&
                Objects.equals(parent, that.parent) &&
                color == that.color;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), data, leftChild, rightChild, parent, color);
    }

    @Override
    public String toString() {
        return "{" +
                "data=" + data +
                ", color=" + color +
                '}';
    }

    private enum Color {
        BLACK(0, "Black"), RED(1, "Red");

        private int code;
        private String color;

        Color(int i, String color) {
            this.code = i;
            this.color = color;
        }

        public boolean isRed() {
            return this == RED;
        }

        public boolean isBlack() {
            return this == BLACK;
        }
    }
}

package com.colter.project.data.mybase;

import java.io.*;
import java.util.LinkedList;

/**
 * @author liangchao03
 * @date 2017/8/22
 */
public class MyBase64 {
    private static void encode(String sourcePath, String destPath) {
        BufferedReader br = null;
        BufferedWriter bw = null;
        try {
            String start = "data:image/jpeg;base64,";
            br = new BufferedReader(new FileReader(sourcePath));
            LinkedList<String> list = new LinkedList();
            String temp;
            int count = 0;
            while ((temp = br.readLine()) != null) {
                if (count == 0) {
                    list.add(temp.replace(start, ""));
                } else {
                    list.add(temp);
                }
            }

            bw = new BufferedWriter(new FileWriter(destPath));
            for (int i = list.size() - 1; i >= 0; i--) {
                bw.write(new StringBuffer(list.get(i)).reverse().toString());
            }

            System.out.println("Encode Done");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private static void decode(String sourcePath, String destPath) {
        BufferedReader br = null;
        BufferedWriter bw = null;
        try {
            String start = "data:image/jpeg;base64,";
            br = new BufferedReader(new FileReader(sourcePath));
            LinkedList<String> list = new LinkedList();
            String temp;
            while ((temp = br.readLine()) != null) {
                list.add(temp.replace(start, ""));
            }

            bw = new BufferedWriter(new FileWriter(destPath));
            for (int i = list.size() - 1; i >= 0; i--) {
                if (i == 0) {
                    bw.write(start + new StringBuffer(list.get(i)).reverse().toString());
                } else {
                    bw.write(new StringBuffer(list.get(i)).reverse().toString());
                }
            }

            System.out.println("Decode Done");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

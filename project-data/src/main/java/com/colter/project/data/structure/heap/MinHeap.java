package com.colter.project.data.structure.heap;

/**
 * @author LC
 * 2018/2/3
 */
public class MinHeap<E extends Comparable<E>> extends AbstractHeap<E> {


    @Override
    protected boolean choseLeft(E leftElement, E rightElement) {
        return leftElement.compareTo(rightElement) <= 0;
    }

}

package com.colter.project.data.structure.mit;

/**
 * @author liangchao
 * creation time:  2019/8/8 13:33
 * desc:
 */

public class QuickSort2 {

    public static int[] sort(int[] array) {
        sortByIndex(array, 0, array.length - 1);
        return array;
    }

    public static void sortByIndex(int[] array, int low, int high) {
        if (low > high || low < 0 || high >= array.length) {
            return;
        }
        int indexLessAndEqual = getMiddleIndexAfterSwapElement(array, low, high);
        sortByIndex(array, low, indexLessAndEqual - 1);
        sortByIndex(array, indexLessAndEqual + 1, high);

    }

    public static int getMiddleIndexAfterSwapElement(int[] array, int low, int high) {
        int key = array[low];
        int start = low + 1;
        int end = high;
        int indexLessAndEqual = low;

        while (start <= end) {
            while (start <= end && array[start] <= key) {
                start++;
                indexLessAndEqual++;
            }

            while (start <= end && array[end] > key) {
                end--;
            }
            if (start < end) {
                int temp = array[start];
                array[start] = array[end];
                array[end] = temp;
            }
            if (start == end || start > end) {
                int temp = array[indexLessAndEqual];
                array[indexLessAndEqual] = array[low];
                array[low] = temp;
            }
        }
        return indexLessAndEqual;
    }
}

package com.colter.project.data.structure.mit;

import static com.colter.project.data.structure.sort.BucketSort.getMax;

/**
 * @author liangchao
 * creation time:  2019/8/8 10:39
 * desc:
 */

public class CountingSort {

    public static int[] sort(int[] array) {
        if (array == null || array.length < 2) {
            return array;
        }
        int max = getMax(array);
        int[] count = new int[max + 1];
        for (int i1 : array) {
            count[i1] = count[i1] + 1;
        }

        for (int i = 1; i < count.length; i++) {
            count[i] = count[i - 1] + count[i];
        }

        int[] result = new int[array.length];

        for (int i = array.length - 1; i >= 0; i--) {
            int value = array[i];
            int index = count[value];
            count[value] = count[value] - 1;
            result[index - 1] = value;
        }

        return result;
    }
}

package com.colter.project.data.structure.heap;

/**
 * @author LC
 * 2018/2/2
 * 1 构建一个最大堆（添加一个元素）
 *  每当向最大堆添加一个元素时，把该元素放到堆的末尾。
 *  比较该元素结点与父节点，如果大于父节点，则调换其位置，（该节点此时位于父节点的位置）
 *  继续比较与新的父节点的大小，直到找到比其大的父节点，或者直到顶点
 * 2 删除顶节点
 *   直接删除顶点，然后把最后一个叶子结点替换到顶点位置。（此时顶点的值为最后的叶子结点的值），删除最后的叶子节点
 *   比较顶点与最大的子节点的大小，如果小于最大的子节点，则交换位置，
 *   继续比较与新的最大子节点的值，直到比最大的子节点值大，或者没有子节点（即是叶子节点）。
 *
 *   子节点计算公式： pindex
 *   son1 = pindex * 2 + 1
 *   son2 = pindex * 2 + 2
 *
 *   父节点计算公式: sindex
 *   parent = (sindex-1) / 2
 */
public class MaxHeap<E extends  Comparable<E>> extends AbstractHeap<E> {


    @Override
    protected boolean choseLeft(E leftElement, E rightElement) {
        return leftElement.compareTo(rightElement) >= 0;
    }


    @Override
    public synchronized String toString() {
        return super.toString();
    }
}

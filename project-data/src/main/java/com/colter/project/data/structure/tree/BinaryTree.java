package com.colter.project.data.structure.tree;

import java.util.Objects;

public class BinaryTree<T> {

    private T data;
    private BinaryTree<T> leftChild;
    private BinaryTree<T> rightChild;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public BinaryTree<T> getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(BinaryTree<T> leftChild) {
        this.leftChild = leftChild;
    }

    public BinaryTree<T> getRightChild() {
        return rightChild;
    }

    public void setRightChild(BinaryTree<T> rightChild) {
        this.rightChild = rightChild;
    }

    public BinaryTree() {

    }

    public BinaryTree(T data) {
        this.data = data;
        this.leftChild = null;
        this.rightChild = null;
    }

    public BinaryTree(T data, BinaryTree<T> leftChild, BinaryTree<T> rightChild) {
        this.data = data;
        this.leftChild = leftChild;
        this.rightChild = rightChild;
    }


    public boolean isEmpty() {
        return data == null;
    }

    public void inOrder(BinaryTree<T> subTree) {
        if (subTree != null) {
            inOrder(subTree.leftChild);
            visit(subTree);
            inOrder(subTree.rightChild);
        }
    }

    private void visit(BinaryTree<T> subTree) {
        System.out.println(subTree);
    }

    @Override
    public String toString() {
        return "BinaryTree [data=" + data + "]";
    }

    private int high() {
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BinaryTree<?> that = (BinaryTree<?>) o;
        return Objects.equals(data, that.data) &&
                Objects.equals(leftChild, that.leftChild) &&
                Objects.equals(rightChild, that.rightChild);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), data, leftChild, rightChild);
    }
}

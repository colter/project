package com.colter.project.data.structure.mit;

/**
 * @author liangchao
 * creation time:  2019/8/7 14:37
 * desc:
 */

public class MergeSort {
    public static <T extends Comparable<T>> T[] sort(T[] array) {
        sort(array, 0, array.length - 1);
        return array;
    }

    public static <T extends Comparable<T>> void sort(T[] array, int start, int end) {
        if (array == null || array.length < 2 || end - start < 1) {
            return;
        }

        int mid = (start + end) / 2;
        sort(array, start, mid);
        sort(array, mid + 1, end);
        merge(array, start, mid, end);
    }

    private static <T extends Comparable<T>> void merge(T[] array, int start, int mid, int end) {
        Object[] temp =new Object[end - start + 1];
        int i = start;
        int j = mid + 1;
        int k = 0;
        while (i <= mid && j <= end) {
            if (array[i].compareTo(array[j]) > 0) {
                temp[k++] = array[j++];
            } else {
                temp[k++] = array[i++];
            }
        }
        while (i <= mid) {
            temp[k++] = array[i++];
        }

        while (j <= end) {
            temp[k++] = array[j++];
        }

        System.arraycopy(temp, 0, array, start, end - start + 1);
    }
}
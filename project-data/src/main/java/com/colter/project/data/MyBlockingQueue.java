package com.colter.project.data;


import java.util.Arrays;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author LC
 * 2018/2/17
 */
public class MyBlockingQueue<E> {
    private volatile int putIndex = 0;
    private volatile int takeIndex = 0;

    private final Object[] elements;
    private final ReentrantLock lock;
    private final Condition notFull;
    private final Condition notEmpty;
    private final int size;


    public int getPutIndex() {
        return putIndex;
    }

    public void setPutIndex(int putIndex) {
        this.putIndex = putIndex;
    }

    public int getTakeIndex() {
        return takeIndex;
    }

    public void setTakeIndex(int takeIndex) {
        this.takeIndex = takeIndex;
    }

    public Object[] getElements() {
        return elements;
    }

    public ReentrantLock getLock() {
        return lock;
    }

    public Condition getNotFull() {
        return notFull;
    }

    public Condition getNotEmpty() {
        return notEmpty;
    }

    public int getSize() {
        return size;
    }

    public MyBlockingQueue(int size) {
        elements = new Object[size];
        lock = new ReentrantLock();
        notFull = lock.newCondition();
        notEmpty = lock.newCondition();
        this.size = size;
    }

    public void put(E e) {
        final ReentrantLock lock = this.lock;
        lock.lock();
        while (putIndex == size) {
            try {
                notEmpty.await();
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            lock.lock();
        }
        add(e);
        lock.unlock();
    }

    private void add(E e) {
        elements[putIndex++] = e;
        takeIndex++;
        notFull.signalAll();
        System.out.println("Add " + e);
    }

    public E take() {
        final ReentrantLock lock = this.lock;
        lock.lock();
        while (takeIndex == 0) {
            try {
                notFull.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        E result = (E) elements[takeIndex - 1];
        System.out.println(Arrays.toString(elements) + result);

        takeIndex--;
        putIndex--;
        elements[takeIndex] = null;
        notEmpty.signalAll();
        lock.unlock();
        return result;
    }


}

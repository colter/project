package com.colter.project.data.structure.heap;

import javax.annotation.Nonnull;

/**
 * @author LC
 * 2018/2/3
 */
public class LimitHeap<E extends Comparable<E>> extends AbstractHeap<E> {

    public LimitHeap(HeapType type, int limitSize) {
        assert limitSize > 0;
        this.type = type;
        this.limit = limitSize;
    }

    private HeapType type;
    private int limit;

    public HeapType getType() {
        return type;
    }

    public int getLimit() {
        return limit;
    }

    @Override
    public synchronized boolean add(@Nonnull E t) {
        E top = peek();
        if (top == null || size() < limit) {
            return super.add(t);
        }

        if (this.type == HeapType.FIND_MIN) {
            if (t.compareTo(top) < 0) {
                removeAndReturnTop();
                return super.add(t);
            }
        }

        if (this.type == HeapType.FIND_MAX) {
            if (t.compareTo(top) > 0) {
                removeAndReturnTop();
                return super.add(t);
            }
        }
        return false;
    }

    @Override
    protected boolean choseLeft(E leftElement, E rightElement) {
        if (this.type == HeapType.FIND_MIN) {
            return leftElement.compareTo(rightElement) >= 0;
        } else {
            return leftElement.compareTo(rightElement) <= 0;
        }
    }


    public enum HeapType {
        FIND_MIN, FIND_MAX
    }


}

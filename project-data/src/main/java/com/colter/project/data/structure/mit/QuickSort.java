package com.colter.project.data.structure.mit;

/**
 * @author liangchao
 * creation time:  2019/8/7 17:09
 * desc:
 */

public class QuickSort {
    public static <T extends Comparable<T>> T[] sort(T[] array) {

        sort(array, 0, array.length - 1);
        return array;
    }

    private static <T extends Comparable<T>> void sort(T[] array, int start, int end) {
        if (array == null || array.length < 2 || end - start < 1) {
            return;
        }
        T flag = array[start];
        int i = start;
        int j = i + 1;
        while (j <= end) {
            //现在发现的都是小于flag的 那么两个指针都往前走就行了
            if (array[j].compareTo(flag) <= 0 && j - i == 1) {
                i++;
            } else if (array[j].compareTo(flag) <= 0 && j - i > 1) {
                //两个指针有间隔，指针i指的是当前小于flag的，i+1大于flag，此时j发现了小于flag的，那么 i+1和 j换位置，然后ij都加1
                T temp = array[i + 1];
                array[i + 1] = array[j];
                array[j] = temp;
                i++;
            }
            j++;
        }
        array[start] = array[i];
        array[i] = flag;

        sort(array, start, i - 1);
        sort(array, i + 1, end);
    }
}

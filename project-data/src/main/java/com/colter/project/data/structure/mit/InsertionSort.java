package com.colter.project.data.structure.mit;

/**
 * @author liangchao
 * creation time:  2019/8/7 10:12
 * desc:
 */

public class InsertionSort {

    public static <T extends Comparable<T>> T[] sort(T[] array) {
        if (array == null || array.length < 2) {
            return array;
        }

        for (int i = 1; i < array.length; i++) {
            int old = i;
            int j = i - 1;
            while (j >= 0) {
                if (array[old].compareTo(array[j]) < 0) {
                    T temp = array[old];
                    array[old] = array[j];
                    array[j] = temp;
                    old = j;
                    j--;
                } else {
                    break;
                }
            }
        }
        return array;
    }
}
